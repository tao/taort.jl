# Automatic build of Julia bindings to a library

This directory provides a Julia script, [`build.jl`](./build.jl), to build the
file `deps.jl` with the following constants:

- `libtao_include` is the directory where the headers TAO are installed;
- `libtao_library` is the path to the Real-Time TAO library;
- `libtao_version` is the version number of the Real-Time TAO library.

These constants are needed by [`wrap.jl`](../src/wrap.jl) and re-generate this
file (in the [`gen`](../gen) directory).

The file `deps.jl` can be built from Julia REPL by:

```julia
import Pkg
Pkg.build("TaoRT")
```

or form the shell by:

```sh
julia build.jl
```
