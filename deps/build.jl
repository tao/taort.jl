using Libdl

"""
    try_load_libtao(libname) -> vers

attempts to load TAO dynamic library `libname` and to call `tao_version_get` in
this library. On success, returns the version of the TAO library. On failure,
returns `nothing` (errors are catched).

"""
function try_load_libtao(libname::AbstractString)
    try
        handle = dlopen(libname)
        try
            major = Ref{Cint}()
            minor = Ref{Cint}()
            micro = Ref{Cint}()
            ccall(dlsym(handle, :tao_version_get), Cvoid, (Ptr{Cint}, Ptr{Cint}, Ptr{Cint}),
                  major, minor, micro)
            return VersionNumber(major[], minor[], micro[])
        catch
        finally
            dlclose(handle)
        end
    catch
    end
    return nothing
end

function check_absdir(arg::AbstractString, path::AbstractString)
    startswith(path, '/') || error("`$arg = \"$path\"` is not an absolute path.")
    isdir(path) || error("`$arg = \"$path\"` is not an existing directory.")
    nothing
end

# We wrap everything into a function to avoid having undefined variables...
function build_deps(dest::AbstractString = "deps.jl";
                    prefix::Union{AbstractString,Nothing} = nothing,
                    incdir::Union{AbstractString,Nothing} = nothing,
                    libdir::Union{AbstractString,Nothing} = nothing)
    if (prefix isa AbstractString) && (incdir === nothing) && (libdir === nothing)
        check_absdir("prefix", prefix)
        incdir = abspath(joinpath(prefix, "include"))
        libdir = abspath(joinpath(prefix, "lib"))
    elseif (prefix === nothing) && (incdir isa AbstractString) && (libdir isa AbstractString)
        check_absdir("incdir", incdir)
        check_absdir("libdir", libdir)
    else
        error("either keywords `incdir` and `libdir` or keyword `prefix` must be specified")
    end

    # Search for library.
    libfile = "libtao.$(Libdl.dlext)"
    library = abspath(joinpath(libdir, libfile))
    version = try_load_libtao(library)
    if version === nothing
        mesg = "Library \"$libfile\" not found in \"$libdir\"."
        @error(mesg)
        error(mesg)
    else
        @info "Library `$libfile` version $version found in `$(dirname(library))`"
    end

    # Search for header.
    hdrfile = "tao-errors.h"
    header = abspath(joinpath(incdir, hdrfile))
    if !isfile(header)
        mesg = "Header \"$hdrfile\" not found in \"$incdir\"."
        @error(mesg)
        error(mesg)
    else
        @info "Header file `$hdrfile` found in `$(dirname(header))`"
    end

    open(dest, "w") do io
        println(io, "const libtao_include = \"$(dirname(header))\"")
        println(io, "const libtao_library = \"$library\"")
        println(io, "const libtao_version = v\"$version\"")
    end
end

# Run the build script.
haskey(ENV, "TAO_INCDIR") || error("environment variable `TAO_INCDIR` is not defined")
haskey(ENV, "TAO_LIBDIR") || error("environment variable `TAO_LIBDIR` is not defined")
build_deps(joinpath(@__DIR__, "deps.jl");
           incdir = ENV["TAO_INCDIR"],
           libdir = ENV["TAO_LIBDIR"])
