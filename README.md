# Julia interface to Tao real-time framework

This package implements the Julia interface to the real-time framework of Tao
(a *Toolkit for Adaptive Optics*).


## Installation

[TAO real-time libraries](https://git-cral.univ-lyon1.fr/tao/tao-rt) must have
been installed.  Follow instructions there.

Two environment variables, `TAO_INCDIR` and `TAO_LIBDIR`, have to be defined to
indicate where are TAO header files and TAO libraries.  They can be defined
in your shell or in Julia.   For instance for a Bourne-like shell:

```.sh
export TAO_INCDIR=/path/to/TAO/headers
export TAO_LIBDIR=/path/to/TAO/libraries
```

where `/path/to/TAO/headers` and `/path/to/TAO/libraries` are the directories
where are installed the headers and compiled libraries of TAO.  For a C-shell,
this becomes:

```.csh
setenv TAO_INCDIR /path/to/TAO/headers
setenv TAO_LIBDIR /path/to/TAO/libraries
```

From Julia (these commands may be added to your `~/.julia/config/startup.jl`
file):

```julia
ENV["TAO_INCDIR"] = "/path/to/TAO/headers"
ENV["TAO_LIBDIR"] = "/path/to/TAO/libraries"
```

Then the easiest way to install `TaoRT` is via Julia registry
[`EmmtRegistry`](https://github.com/emmt/EmmtRegistry):

```julia
using Pkg
pkg"registry add https://github.com/emmt/EmmtRegistry"
pkg"add TaoRT"
pkg"build TaoRT"
```
