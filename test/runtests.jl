module TestingTaoRT

using Test
using TaoRT
using TaoRT:
    LockMode, READ_ONLY, READ_WRITE, UNLOCKED,
    #SharedCamera,
    SharedObject,
    RemoteObject,
    RWLockedObject,
    ShmId,
    HighResolutionTime,
    TimeVal,
    detach,
    normalize

@testset "Times         " begin
    ts = current_time()
    @test isa(ts, HighResolutionTime)
    tv = TimeVal(ts)
    @test isa(tv, TimeVal)
    @test abs((tv.sec - ts.sec)*1_000_000_000 + (tv.usec*1_000 - ts.nsec)) ≤ 500
    @test isa(HighResolutionTime(tv), HighResolutionTime)
    @test TimeVal(HighResolutionTime(tv)) === tv
    t = current_time()
    @test ts === +ts
    @test ts === -(-ts)
    @test ts - ts === HighResolutionTime(0,0)
    sec, nsec = ts.sec, ts.nsec + 3_000_000_000
    t = HighResolutionTime(sec, nsec)
    @test t.sec == sec && t.nsec == nsec
    t = normalize(t)
    @test t.sec == ts.sec + 3 && t.nsec == ts.nsec
    dt = t - ts
    @test dt.sec == 3 && dt.nsec == 0
    @test ts + dt === t
    @test float(dt) == 3
    @test Float32(dt) === Float32(3)
    @test Float64(dt) === Float64(3)
    @test isa(BigFloat(dt), BigFloat) && BigFloat(dt) == 3
    dt = ts - t
    @test dt.sec == -3 && dt.nsec == 0
    @test ts - dt === t
end

@testset "Shared Arrays " begin
    T = Int
    dims = (2,3,4)
    A = SharedArray{T}(dims)
    @test eltype(A) === T
    @test size(A) === dims
    @test IndexStyle(A) === IndexLinear()
    @test typeof(A.shmid) === ShmId
    @test !islocked(A)
    @test A.lock === UNLOCKED
    @test all(x -> x == zero(x), A) # shared memory is initially zero-filled
    for i in eachindex(A)
        A[i] = rand(T)
    end
    @test_throws Exception A.type = zero(A.type) # read-only property
    @test_throws Exception A.gizmo = 3 # non-existing property
    @test_throws TaoError SharedArray{T}(2,-3)
    @test_throws Exception SharedArray{Bool}(A.shmid)

    B = SharedArray{T}(A.shmid)
    C = SharedArray(A.shmid)
    for X in (B, C)
        @test eltype(X) === T
        @test size(X) === dims
        @test IndexStyle(X) === IndexLinear()
        @test typeof(X.shmid) === ShmId
        @test X.serial == A.serial
        @test X.shmid == A.shmid
        @test X.size == A.size
        for i in eachindex(X.timestamps)
            @test X.timestamps[i] == A.timestamps[i]
        end
        @test X.type == A.type
        @test X == A
    end

    @test try_rdlock(A) === true
    @test islocked(A)
    @test try_rdlock(B) === true
    @test_throws ErrorException wrlock(B; timeout=0) # B is already locked
    unlock(B)
    @test wrlock(B; timeout=0.01) === false
    @test rdlock(C; timeout=0.01)
    @test wrlock(B; timeout=0.01) === false
    unlock(A)
    @test wrlock(B; timeout=0.01) === false
    unlock(C)
    @test wrlock(B; timeout=0.01) === true
    @test rdlock(A; timeout=0.01) === false
    @test wrlock(A; timeout=0.01) === false
    unlock(B)
    @test rdlock(A; timeout=0.01) === true
    @test rdlock(B; timeout=0.01) === true
    @test rdlock(C; timeout=0.01) === true
    unlock(A)
    unlock(B)
    unlock(C)
end

end # module
