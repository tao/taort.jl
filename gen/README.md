# Wrapping headers

This directory contains a script that can be used to automatically generate
wrappers from the C headers of the TAO real time library. This is done using
`Clang.jl`.

## Usage

Either run `julia --project make.jl` directly, or include it and call the
`Wrapper.make()` function. Be sure to activate the project environment in this
folder (`julia --project`), which will install `Clang.jl`. Note that it is
expected that file [`deps.jl`](../deps/deps.jl) exists. See instructions
[here](../deps/README.md) to build or update this file.

After having automatically generated the `gen/wrap.jl` file, copy it in place
of `src/wrap.jl` and make any required changes before committing this new
version of the wrappers.
