module Wrapper

using Libdl
using Clang.Generators
using Clang.LibClang.Clang_jll

let file = normpath(joinpath(@__DIR__, "..", "deps", "deps.jl"))
    isfile(file) || error(
        "File \"$file\" does not exists. ",
        "Run `using Pkg; Pkg.build(\"$(@__MODULE__)\");` to build it.")
end
include(joinpath("..", "deps", "deps.jl"))

# We wrap everything into a function to avoid having undefined variables...
function make()
    # Header files.
    headers = map(x -> joinpath(libtao_include, x), [
        #"tao-alpao.h",
        #"tao-andor.h",
        "tao-arrays.h",
        #"tao-arrays-private.h",
        "tao-attributes.h",
        "tao-basics.h",
        "tao-buffers.h",
        #"tao-camera-servers.h",
        #"tao-cameras.h",
        #"tao-cameras-private.h",
        #"tao-coaxpress.h",
        "tao-config.h",
        "tao-encodings.h",
        "tao-errors.h",
        #"tao-fits.h", # FIXME needs libtao-fits and cfitsio
        #"tao-generic.h",
        "tao-layouts.h", # FIXME not needed?
        #"tao-macros.h",
        #"tao-options.h",
        #"tao-phoenix.h",
        #"tao-phoenix-options.h",
        #"tao-phoenix-private.h",
        #"tao-pixels.h",
        "tao-remote-cameras.h",
        #"tao-remote-cameras-private.h",
        "tao-remote-mirrors.h",
        #"tao-remote-mirrors-private.h",
        "tao-remote-objects.h",
        #"tao-remote-objects-private.h",
        "tao-remote-sensors.h",
        #"tao-remote-sensors-private.h",
        "tao-rwlocked-objects.h",
        #"tao-rwlocked-objects-private.h",
        #"tao-semaphores.h",
        "tao-sensors.h",
        "tao-shared-arrays.h",
        #"tao-shared-arrays-private.h",
        "tao-shared-memory.h",
        #"tao-shared-objects-private.h",
        "tao-shared-objects.h",
        #"tao-test-preprocessing.h",
        #"tao-threadpools.h",
        #"tao-threads.h",
        "tao-time.h",
        #"tao-utils.h"
    ])

    # List of (unique and in order) include directories.
    include_dirs = String[]
    for dir ∈ Iterators.map(dirname, headers)
        dir ∈ include_dirs || push!(include_dirs, dir)
    end

    # The rest is pretty standard.
    cd(@__DIR__)
    options = load_options(joinpath(@__DIR__, "generator.toml"))
    args = get_default_args()
    for dir in include_dirs
        push!(args, "-I$dir")
    end
    push!(args, "-DJULIA_WRAPPER") # NOTE: define macro to account for Julia
    ctx = create_context(headers, args, options)
    build!(ctx)

    # Rewrite destination file.
    dest_file = options["general"]["output_file_path"]
    code = readlines(dest_file)
    for repl in [
        r"^\s*struct\s+tao_error\s*$" =>
            "struct tao_error <: Exception",
        r"\bTAO_TIME\(" =>
            "tao_time(",
        r"\bTAO_MAX_(UN)?SIGNED_INT\(" =>
            "typemax(",
        r"\bTAO_MIN_(UN)?SIGNED_INT\(" =>
            "typemin(",
        r"\bTAO_ENCODING_[2-4]_\b" =>
            "TAO_ENCODING",

        # FIXME The following can only work for mutable structures and mutable
        #       structures would not nest in Julia as they do in C.
        #=
        # Atomic fields.
        r"^(\s*)atomic_(\w+\s*::)" => s"\1@atomic \2",
        =#

        # Make opaque structures abstract types, remove trailing underscore in
        # struct/enum, and avoid aliases such as `const tao_xxx = tao_xxx_`
        # aliases.
        r"^\s*struct\s+(\w+?)_?\s*$" => s"struct \1",
        r"^\s*(@c?enum)\s+(\w+?)_?\s*(::.*?)\s*$" => s"\1 \2\3",
        r"^\s*mutable\s+struct\s+(\w+?)_?\s+end\s*$" => s"abstract type \1 end",
        r"^\s*const\s+(\w+)\s*=\s*\1_*$" => "",
        r"\b(tao_\w+)_\b" => s"\1",

        # Rewrite typemin/typemax constants.
        r"^\s*const\s+(TAO_(U?)INT([0-9]+)_MAX)\s*=.*" =>
            s"const \1 = typemax(\2Int\3)",
        r"^\s*const\s+(TAO_(U?)INT([0-9]+)_MIN)\s*=.*" =>
            s"const \1 = typemin(\2Int\3)",
        # NOTE: Make the tao_time structure inherit from Tao.AbstractTimeValue.
        r"^\s*struct\s+(tao_time_?)\s*$" =>
            s"struct \1 <: Tao.AbstractTimeValue",
        ]
        for i in eachindex(code)
            code[i] = replace(code[i], repl)
        end
    end
    open(dest_file, "w") do io
        flag = false
        for line in code
            if isempty(line)
                # Skip line if previous line(s) were also empty.
                flag && continue
                flag = true
            else
                flag = false
            end
            println(io, line)
        end
    end
end
end # module

# Use the file as a script with `julia --project make.jl`
if abspath(PROGRAM_FILE) == @__FILE__
    Wrapper.make()
end
