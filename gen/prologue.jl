import Tao

let file = normpath(joinpath(@__DIR__, "..", "deps", "deps.jl"))
    isfile(file) || error(
        "File \"$file\" does not exists. ",
        "Run `using Pkg; Pkg.build(\"TaoRT\");` to build it.")
end
include(joinpath("..", "deps", "deps.jl"))

const libtao = libtao_library

TAO_ENCODING(col,pxl) = TAO_ENCODING(col,pxl,pxl)

TAO_ENCODING(col,pxl,pkt) = ((tao_encoding(col) << 16) |
                             (tao_encoding(pkt) <<  8) | tao_encoding(pxl))

TAO_ENCODING(col,pxl,pkt,flg) = ((tao_encoding(flg) << 24) |
                                 TAO_ENCODING(col,pxl,pkt))

TAO_ENCODING_BITS_PER_PIXEL(enc) = (tao_encoding(enc) & TAO_ENCODING_MASK)

TAO_ENCODING_BITS_PER_PACKET(enc) = ((tao_encoding(enc) >> 8) &
                                     TAO_ENCODING_MASK)

TAO_ENCODING_COLORANT(enc) = ((tao_encoding(enc) >> 16) & TAO_ENCODING_MASK)

TAO_ENCODING_FLAGS(enc) = ((tao_encoding(enc) >> 24) & TAO_ENCODING_MASK)

TAO_ENCODING_RAW(pxl) = TAO_ENCODING(TAO_COLORANT_RAW, pxl)

TAO_ENCODING_RAW_PKT(pxl, pkt) = TAO_ENCODING(TAO_COLORANT_RAW, pxl, pkt)

TAO_ENCODING_MONO(pxl) = TAO_ENCODING(TAO_COLORANT_MONO, pxl)

TAO_ENCODING_MONO_PKT(pxl, pkt) = TAO_ENCODING(TAO_COLORANT_MONO, pxl, pkt)

TAO_ENCODING_RGB(pxl) = TAO_ENCODING(TAO_COLORANT_RGB, pxl)

TAO_ENCODING_RGB_PKT(pxl, pkt) = TAO_ENCODING(TAO_COLORANT_RGB, pxl, pkt)

TAO_ENCODING_BGR(pxl) = TAO_ENCODING(TAO_COLORANT_BGR, pxl)

TAO_ENCODING_BGR_PKT(pxl, pkt) = TAO_ENCODING(TAO_COLORANT_BGR, pxl, pkt)

TAO_ENCODING_ARGB(pxl) = TAO_ENCODING(TAO_COLORANT_ARGB, pxl)

TAO_ENCODING_ARGB_PKT(pxl, pkt) = TAO_ENCODING(TAO_COLORANT_ARGB, pxl, pkt)

TAO_ENCODING_RGBA(pxl) = TAO_ENCODING(TAO_COLORANT_RGBA, pxl)

TAO_ENCODING_RGBA_PKT(pxl, pkt) = TAO_ENCODING(TAO_COLORANT_RGBA, pxl, pkt)

TAO_ENCODING_ABGR(pxl) = TAO_ENCODING(TAO_COLORANT_ABGR, pxl)

TAO_ENCODING_ABGR_PKT(pxl, pkt) = TAO_ENCODING(TAO_COLORANT_ABGR, pxl, pkt)

TAO_ENCODING_BGRA(pxl) = TAO_ENCODING(TAO_COLORANT_BGRA, pxl)

TAO_ENCODING_BGRA_PKT(pxl, pkt) = TAO_ENCODING(TAO_COLORANT_BGRA, pxl, pkt)

TAO_ENCODING_BAYER_RGGB(pxl) = TAO_ENCODING(TAO_COLORANT_BAYER_RGGB, pxl)

TAO_ENCODING_BAYER_GRBG(pxl) = TAO_ENCODING(TAO_COLORANT_BAYER_GRBG, pxl)

TAO_ENCODING_BAYER_GBRG(pxl) = TAO_ENCODING(TAO_COLORANT_BAYER_GBRG, pxl)

TAO_ENCODING_BAYER_BGGR(pxl) = TAO_ENCODING(TAO_COLORANT_BAYER_BGGR, pxl)

const TAO_NATIVE_ENDIAN_BOM = ENDIAN_BOM
