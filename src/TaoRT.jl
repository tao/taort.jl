#
# TaoRT.jl --
#
# Provide the Julia interface to the C libraries of TAO, a Toolkit for Adaptive
# Optics.
#
#------------------------------------------------------------------------------
#
# This file is part of TAO software (https://git-cral.univ-lyon1.fr/tao)
# licensed under the MIT license.
#
# Copyright (C) 2018-2023, Éric Thiébaut.
#

module TaoRT

export
    # Top-level module.
    Tao,

    # Macros.
    @enc_str,
    @autolock, @lock, @unlock, @rdlock, @wrlock,
    @try_lock, @try_rdlock, @try_wrlock,

    # Constants.
    BAD_SHMID,

    # Types.
    CameraConfig,
    HighResolutionTime, # FIXME: -> TaoTime
    TaoError,
    #OverwriteError, # FIXME: -> TaoOverwriteError
    RemoteCamera,
    RemoteMirror,
    SharedArray,
    ShmId,
    #TimeoutError,
    MirrorDataFrame,
    PixelEncoding,

    # Methods.
    abort,
    attach,
    autolock,
    configure,
    current_time,
    fetch!,
    #get_image_shmid,
    monotonic_time,
    send,
    #allocate_reference,
    #get_reference,
    #get_reference!,
    set_reference,
    set_perturbation,
    start,
    stop,
    rdlock,
    reset,
    try_lock, # FIXME: trylock?
    try_rdlock,
    try_wrlock,
    wait_command,
    wait_output,
    wrlock

using Tao, EasyFITS, Statistics, Printf, TypeUtils

using Tao:
    AbstractCamera,
    AbstractDeformableMirror,
    CameraOutput,
    TimeLimit, TimeValue, Forever, forever

using Base.Libc: TimeVal

import Tao:
    # Methods for remote objects.
    abort, configure, kill, reset, send, start, stop,
    wait_command, wait_output, layout

# Pre-defined public methods so they can be extended by sub-modules.
function allocate_reference end
function attach end
function autolock end
function fetch! end
function get_reference end
function get_reference! end
function rdlock end
function set_perturbation end
function set_reference end
function try_lock end
function try_rdlock end
function try_wrlock end
function wrlock end

# Macros. NOTE `@lock` is already in `Base`.
macro autolock end
macro rdlock end
macro try_lock end
macro try_rdlock end
macro try_wrlock end
macro unlock end
macro wrlock end

# Load the low level interface to the TAO library.
include("wrap.jl")

include("types.jl")
include("times.jl")
include("private.jl")
#include("diagnostics.jl")
include("servers.jl")

"""
    enc"..."

string macro to specify pixel encoding literally.

"""
macro enc_str(str)
    PixelEncoding(str)
end

end # module
