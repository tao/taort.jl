# errors.jl --
#
# Management of errors for the Julia interface to the real-time C library of
# TAO, a Toolkit for Adaptive Optics.
#
#------------------------------------------------------------------------------
#
# This file is part of TAO software (https://git-cral.univ-lyon1.fr/tao)
# licensed under the MIT license.
#
# Copyright (C) 2018-2022, Éric Thiébaut.
#

"""
    TaoRT.Priv.check(status::TaoRT.Lib.tao_status)

deals with error reporting after calling a function of the TAO Library that
returns a result `status` of C type `tao_status` (equivalent to Julia type
`TaoRT.Lib.tao_status`). The call is considered as successful if `status`
is equal to `TaoRT.Lib.TAO_OK`; otherwise, if `status` is equal
`TaoRT.Lib.TAO_TIMEOUT`, a timeout exception is thrown; otherwise, a TAO
exception is thrown.

"""
function check(status::tao_status)
    if status !== TAO_OK
        if status === TAO_TIMEOUT
            throw_timeout()
        else
            throw_last_error()
        end
    end
    return nothing
end

"""
    TaoRT.Priv.check(success::Bool)

deals with error reporting after calling a function of the TAO Library.  The
call is considered as successfulif `success` is `true`; otherwise, a TAO
exception is thrown.

"""
function check(success::Bool)
    if !success
        throw_last_error()
    end
    return nothing
end

"""
    TaoRT.Priv.check(shmid::ShmId) -> shmid

throws the last TAO Library error if `shmid` value indicates that an error has
occured; otherwise returns `shmid`.

"""
function check(shmid::ShmId)
    if shmid.val == TAO_BAD_SHMID
        throw_last_error()
    end
    return shmid
end

"""
    TaoRT.Priv.check(ptr::Ptr) -> ptr

throws the last TAO Library error if `ptr` is null; otherwise returns `ptr`.

"""
function check(ptr::Ptr)
    if isnull(ptr)
        throw_last_error()
    end
    return ptr
end

"""
    TaoRT.Priv.check_command_serial(n) -> n

raises an exception if command serial number `n` indicates an error, otherwise
just returns `n`. The number `n` must be the result of a call to a TAO C
Library function.

"""
function check_command_serial(n::Serial)
    if n > zero(n)
        return n
    elseif iszero(n)
        throw_timeout()
    else
        throw_last_error()
    end
end

"""
    TaoRT.Priv.throw_timeout()

throws a timeout exception.

"""
throw_timeout() = throw(Tao.TimeoutError())

"""
    TaoRT.Priv.throw_last_error()

throws an exception corresponding to last error that occurred in TAO C library.

"""
@noinline throw_last_error() =
    throw(unsafe_load(tao_get_last_error()))

"""
    TaoRT.Priv.clear_error()

clears the last TAO library error that may have occurred in the calling thread.

"""
clear_error() = tao_clear_error(C_NULL)

function Base.print(io::IO, err::tao_error)
    reason = Ref{Cstring}()
    info = Ref{Cstring}()
    buf = Array{Cchar}(undef, 20)
    GC.@preserve buf begin
        tao_retrieve_error_details(
            err.code, reason, info, err.proc, pointer(buf))
        print(io, unsafe_string(reason[]), " in `",
              unsafe_string_with_default(err.func, "unknown_function"),
              "` [", unsafe_string(info[]), ']')
    end
end

Base.showerror(io::IO, err::tao_error) = print(io, "TAO Error: ", err)

unsafe_string_with_default(ptr::Union{Ptr{Int8},Ptr{UInt8}}, def::String) =
    isnull(ptr) ? def : unsafe_string(ptr)

unsafe_string_with_default(str::Union{Cstring,Cwstring}, def::String) =
    isnull(str) ? def : unsafe_string(str)

"""
    TaoRT.Priv.isnull(ptr)

yields whether pointer `ptr` is null.

"""
isnull(ptr::Union{Ptr,Cstring,Cwstring}) = (ptr === null(ptr))

"""
    TaoRT.Priv.null(ptr)

yields a null pointer of same type as `ptr` (if it is a pointer instance) or of
type `ptr` (if it is a pointer type).

"""
null(ptr::Union{Ptr,Cstring,Cwstring}) = null(typeof(ptr))
null(T::Type{<:Ptr}) = T(0)
null(::Type{Cstring}) = Cstring(C_NULL)
null(::Type{Cwstring}) = Cwstring(C_NULL)

assertion_error(mesg::AssertionError.types[1]) = throw(AssertionError(mesg))
@noinline assertion_error(args...) = assertion_error(string(args...))

argument_error(mesg::ArgumentError.types[1]) = throw(ArgumentError(mesg))
@noinline argument_error(args...) = argument_error(string(args...))
