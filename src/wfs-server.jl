using Tao, TaoRT

# Default settings.
owner = "wfs0"
nbufs = 10_000
max_ninds = 12*12
max_nsubs = ceil(Int, max_ninds*π/4)
flags = 0
time_limit = 10.0 # max. number of seconds to wait

# Parse command line.
# FIXME: to be done

abstract type TimeoutPolicy end
struct IgnoreTimeouts <: TimeoutPolicy end
struct ThrowTimeouts <: TimeoutPolicy end
struct TimeoutsYield{T} <: TimeoutPolicy
    val::T
end
timeout(::IgnoreTimeouts) = nothing
timeout(::ThrowTimeouts) = TaoRT.Priv.throw_timeout()
timeout(on_timeout::TimeoutsYield) = on_timeout.val
function Base.trylock(f::Function, obj::AnySharedObject,
                      on_timout::TimeoutsPolicy = ThrowTimeouts())
    try_lock(obj) || return timeout(on_timeout)
    try
        f()
    finally
        unlock(obj)
    end
end

abstract type ErrorPolicy end
struct IgnoreErrors <: ErrorPolicy end
struct ThrowErrors <: ErrorPolicy end
struct ErrorsYield{T} <: ErrorPolicy
    val::T
end
struct ErrorsCall{F<:Function} <: ErrorPolicy
    func::F
end
Base.error(::IgnoreErrors, args...) = nothing
Base.error(::ThrowErrors, args...) = error(args...)
Base.error(on_error::ErrorsYield, args...) = on_error.val
Base.error(on_error::ErrorsCall, args...) = on_error.func(args...)

const TIMEOUT = Ref(5.0)

# FIXME:
const ServerState = TaoRT.Lib.tao_state
const ServerCommand = TaoRT.Lib.tao_command
const COMMAND_NONE = TaoRT.Lib.TAO_COMMAND_NONE
const COMMAND_RESET = TaoRT.Lib.TAO_COMMAND_RESET
const COMMAND_SEND = TaoRT.Lib.TAO_COMMAND_SEND
const COMMAND_CONFIG = TaoRT.Lib.TAO_COMMAND_CONFIG
const COMMAND_START = TaoRT.Lib.TAO_COMMAND_START
const COMMAND_STOP = TaoRT.Lib.TAO_COMMAND_STOP
const COMMAND_ABORT = TaoRT.Lib.TAO_COMMAND_ABORT
const COMMAND_KILL = TaoRT.Lib.TAO_COMMAND_KILL

"""
    TaoRT.Servers.publish_shmid(obj)

writes the shared memory identifier of the shared object `obj` to the public
configuration so that others (that is *clients*) can connect to it.

!!! warning
    This function shall only be called by the owner (that is the *server*) of
    the shared object.

"""
function publish_shmid(obj::AnyRemoteObject)
    on_error("not yet implemented")
end

"""
    TaoRT.Servers.take_config!(obj, on_error=error) -> bool

checks the proposed configuration via the shared object `obj` and, if it is
valid make it the current one, otherwise, calls the `on_error` function to
handle the error.

The returned value indicates whether any changes have been made. If the
`on_error` callback does not throw, a value is returned even in case of errors
(other may have been changes or not). If unsure, always assume that something
has changed.

!!! warning
    The caller must own an exclusive lock on `obj` and this function shall only
    be called by the owner (that is the *server*) of the shared object, clients
    may only send `config` commands.

"""
function take_config!(obj::AnyRemoteObject; on_error=error)
    on_error("not yet implemented")
end

"""
    TaoRT.Servers.set_state!(obj, state)

sets the state of the shared object `obj`. If the state changes, others are
notified (there are no needs to call `TaoRT.signal_condition` or
`TaoRT.broadcast_condition`). If the object is unreachable, it is an error if
`state` is not `:unreachable`.

!!! warning
    The caller should be the owner of the object and must have locked the
    object for exclusive (read-write) access. Clients may only read the state,
    e.g. with `obj.state`.

"""
set_state!(obj::AnyRemoteObject, sym::Symbol) = ServerState(sym)
function set_state!(obj::AnyRemoteObject, state::ServerState)
    error("not yet implemented")
end

"""
    TaoRT.Servers.pop_command!(obj) -> cmd

This function performs the following operations on the remote object `obj`:

- If the object is reachable and has a pending command, the pending command is
  cleared from the object and the command counter of the object is incremented.

- If the the object is modified, others are notified whatever the object state.

- It the object is reachable and had a pending command, this pending command
  is returned; otherwise, `TAO_COMMAND_NONE` is returned.

!!! warning
    The caller should be the owner of the object and must have locked the
    object for exclusive (read-write) access.

""" pop_command!
function pop_command!(obj::AnyRemoteObject)
    command = Ref{ServerCommand}(:none)
    check(priv.$cfunc(obj, command))
    return command
end

"""
    get_processing_parameters(obj) -> params

extracts the current processing parameters from the shared object `obj`.

!!! warning
    The caller must own an exclusive lock on `obj`.

"""
function get_processing_parameters(obj::AnyRemoteObject)
    params = error("not yet implemented")
    return params
end

"""
    set_processing_parameters!(obj, params)

sets the processing parameters into the shared object `obj`.

!!! warning
    The caller must own an exclusive lock on `obj`.

"""
function set_processing_parameters!(obj::AnyRemoteObject, params)
    error("not yet implemented")
end

"""
    wait_inputs(srv, lim = forever) -> data

waits for the next data frame from remote object `input`. May throw a timeout
exception.

"""
function wait_inputs(srv::AbstractServer, lim::TimeLimit = forever)
    inputs = error("not yet implemented")
    return inputs
end

"""
    process_inputs!(srv::AbstractServer, inputs) -> ouputs

processes the input data and yields output data.

"""
function process_inputs(srv::AbstractServer, inputs)
    outputs = error("not yet implemented")
    return outputs
end

"""
    post_outputs(srv::AbstractServer, outputs)

posts the ouput data for the clients or send them to another server.

"""
function post_outputs(srv::AbstractServer, outputs)
     error("not yet implemented")
end

"""
    exec_postprocessing(srv::AbstractServer)

performs any post-processing after posting ouput data and before waiting for
next input data.

"""
function exec_postprocessing(srv::AbstractServer)
     error("not yet implemented")
end

"""
    do_work(obj)

entry point to the work part of the server.

"""
function do_work end

@noinline report_error(args...) = report_error(string(args...))
@noinline function report_error(mesg::AbstractString)
    printstyled(stderr, "ERROR: "; color=:red, bold=true)
    printstyled(stderr, mesg; color=:red)
end

function run_server(owner::AbstractString, nbufs, max_ninds, max_nsubs, flags;
                    kwds...)
    # Create shared server resources.
    shared = RemoteSensor(owner, nbufs, max_ninds, max_nsubs, flags)
    set_state!(shared, :initializing)
    run_server(shared; kwds...)
end

# First stage: attach to the camera.
function work(shared::RemoteSensor)
    owner = get_camera_owner(shared)
    cam = @unlock shared RemoteCamera(owner)
    work(shared, cam)
end

# Second stage: build wavefront sensor.
# Third stage: start camera and run processing loop
function work(shared::RemoteSensor, cam::RemoteCamera)
    cfg = get_wfs_config(shared)
    wfs = @unlock shared WavefrontSensor(cfg, cam)
     # Start acquisition by the camera.
    @unlock shared start(cam, TIMEOUT[])
    # Call work with the task to perform at each iteration of the working loop.
    work(shared) do # FIXME avoid closure!
        # Retrieve processing parameters before waiting for next input.
        params = get_processing_parameters(shared)
        @unlock shared begin
            # Wait for next image and extract measurements with most up-to-date
            # processing parameters if possible.
            inputs = wait_inputs(worker, TIMEOUT[])
            @try_lock shared IgnoreTimouts() begin
                params = get_processing_parameters(shared)
            end
            outputs = process_inputs(worker, params, inputs)
        end
        post_outputs(worker, params, outputs)
        @unlock shared exec_postprocessing(worker, params)
   end
end

struct WavefrontWorker{C<:RemoteCamera,
                       W<:WavefrontSensor} <: AbstractWorker
    shared::RemoteSensor
    cam::C
    wfs::W
end
