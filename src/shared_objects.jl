#
# shared_objects.jl -
#
# Implementation of basic shared objects for the Julia interface to the real-time C
# library of TAO, a Toolkit for Adaptive Optics.
#
# This file is part of TAO software (https://git-cral.univ-lyon1.fr/tao) licensed under
# the MIT license.
#
# Copyright (c) 2018-2024, Éric Thiébaut.
#

import ..TaoRT: SharedObject

# Replace symbolic value by unique type for dispatching at compile time and
# deal with non-existing properties.
getproperty(obj::Shared, sym::Symbol) = getproperty(obj, Val(sym))
getproperty(obj::Shared, ::Val{sym}) where {sym} =
    throw_non_existing_property(obj, sym)
@noinline throw_non_existing_property(obj, sym::Symbol) =
    error("objects of type `", typeof(obj), "` have no property `", sym, "`")


setproperty!(obj::Shared, sym::Symbol, val) = setproperty!(obj, Val(sym), val)
setproperty!(obj::Shared, ::Val{sym}, val) where {sym} =
    throw_non_existing_or_read_only_property(obj, sym)
@noinline function throw_non_existing_or_read_only_property(obj, sym::Symbol)
    sym ∈ propertynames(obj) || throw_non_existing_property(obj, sym)
    error("property `", sym, "` is read-only for object of type `", typeof(obj), "`")
end

propertynames(obj::SharedObject) = (
    :flags,
    :lock,
    :perms,
    :shmid,
    :size,
    :type)

getproperty(obj::Shared, ::Val{:flags}) = tao_get_flags(obj)
getproperty(obj::Shared, ::Val{:lock})  = getfield(obj, :lock)
getproperty(obj::Shared, ::Val{:perms}) = tao_get_perms(obj)
getproperty(obj::Shared, ::Val{:shmid}) = tao_get_shmid(obj) |> ShmId
getproperty(obj::Shared, ::Val{:size})  = tao_get_size(obj) |> Int
getproperty(obj::Shared, ::Val{:type})  = tao_get_type(obj)

Base.sizeof(obj::Shared) = obj.size

"""
    TaoRT.SharedObject(id) -> obj

attaches to the TAO shared object identified by `id` the shared memory
identifier of the object.

The returned object implements the `obj.key` syntax with the following
properties:

| Name    | Description                                         |
|:--------|:----------------------------------------------------|
| `flags` | (c) Bitwise flags of the shared object              |
| `lock`  | (p) Type of lock owned by the caller                |
| `perms` | (c) Permissions access bits of the shared object    |
| `shmid` | (c) Shared memory identifier of the object          |
| `size`  | (c) Number of bytes allocated for the shared object |
| `type`  | (c) Type identifier of the shared object            |

- (c) constant member, no needs to lock the object;
- (p) private member, no needs to lock the object.

!!! warning
    Properties should all be considered as read-only by the end-user and never
    directly modified or unexpected behavior may occur.

"""
SharedObject(id::Integer) = SharedObject(ShmId(id))
SharedObject(shmid::ShmId) = build(SharedObject, shmid)

"""
    TaoRT.DEFAULT_PERMS

default flags for granted access of created shared objects.

"""
const DEFAULT_PERMS = Cuint(0o600)

"""
    TaoRT.SharedObject(type, size, flags = TaoRT.DEFAULT_PERMS) -> obj

creates a new TAO shared object.

"""
SharedObject(type::Integer, size::Integer, flags::Integer = DEFAULT_PERMS) =
    build(SharedObject, type, size, flags)

function build(T::Type{<:SharedObject}, shmid::ShmId)
    ptr = tao_shared_object_attach(shmid)
    return build(T, ptr)
end

function build(T::Type{<:SharedObject}, type::Integer, size::Integer, flags::Integer)
    ptr = tao_shared_object_create(type, size, flags)
    return build(T, ptr)
end

function Base.show(io::IO, obj::Shared)
    lock(io)
    try
        print(io, typeof(obj))
        print(io, "\n ├─ shmid: "); show(io, obj.shmid)
        print(io, "\n ├─ type:  "); show(io, obj.type)
        print(io, "\n └─ lock:  "); show(io, obj.lock)
    finally
        unlock(io)
    end
end
"""
    TaoRT.Priv.build(T, ptr) -> obj

yields a TAO object of type `T` given its opaque handle `ptr` using the
`TaoRT.Priv.finalize` method as a finalizer. If `ptr` is `NULL`, the last TAO
error is reported and if any other error occurs, the object is detached before
re-throwing the error.

"""
function build(T::Type{<:AbstractSharedObject}, ptr::Ptr)
    check(ptr)
    try
        obj = T(ptr, UNLOCKED)
        return finalizer(finalize, obj)
    catch ex
        tao_detach(ptr)
        throw(ex)
    end
end

"""
    TaoRT.detach(obj)

detaches shared TAO object `obj` from the data space of the caller. Object is
automatically unlocked if needed. Calling this method makes the object almost
completely unusable; it is generally better to rely on the garbage collector to
finalize an object which automatically detaches it.

"""
detach(obj::Shared) = check(detach(Bool, obj))

"""
    TaoRT.detach(Bool, obj) -> status

detaches TAO object `obj` from the data space of the caller. This does most of
the job of `TaoRT.detach(obj)` except that it returns a status instead of
throwing errors. This is also used to *finalize* a TAO object.

"""
function detach(::Type{Bool}, obj::Shared)
    # NOTE: No needs to preserve the Julia object, the pointer to the opaque
    #       object is valid until we detach it.
    ptr = getfield(obj, :ptr)
    status = TAO_OK
    if islocked(obj)
        # NOTE: It may not be a good idea to unlock automatically because the
        #       object may be dirty. However, most of the time, the object is
        #       locked just for reading, so we unlock it automatically.
        isnull(ptr) || tao_unlock(ptr) == TAO_OK || (status = TAO_ERROR)
        setfield!(obj, :lock, UNLOCKED)
    end
    if !isnull(ptr)
        tao_detach(ptr) == TAO_OK || (status = TAO_ERROR)
        setfield!(obj, :ptr, null(ptr)) # never detach more than once
    end
    return status
end

# Finalizing an object amounts to detaching it but without throwing errors.
function finalize(obj::Shared)
    detach(Bool, obj) == TAO_OK || clear_error()
    nothing
end

"""
    attach(id) -> obj

attaches to the shared TAO object identified by `id` the shared memory
identifier of the object or the name of the server owning the object.

"""
attach(id::Union{Integer,AbstractString}) = attach(ShmId(id))
function attach(shmid::ShmId)
    ptr = check(tao_shared_object_attach(shmid))
    try
        type = tao_get_type(ptr)
        if type == TAO_SHARED_OBJECT
            return build(SharedObject, ptr)
        elseif type == TAO_RWLOCKED_OBJECT
            return build(RWLockedObject, Ptr{tao_rwlocked_object}(ptr))
        elseif type == TAO_REMOTE_OBJECT
            return build(RemoteObject, Ptr{tao_remote_object}(ptr))
        elseif type == TAO_SHARED_ARRAY
            return build(SharedArray, Ptr{tao_shared_array}(ptr))
        elseif type == TAO_REMOTE_CAMERA
            return build(RemoteCamera, Ptr{tao_remote_camera}(ptr))
        elseif type == TAO_REMOTE_MIRROR
            return build(RemoteMirror, Ptr{tao_remote_mirror}(ptr))
        elseif type == TAO_REMOTE_SENSOR
            return build(RemoteSensor, Ptr{tao_remote_sensor}(ptr))
        else
            argument_error("unknown object type $type")
        end
    catch ex
        tao_detach(ptr)
        throw(ex)
    end
end
