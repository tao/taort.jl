#
# private.jl -
#
# Implement the `TaoRT.Priv` sub-module which is a thin layer between public Julia
# interface and the real-time C library of TAO, a Toolkit for Adaptive Optics.
#
# This file is part of TAO software (https://git-cral.univ-lyon1.fr/tao) licensed under
# the MIT license.
#
# Copyright (c) 2018-2024, Éric Thiébaut.
#
module Priv

using Tao, Printf, TypeUtils
using ..Lib
import ..TaoRT
using ..TaoRT:
    # List of types.
    REMOTE_OBJECT_TYPES,
    RWLOCKED_OBJECT_TYPES,
    SHARED_OBJECT_TYPES,

    # Lock modes.
    LockMode, UNLOCKED, READ_ONLY, READ_WRITE,

    # Types, aliases and unions.
    AbstractSharedObject,
    CameraConfig,
    Command,
    DataFrame,
    DataFrameInfo,
    Lockable,
    RWLockable,
    RWLockedObject,
    Remote,
    RemoteCamera,
    RemoteMirror,
    RemoteObject,
    RemoteSensor,
    Serial,
    Shared,
    SharedArray,
    SharedObject,
    ShmId, BAD_SHMID,
    State,

    # Exceptions.
    OverwriteError,
    NullPointer,

    # Times.
    HighResolutionTime

import ..TaoRT:
    # Types.
    ShmId, PixelEncoding, TimeLimit, TimeValue, Forever, forever,

    # Methods.
    attach, fetch!,
    start, stop, abort, send, configure,
    autolock, rdlock, wrlock, try_lock, try_rdlock, try_wrlock,
    allocate_reference, get_reference, get_reference!, set_reference,
    set_perturbation, wait_command, wait_output,

    # Macros.
    @autolock, @unlock, @rdlock, @wrlock,
    @try_lock, @try_rdlock, @try_wrlock

import Base: @lock

using Tao:
    TimeLimit, TimeValue, Forever, forever

import Base:
    detach, fetch, reset, kill,
    getproperty, setproperty!, propertynames,
    lock, unlock, trylock, islocked

# Pseudo-vectors keep a reference on their parent. They can be indexed like vectors
# provided the `Base.getindex` and `Base.setindex!` methods be implemented for their
# specific type.
struct PseudoVector{T,                         # element type
                    R<:AbstractUnitRange{Int}, # range type
                    P,                         # parent type
                    W,                         # unique identifier
                    } <: AbstractVector{T}
    range::R
    parent::P
end
Base.keys(A::PseudoVector) = getfield(A, :range)
Base.parent(A::PseudoVector) = getfield(A, :parent)
Base.length(A::PseudoVector) = length(keys(A))
Base.size(A::PseudoVector) = (length(A),)
Base.axes(A::PseudoVector) = (keys(A),)
Base.IndexStyle(::Type{<:PseudoVector}) = IndexLinear()

include("generic.jl")
include("errors.jl")
include("meta.jl")
include("encodings.jl")
include("locks.jl")
include("shared_objects.jl")
include("remote_objects.jl")
include("rwlocked_objects.jl")
include("shared_arrays.jl")
include("remote_cameras.jl")
include("remote_mirrors.jl")

"""
    ShmId(x)

yields the shared memory identifier corresponding to `x` an integer, a shared
TAO object, or the name of a TAO server.

"""
ShmId(id::ShmId) = id
ShmId(obj::Shared) = obj.shmid
function ShmId(owner::AbstractString; throwerrors::Bool=true)
    shmid = tao_config_read_shmid(owner)
    shmid < 0 && throwerrors && error("server \"", owner, "\" is not running")
    return ShmId(shmid)
end

Base.convert(T::Type{<:Integer}, shmid::ShmId) = convert(T, shmid.val)

"""
    TaoRT.Priv.unsafe_copy!(dst, src)

copies contents of source `src` into that of destination`dst`. One of the
argument is a pointer while the other is a dense array.

"""
unsafe_copy!(dst::DenseArray{T}, src::Ptr{T}) where {T} =
    @ccall memcpy(dst::Ptr{T}, src::Ptr{T},
                  (length(dst)*sizeof(T))::Csize_t)::Ptr{T}

unsafe_copy!(dst::Ptr{T}, src::DenseArray{T}) where {T} =
    @ccall memcpy(dst::Ptr{T}, src::Ptr{T},
                  (length(src)*sizeof(T))::Csize_t)::Ptr{T}

end # module Priv
