# encodings.jl --
#
# Deal with pixel encoding in TAO.
#
#------------------------------------------------------------------------------
#
# This file is part of TAO software (https://git-cral.univ-lyon1.fr/tao)
# licensed under the MIT license.
#
# Copyright (C) 2018-2023, Éric Thiébaut.
#

Base.convert(::Type{PixelEncoding}, x::PixelEncoding) = x
Base.convert(::Type{PixelEncoding}, x) = PixelEncoding(x)

Base.convert(::Type{tao_encoding}, enc::PixelEncoding) = enc.bits

"""
    PixelEncoding(bits)
    PixelEncoding(col, pxl, pkt = pxl, flg=0)

yield an immutable structure wrapping the 32-bit unsigned integer `bits` which
defines the pixel encoding. The pixel encoding is a bitwise combination of up
to 4 bytes `col`, `pxl`, `pkt`, and `flg` stored in a 32-bit unsigned integer:

| Bits  | Description     | Argument |
|:-----:|:----------------|:---------|
| 1-8   | Bits per pixel  | `pxl`    |
| 9-16  | Bits per packet | `pkt`    |
| 17-24 | Color type      | `col`    |
| 25-32 | Flags           | `flg`    |

---
    PixelEncoding(T::DataType)

yields the pixel encoding matching the element type `T`.

"""
PixelEncoding(col::Integer, pxl::Integer) =
    PixelEncoding(TAO_ENCODING(col, pxl))
PixelEncoding(col::Integer, pxl::Integer, pkt::Integer) =
    PixelEncoding(TAO_ENCODING(col, pxl, pkt))
PixelEncoding(col::Integer, pxl::Integer, pkt::Integer, flg::Integer) =
    PixelEncoding(TAO_ENCODING(col, pxl, pkt, flg))

# Define pixel encodings for integer Julia types.
for S in (Int8, Int16, Int32, Int64)
    for (T, col) in ((S,           TAO_COLORANT_SIGNED),
                     (unsigned(S), TAO_COLORANT_UNSIGNED))
        enc = PixelEncoding(col, 8*sizeof(T))
        @eval PixelEncoding(::Type{$T}) = $enc
    end
end

# Define encodings for floating-point Julia types.
for T in (Float16, Float32, Float64)
    enc = PixelEncoding(TAO_COLORANT_FLOAT, 8*sizeof(T))
    @eval PixelEncoding(::Type{$T}) = $enc
end

# Define encodings for TAO array element types.
let
    vals = tao_eltype[]
    encs = PixelEncoding[]
    for (T,val) in ELEM_TYPES
        enc = PixelEncoding(tao_encoding_of_eltype(val))
        @assert PixelEncoding(T) === enc
        push!(vals, val)
        push!(encs, enc)
    end
    mesg = "unexpected value for `tao_eltype` enumeration"
    expr = :(throw(ArgumentError($mesg)))
    for i in reverse(sortperm(vals))
        enc, val = encs[i], vals[i]
        expr = Expr(:if, :(x == $val), enc, expr)
    end
    @eval PixelEncoding(x::tao_eltype) = $expr
end

"""
    DataType(enc::PixelEncoding) -> T

yields the Julia type `T` matching TAO encoding `enc`, or `Nothing` if there is
no exact correspondance.

"""
function Base.DataType(enc::PixelEncoding) :: DataType
    pxl = TAO_ENCODING_BITS_PER_PIXEL(enc)
    pkt = TAO_ENCODING_BITS_PER_PACKET(enc)
    flg = TAO_ENCODING_FLAGS(enc)
    if pkt == pxl && flg == 0
        col = TAO_ENCODING_COLORANT(enc)
        if col == TAO_COLORANT_UNSIGNED
            if     pxl ==   8; return UInt8
            elseif pxl ==  16; return UInt16
            elseif pxl ==  32; return UInt32
            elseif pxl ==  64; return UInt64
            elseif pxl == 128; return UInt128; end
        elseif col == TAO_COLORANT_SIGNED
            if     pxl ==   8; return Int8
            elseif pxl ==  16; return Int16
            elseif pxl ==  32; return Int32
            elseif pxl ==  64; return Int64
            elseif pxl == 128; return Int128; end
        elseif col == TAO_COLORANT_FLOAT
            if     pxl ==  16; return Float16
            elseif pxl ==  32; return Float32
            elseif pxl ==  64; return Float64; end
        end
    end
    return Nothing
end

function Base.String(enc::PixelEncoding)
    buf = Base.StringVector(TAO_ENCODING_STRING_SIZE)
    ptr = pointer(buf)
    status = GC.@preserve buf tao_format_encoding(ptr, enc)
    check(status)
    len = length(buf) - 1
    @inbounds for i in 1:len
        if iszero(buf[i])
            len = i - 1
            break
        end
    end
    String(resize!(buf, len))
end

"""
    PixelEncoding(str::AbstractString)

yields the pixel encoding matching the encoding name in `str` throwing an error
if `str` is invalid.  Call `tryparse(PixelEncoding, str)` to parse string `str`
as a pixel encoding but return `nothing` if `str` is invalid.

Method `DataType(enc)` yields the Julia type matching the encoding `enc` or
`Nothing` if there is no exact correspondance.

"""
PixelEncoding(str::AbstractString) =
    (enc = tryparse(PixelEncoding, str)) === nothing ?
    error("invalid pixel encoding \"$str\"") : enc

Base.tryparse(::Type{PixelEncoding}, str::AbstractString) =
    (bits = tao_parse_encoding(str)) == TAO_ENCODING_UNKNOWN ?
    nothing : PixelEncoding(bits)

Base.parse(::Type{PixelEncoding}, str::AbstractString) = PixelEncoding(str)

Base.show(io::IO, enc::PixelEncoding) = print(io, String(enc))
Base.show(io::IO, ::MIME"text/plain", enc::PixelEncoding) =
    print(io, "enc\"", String(enc), "\"")
