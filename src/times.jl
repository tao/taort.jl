#
# times.jl -
#
# Methods related to time for the Julia interface to the real-time C library of
# TAO, a Toolkit for Adaptive Optics.
#
#------------------------------------------------------------------------------
#
# This file is part of TAO software (https://git-cral.univ-lyon1.fr/tao)
# licensed under the MIT license.
#
# Copyright (C) 2018-2023, Éric Thiébaut.
#

using Base.Libc: TimeVal
using Tao: build, convert_time, resolution, time_secs, time_frac, normalize
import .Lib: tao_time

# Implement time value API of TAO.
Tao.resolution(::Type{HighResolutionTime}) = 1_000_000_000
let type = HighResolutionTime
    @eval Tao.build(::Type{$type}, ip::Integer, fp::Integer) = $type(ip, fp)
    for (f, i) in ((:time_secs, 1), (:time_frac, 2))
        @eval Tao.$f(t::$type) = getfield(t, $i)
        @eval Tao.$f(::Type{$type}) = fieldtype($type, $i)
    end
end

function Base.show(io::IO, t::HighResolutionTime)
    t = normalize(t)
    @printf io "%d.%09ds" t.sec t.nsec
end

"""
    TaoRT.current_time()

yields the current time since the
[Epoch](https://en.wikipedia.org/wiki/Unix_time), that is 00:00:00 UTC, 1st of
January 1970.

See also [`TaoRT.monotonic_time`](@ref).

"""
function current_time()
    t = Ref{HighResolutionTime}()
    Priv.check(Lib.tao_get_current_time(t))
    return t[]
end

"""
    TaoRT.monotonic_time()

yields a monotonic time since some unspecified starting point but which is not
affected by discontinuous jumps in the system time (e.g., if the system
administrator manually changes the clock), but is affected by the incremental
adjustments performed by `adjtime` and NTP.

See also [`TaoRT.current_time`](@ref).

"""
function monotonic_time()
    t = Ref{HighResolutionTime}()
    Priv.check(Lib.tao_get_monotonic_time(t))
    return t[]
end

# Extend `HighResolutionTime` constructor. Extend `convert` for conversion to
# time structure. Do not do that for real argument and avoid type-piracy for
# `TimeVal`.
HighResolutionTime(t::HighResolutionTime) = t
HighResolutionTime(t) = convert_time(HighResolutionTime, t)
function Base.convert(::Type{T},
                      t::Union{TimeVal,Real,
                               NTuple{2,Integer}}) where {T<:HighResolutionTime}
   T(t)
end
Base.convert(::Type{T}, t::T) where {T<:HighResolutionTime} = t

# The following is for passing a time-compatible value to a C funtion of the
# TAO library.
function Base.cconvert(::Type{Ptr{HighResolutionTime}},
                       t::Union{HighResolutionTime,TimeVal,Real,Forever})
    Ref(convert(tao_time, t))
end

# Converts high resolution time to `TimeVal`.  Other conversions are not
# implemented to avoid type-piracy.
Base.Libc.TimeVal(t::HighResolutionTime) = convert_time(TimeVal, t)
Base.convert(::Type{TimeVal}, t::HighResolutionTime) = TimeVal(t)
