#
# shared_arrays.jl -
#
# Low level interface to shared multi-dimensional arrays for the Julia interface to the
# real-time C library of TAO, a Toolkit for Adaptive Optics.
#
# This file is part of TAO software (https://git-cral.univ-lyon1.fr/tao) licensed under
# the MIT license.
#
# Copyright (c) 2018-2024, Éric Thiébaut.
#

import ..TaoRT: SharedArray

propertynames(arr::SharedArray) = (
    :flags,
    :lock,
    :perms,
    :serial,
    :shmid,
    :size,
    :timestamps,
    :type)

getproperty(A::SharedArray, ::Val{:serial}) = tao_get_serial(A)

setproperty!(A::SharedArray, ::Val{:serial}, n::Integer) = tao_set_serial(A, n)

const TimestampList{A<:SharedArray} =
    PseudoVector{HighResolutionTime,Base.OneTo{Int},A,:timestamps}

getproperty(A::SharedArray, ::Val{:timestamps}) =
    TimestampList{typeof(A)}(Base.OneTo(TAO_SHARED_ARRAY_TIMESTAMPS), A)

function Base.getindex(A::TimestampList, i::Int)
    i ∈ keys(A) || throw(BoundsError(A, i))
    t = Ref{HighResolutionTime}()
    tao_get_timestamp(parent(A), i - 1, t)
    return t[]
end

function Base.setindex!(A::TimestampList, x, i::Int)
    i ∈ keys(A) || throw(BoundsError(A, i))
    tao_set_timestamp(parent(A), i - 1, x)
    return A
end

"""
    SharedArray{T,N}(id) -> arr

attaches shared TAO array whose shared memory identifier is `id` to the
caller's address space and returns an array-like object. Type parameters `T`
and `N` are the type of elements and number of dimensions of the array, these
parameters are optional but the result is not type-stable if any of these is
omitted.

`SharedArray{T,N}` is a concrete sub-type of `DenseArray{T,N}` which includes
all arrays whose elements are stored contiguously in column-major order. TAO
shared arrrays implement the `arr.key` syntax with the following properties:

| Name         | Description                                         |
|:-------------|:----------------------------------------------------|
| `flags`      | (c) Bitwise flags of the shared object              |
| `lock`       | (p) Type of lock owned by the caller                |
| `perms`      | (c) Permissions access bits of the shared object    |
| `serial`     | (s) Serial number of the shared array               |
| `shmid`      | (c) Shared memory identifier of the object          |
| `size`       | (c) Number of bytes allocated for the shared object |
| `timestamps` | (s) Time-stamps  of the shared array                |
| `type`       | (c) Type identifier of the shared object            |

Notes:
- (p) private member, no needs to lock the object;
- (s) shared member, caller shall have locked the object;
- (c) constant member, no needs to lock the object.

!!! warning
    Properties should all be considered as read-only by a *client* and never
    directly modified or unexpected behavior may occur.

"""
SharedArray(shmid::ShmId) = build(SharedArray, shmid)
SharedArray{T}(shmid::ShmId) where {T} = build(SharedArray{T}, shmid)
SharedArray{T,N}(shmid::ShmId) where {T,N} = build(SharedArray{T,N}, shmid)

function build(T::Type{<:SharedArray}, shmid::ShmId)
    ptr = tao_shared_array_attach(shmid)
    return build(T, ptr)
end

# Build a shared array that has been attached by the caller with specified
# element type and number of dimensions.
function build(::Type{SharedArray{T,N}}, ptr::Ptr{tao_shared_array}) where {T,N}
    check(ptr)
    try
        check_eltype(ptr, T)
        check_ndims(ptr, N)
        return unsafe_build(SharedArray{T,N}, ptr)
    catch ex
        tao_detach(ptr)
        throw(ex)
    end
end

# Build a shared array that has been attached by the caller with specified
# element type but unspecified number of dimensions.
function build(::Type{SharedArray{T}}, ptr::Ptr{tao_shared_array}) where {T}
    check(ptr)
    try
        check_eltype(ptr, T)
        N = Int(tao_get_ndims(ptr))::Int
        return unsafe_build(SharedArray{T,N}, ptr)
    catch ex
        tao_detach(ptr)
        throw(ex)
    end
end

# Build a shared array that has been attached by the caller with unspecified
# element type and number of dimensions.
function build(::Type{SharedArray}, ptr::Ptr{tao_shared_array})
    check(ptr)
    try
        type = tao_get_eltype(ptr)
        N = Int(tao_get_ndims(ptr))::Int
        type === TAO_UINT8  && return unsafe_build(SharedArray{UInt8,  N}, ptr)
        type === TAO_INT8   && return unsafe_build(SharedArray{Int8,   N}, ptr)
        type === TAO_UINT16 && return unsafe_build(SharedArray{UInt16, N}, ptr)
        type === TAO_INT16  && return unsafe_build(SharedArray{Int16,  N}, ptr)
        type === TAO_UINT32 && return unsafe_build(SharedArray{UInt32, N}, ptr)
        type === TAO_INT32  && return unsafe_build(SharedArray{Int32,  N}, ptr)
        type === TAO_UINT64 && return unsafe_build(SharedArray{UInt64, N}, ptr)
        type === TAO_INT64  && return unsafe_build(SharedArray{Int64,  N}, ptr)
        type === TAO_FLOAT  && return unsafe_build(SharedArray{Cfloat, N}, ptr)
        type === TAO_DOUBLE && return unsafe_build(SharedArray{Cdouble,N}, ptr)
        argument_error("invalid element type $type for shared array")
    catch ex
        tao_detach(ptr)
        throw(ex)
    end
end

"""
    SharedArray{T,N}(dims...; perms=...) -> arr

creates a new `N`-dimensional shared TAO array with element type `T` and
dimensions `dims`.  Keyword `perms` can be used to specify access permissions
other than having read and write permissions for the creator only.  Type
parameter `N` may be omitted as it can be deduced from `dims`.

"""
SharedArray{T}(dims::Integer...; kwds...) where {T} =
    SharedArray{T}(dims; kwds...)
SharedArray{T}(dims::NTuple{N,Integer}; kwds...) where {T,N} =
    SharedArray{T,N}(dims; kwds...)
SharedArray{T,N}(dims::Integer...; kwds...) where {T,N} =
    SharedArray{T,N}(dims; kwds...)
SharedArray{T,N}(dims::NTuple{N,Integer}; kwds...) where {T,N} =
    SharedArray{T,N}(map(Int, dims); kwds...)
SharedArray{T,N}(dims::Tuple{Vararg{Integer}}; kwds...) where {T,N} =
    argument_error("number of dimensions mismatch")
SharedArray{T,N}(dims::Dims{N}; perms::Integer = DEFAULT_PERMS) where {T,N} =
    build(SharedArray{T,N}, dims, perms)

# Create a new shared array.
function build(::Type{SharedArray{T,N}}, dims::Dims{N}, flags::Integer) where {T,N}
    ptr = check(shared_array_create(T, dims, flags))
    try
        return unsafe_build(SharedArray{T,N}, ptr, dims)
    catch ex
        tao_detach(ptr)
        throw(ex)
    end
end

# Shared array unsafe builders to dispatch on type parameters T and N.
#
# These builders are "unsafe" because it assumes that arguments and parameters
# are correct, must be called in a try-catch block to detach handle on error.
function unsafe_build(::Type{SharedArray{T,N}},
                      ptr::Ptr{tao_shared_array}) where {T,N}
    dims = ntuple(d -> Int(tao_get_dim(ptr, d))::Int, Val(N))
    return unsafe_build(SharedArray{T,N}, ptr, dims)
end

function unsafe_build(::Type{SharedArray{T,N}},
                      ptr::Ptr{tao_shared_array},
                      dims::Dims{N}) where {T,N}
    data = tao_get_data(ptr)
    isnull(data) && assertion_error("invalid address")
    arr = unsafe_wrap(Array, Ptr{T}(data), dims; own = false)
    obj = SharedArray{T,N}(ptr, arr, UNLOCKED)
    return finalizer(finalize, obj)
end

# Helper functions for shared arrays.
check_eltype(ptr::Ptr{tao_shared_array}, T::Type) =
   tao_get_eltype(ptr) === tao_eltype(T) || not_same_eltype(ptr, T)
@noinline not_same_eltype(ptr::Ptr{tao_shared_array}, T::DataType) =
    error("invalid element type for shared array, should be `",
          DataType(tao_get_eltype(ptr)), "` instead of `", T, "`")

check_ndims(ptr::Ptr{tao_shared_array}, N::Integer) =
    tao_get_ndims(ptr) == N || not_same_ndims(ptr, N)
@noinline not_same_ndims(ptr::Ptr{tao_shared_array}, N::Integer) =
    error("invalid number of dimensions for shared array, should be ",
          tao_get_ndims(ptr), " instead of ", N)

shared_array_create(::Type{T}, dims::Dims{1}, flags) where {T} =
    tao_shared_array_create_1d(T, dims..., flags)
shared_array_create(::Type{T}, dims::Dims{2}, flags) where {T} =
    tao_shared_array_create_2d(T, dims..., flags)
shared_array_create(::Type{T}, dims::Dims{3}, flags) where {T} =
    tao_shared_array_create_3d(T, dims..., flags)
shared_array_create(::Type{T}, dims::Dims{N}, flags) where {T,N} =
    tao_shared_array_create(T, N, Ref(map(Clong, dims)), flags)

# Detaching a TAO shared array makes the associated Julia array invalid
# so we replace it with an array of the correct type but with all
# dimensions equal to zero so that type-stability is maintained.
function detach(obj::SharedArray{T,N}) where {T,N}
    setfield!(obj, :arr, Array{T,N}(undef, ntuple(i -> 0, Val(N))))
    check(detach(Bool, obj))
end

# Get the Julia wrapped array associated with a shared array.
Base.parent(obj::SharedArray) = getfield(obj, :arr)

# Make a `TaoRT.SharedArray{T,N}` behaves like an array. Note that, for
# performance reasons, reading/writing the array contents is done without any
# attempt to lock the object before. Locking has to be done appropriately when
# such an object is used.
Base.length(A::SharedArray) = length(parent(A))
Base.size(A::SharedArray) = size(parent(A))
Base.axes(A::SharedArray) = axes(parent(A))
Base.IndexStyle(::Type{<:SharedArray}) = IndexLinear()
@inline function Base.getindex(A::SharedArray, i::Int)
    B = parent(A)
    @boundscheck checkbounds(B, i)
    return @inbounds getindex(B, i)
end
@inline function Base.setindex!(A::SharedArray, x, i::Int)
    B = parent(A)
    @boundscheck checkbounds(B, i)
    @inbounds setindex!(B, x, i)
    return A
end

# The following specializations are provided because calling the method on
# `parent(A)` leads to a method more optimized than the default one for
# abstract arrays. If functions are in-lined, there are no needs to extend
# `similar` which yields an `Array` by default.
Base.reshape(A::SharedArray, dims...) = reshape(parent(A), dims...)
Base.reshape(A::SharedArray, ndims::Val) = reshape(parent(A), ndims)
Base.copy(A::SharedArray) = copy(parent(A))
Base.deepcopy(A::SharedArray) = deepcopy(parent(A))
Base.iterate(A::SharedArray) = iterate(parent(A))

# Extend `unsafe_convert` to make shared arrays usable in other `ccalls` than
# those defined in this package.  This also makes `pointer` yields the base
# address of the shared elements.
Base.unsafe_convert(::Type{Ptr{T}}, A::SharedArray{T}) where {T} =
    Base.unsafe_convert(Ptr{T}, parent(A))
