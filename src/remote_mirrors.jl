#
# remote_mirrors.jl -
#
# Low level interface to remote deformable mirrors and associated shared data
# for the Julia interface to the real-time C library of TAO, a Toolkit for
# Adaptive Optics.
#
#------------------------------------------------------------------------------
#
# This file is part of TAO software (https://git-cral.univ-lyon1.fr/tao)
# licensed under the MIT license.
#
# Copyright (C) 2018-2023, Éric Thiébaut.
#

import ..TaoRT: RemoteMirror, MirrorDataFrame

propertynames(obj::RemoteMirror) = (
    :alive,
    :cmax,
    :cmin,
    :dims,
    :flags,
    :layout,
    :lock,
    :mark,
    :nacts,
    :ncmds,
    :nbufs,
    :owner,
    :perms,
    :pid,
    :serial,
    :shmid,
    :size,
    :state,
    :type)

# Fast getters for non-atomic members, use getters provided by the the TAO C
# library for others.
getproperty(obj::RemoteMirror, key::Val{:cmin})   = tao_get_cmin(obj)
getproperty(obj::RemoteMirror, key::Val{:cmax})   = tao_get_cmax(obj)
getproperty(obj::RemoteMirror, key::Val{:dims})   = size(obj.layout)
getproperty(obj::RemoteMirror, key::Val{:mark})   = tao_get_mark(obj)
getproperty(obj::RemoteMirror, key::Val{:layout}) = getfield(obj, :inds)
getproperty(obj::RemoteMirror, key::Val{:nacts})  = getfield(obj, :nacts)
getproperty(obj::RemoteMirror, key::Val{:nbufs})  = getfield(obj, :nbufs)

# AbstractDeformableMirror API.
Base.length(dm::RemoteMirror) = dm.nacts
Base.size(dm::RemoteMirror) = dm.dims
Base.minimum(dm::RemoteMirror) = dm.cmin
Base.maximum(dm::RemoteMirror) = dm.cmax
Tao.layout(dm::RemoteMirror) = dm.layout
# FIXME: Tao.reference(dm::RemoteMirror) = dm.refcmds
#Base.close(dm::RemoteMirror) = (tao_kill(dm); tao_detach(dm); nothing)

"""
    RemoteMirror(id) -> dm

yields a client object which can be used to communicate with the deformable
mirror server whose name or shared-memory identifier is specified by `id`.

The returned object implements the `dm.key` syntax for the following
properties:

| Name        | Description                                            |
|:------------|:-------------------------------------------------------|
| `alive`     | (a) Whether the server owning the object is alive      |
| `cmax`      | (c) Maximum value for an actuator command              |
| `cmin`      | (c) Minimum value for an actuator command              |
| `flags`     | (c) Bitwise flags of the shared object                 |
| `layout`    | (c) Layout of the actuators                            |
| `lock`      | (p) Type of lock owned by the caller                   |
| `mark`      | (s) Last mark set by a client                          |
| `nacts`     | (c) Number of actuators                                |
| `nbufs`     | (c) Number of entries in cyclic list of output buffers |
| `ncmds`     | (a) Number of commands executed by the server          |
| `owner`     | (c) Name of the server owning the object               |
| `perms`     | (c) Permissions access bits of the shared object       |
| `pid`       | (c) Process identifier of owner                        |
| `serial`    | (a) Number of outputs produced by the server           |
| `shmid`     | (c) Shared memory identifier of the object             |
| `size`      | (c) Number of bytes allocated for the shared object    |
| `state`     | (a) Current server state                               |
| `type`      | (c) Type identifier of the shared object               |

- (a) atomic shared member, no needs to lock the object;
- (c) constant member, no needs to lock the object;
- (s) shared member, caller shall have locked the object;
- (p) private member, no needs to lock the object.

!!! warning
    Properties should all be considered as read-only by the end-user and never
    directly modified or unexpected behavior may occur.

The following commands can be sent to a remote mirror:

- `set_reference(dm, vals, secs=Inf)` changes the reference values for
  subsequent actuators commands, `vals` can be an array or the name of a FITS
  file.

- `set_perturbation(dm, vals, secs=Inf)` sets a perturbation on actuators that
  will be added to the next commands.

- `send(dm, cmds, secs=Inf; mark=0)` sends actuators commands, these commands
  are applied as soon as possible.

- `reset(dm, secs=Inf; mark=0)` resets the deformable mirror, this is exactly
  the same as applying commands `zeros(dm.nacts)`.

- `kill(dm, secs=Inf)` kills the server owning the deformable mirror.

- `dm[i]` or equivalently `fetch(dm, i)` yields the `i`-th output data-frame
  from a remote mirror `dm`. To retrieve the last output data-frame, use `dm[]`
  or call `fetch(dm)`. To avoid allocations, call `fetch!` instead.

"""
RemoteMirror(owner::AbstractString) = RemoteMirror(ShmId(owner))
RemoteMirror(shmid::ShmId) = build(RemoteMirror, shmid)

function build(T::Type{<:RemoteMirror}, shmid::ShmId)
    ptr = tao_remote_mirror_attach(shmid)
    return build(T, ptr)
end

function build(T::Type{<:RemoteMirror}, ptr::Ptr{tao_remote_mirror})
    check(ptr)
    try
        nacts = tao_get_nacts(ptr) |> Int
        nbufs = tao_get_nbufs(ptr) |> Int
        dims = Ref{NTuple{2,Clong}}()
        inds_ptr = tao_get_layout(ptr, dims)
        isnull(inds_ptr) && error("invalid NULL layout")
        inds = Matrix{Int}(undef, dims[])
        @inbounds for i in 1:length(inds)
            inds[i] = unsafe_load(inds_ptr, i) + 1
        end
        return unsafe_build(T, ptr, nacts, nbufs, inds)
    catch ex
        tao_detach(ptr)
        throw(ex)
    end
end

function unsafe_build(T::Type{<:RemoteMirror},
                      ptr::Ptr{tao_remote_mirror},
                      nacts::Integer,
                      nbufs::Integer,
                      inds::AbstractMatrix{<:Integer})
    buf = Vector{Cdouble}(undef, nacts)
    obj = T(ptr, buf, nacts, nbufs, inds, UNLOCKED)
    return finalizer(finalize, obj)
end

"""
    RemoteMirror(owner, nbufs, inds, flags=TaoRT.DEFAULT_PERMS)

yields a deformable mirror server object whose name is specified by `owner`,
with `nbufs` allocated output buffers and actuators layout `inds`.  Optional
argument `flags` is to specify options and granted permissions.

"""
function RemoteMirror(owner::AbstractString,
                      nbufs::Integer,
                      inds::AbstractMatrix{<:Integer},
                      flags::Integer = DEFAULT_PERMS)
    return build(RemoteMirror, owner, nbufs, inds, flags)
end

function build(T::Type{<:RemoteMirror},
               owner::AbstractString,
               nbufs::Integer,
               inds::AbstractMatrix{<:Integer},
               flags::Integer)
    sizeof(owner) < TAO_OWNER_SIZE || argument_error("owner name too long")
    Base.has_offset_axes(inds) && argument_error(
        "matrix of indices has non-standard indexing")
    dims = size(inds)
    jl_inds = Matrix{Int}(undef, dims)
    c_inds = Matrix{Clong}(undef, dims)
    @inbounds for i in eachindex(jl_inds, c_inds, inds)
        ind = max(Int(inds[i])::Int, 0) # clamp index
        jl_inds[i] = ind
        c_inds[i] = ind - 1
    end
    ptr = check(tao_remote_mirror_create(owner, nbufs, inds, flags))
    try
        nacts = tao_get_nacts(ptr) |> Int
        return unsafe_build(T, ptr, nacts, nbufs, jl_inds)
    catch ex
        tao_detach(ptr)
        throw(ex)
    end
end

function Base.show(io::IO, obj::RemoteMirror)
    lock(io)
    try
        print(io, "RemoteMirror(\"", obj.owner,"\")")
        print(io, "\n ├─ shmid:     "); show(io, obj.shmid)
        print(io, "\n ├─ nbufs:     ", obj.nbufs)
        print(io, "\n ├─ nacts:     ", obj.nacts)
        print(io, "\n ├─ cmin/cmax: ", obj.cmin, "/", obj.cmax)
        print(io, "\n ├─ state:     "); show(io, obj.state)
        print(io, "\n ├─ lock:      "); show(io, obj.lock)
        print(io, "\n ├─ mark:      ", obj.mark)
        print(io, "\n ├─ serial:    ", obj.serial)
        print(io, "\n └─ ncmds:     ", obj.ncmds)
    finally
        unlock(io)
    end
end

"""
    TaoRT.allocate_reference(obj) -> ref

yields an object (usually an array) suitable to store the reference values of
the remote TAO object `obj`.  The contents of the result is undefined, call
`TaoRT.get_reference!(ref, dst)` to fill it.

"""
allocate_reference(obj::RemoteMirror) = Vector{Cdouble}(undef, obj.nacts)

"""
    TaoRT.get_reference(obj) -> ref

yields the reference values of the remote TAO object `obj`. Unlike
`obj.reference`, the result is an independent copy of the values stored in
`obj`.

The caller must have locked the object.

"""
get_reference(obj::RemoteMirror) = get_reference!(allocate_reference(obj), obj)

"""
    TaoRT.get_reference!(dst, obj) -> dst

overwrites the destination `dst` with the reference values of the remote TAO
object `obj`. Calling this function avoids allocating the destination as would
be done by `get_reference(obj)`.

The caller must have locked the object.

"""
function get_reference!(dst::DenseVector{Cdouble}, obj::RemoteMirror)
    assert_locked(obj)
    nacts = obj.nacts
    length(dst) == nacts || throw(DimensionMismatch("invalid destination length"))
    if nacts > 0
        src = tao_get_reference(obj)
        if isnull(src)
            fill!(dst, zero(eltype(dst)))
        else
            GC.@preserve obj unsafe_copy!(dst, src)
        end
    end
    return dst
end

"""
    set_reference(obj::RemoteMirror, vals, secs=Inf) -> (cmdnum => datnum)

sets the actuators reference commands to be `vals` for the subsequent actuators
commands sent to the remote deformable mirror `obj` not waiting longer than
`secs` seconds for being allowed to execute the request.

The request is executed asynchronously, the returned value is a pair `cmdnum =>
datnum` where `cmdnum` is the serial number of the request and `datnum` is the
serial number of the first data-frame affected by the reference values in the
mirror telemetry. The methods `wait_command` and `wait_output` can be
respectively called to wait on these values.

The caller must not have locked the object.

"""
set_reference(obj::RemoteMirror, file::AbstractString, secs::Real = Inf) =
    set_reference(obj, readfits(file), secs)

"""
    set_perturbation(obj::RemoteMirror, vals, secs=Inf) -> (cmdnum => datnum)

sets the actuators perturbation commands to be `vals` for the next actuators
commands sent to the remote deformable mirror `obj` not waiting longer than
`secs` seconds for for being allowed to execute the request. The perturbation
is only applied to the next actuators commands sent to the mirror. Any pre-set
perturbation is replaced by `vals`.

The request is executed asynchronously, the returned value is a pair `cmdnum =>
datnum` where `cmdnum` is the serial number of the request and `datnum` is the
serial number of the first data-frame affected by the perturbation in the
mirror telemetry. The methods `wait_command` and `wait_output` can be
respectively called to wait on these values.

The caller must not have locked the object.

""" set_perturbation

for func in (:set_reference, :set_perturbation)
    @eval begin
        $func(obj::RemoteMirror, vals::AbstractVector, args...) =
            $func(obj, Priv.bufferize!(obj, vals), args...)

        function $func(obj::RemoteMirror,
                       vals::DenseVector{Cdouble},
                       secs::Real = Inf)
            assert_unlocked(obj) # avoid dead-locks
            datnum = Ref{Serial}()
            cmdnum = $(Symbol("tao_",func))(obj, vals, length(vals), secs, datnum)
            return check_command_serial(cmdnum) => datnum[]
        end
    end
end

"""
    TaoRT.Priv.bufferize!(obj, dat) -> buf

stores the values of `dat` in an internal buffer of `obj` and returns it. This
private method is useful to avoid allocating temporaries when conversion is
needed.

"""
function bufferize!(obj::RemoteMirror, arg::AbstractVector)
    buf = getfield(obj, :buf)
    @inbounds @simd for i in eachindex(buf, arg)
        buf[i] = arg[i]
    end
    return buf
end

"""
    send(obj::RemoteMirror, vals, secs=Inf; mark=0) -> (cmdnum => datnum)

sends the actuators' commands `vals` to the remote deformable mirror `obj` not
waiting longer than `secs` seconds for being allowed to execute the request.
Keyword `mark` is to specify a numerical identifier for the resulting
data-frame.

The request is executed asynchronously, the returned value is a pair `cmdnum =>
datnum` where `cmdnum` is the serial number of the request and `datnum` is the
serial number of the first data-frame affected by these commands in the mirror
telemetry. The methods `wait_command` and `wait_output` can be respectively
called to wait on these values.

The actuators' commands `vals` are specified relatively to the current
reference values and, possibly, some perturbations. Because of adjustments such
as clamping of commands, the commands effectively applied to the deformable
mirror may be different from the sent ones. The mirror telemetry in the
corresponding output data-frame accounts for these adjustments (provided the
mirror model is correct).

The caller must not have locked the object.

"""
send(obj::RemoteMirror, vals::AbstractVector{<:Real}, args...; kwds...) =
    send(obj, bufferize!(obj, vals), args...; kwds...)

function send(obj::RemoteMirror,
              vals::DenseVector{Cdouble},
              secs::Real = Inf;
              mark::Integer = 0)
    assert_unlocked(obj) # avoid dead-locks
    datnum = Ref{Serial}()
    cmdnum = tao_send_commands(obj, vals, length(vals), mark, secs, datnum)
    return check_command_serial(cmdnum) => datnum[]
end

"""
    reset(obj::RemoteMirror, secs=Inf; mark=0) -> (cmdnum => datnum)

resets remote deformable mirror `obj` not waiting longer than `secs` seconds for
being allowed to execute the request. Keyword `mark` is to specify a numerical
identifier for the resulting data-frame. Resetting a deformable mirror is
rigorously equivalent to calling [`send`](@ref) with all actuators commands set
to zero.

The request is executed asynchronously, the returned value is a pair `cmdnum =>
datnum` where `cmdnum` is the serial number of the request and `datnum` is the
serial number of the first data-frame affected by this operation in the mirror
telemetry. The methods `wait_command` and `wait_output` can be respectively
called to wait on these values.

The caller must not have locked the object.

"""
function reset(obj::RemoteMirror,
               secs::Real = Inf;
               mark::Integer = 0)
    assert_unlocked(obj) # avoid dead-locks
    datnum = Ref{Serial}()
    cmdnum = tao_reset(obj, mark, secs, datnum)
    return check_command_serial(cmdnum) => datnum[]
end

function configure(obj::RemoteMirror, secs::Real=Inf; refcmds=nothing)
    assert_unlocked(obj) # avoid dead-locks
    if refcmds === nothing
        return obj.ncmds
    elseif refcmds isa Union{AbstractVector,AbstractString}
        return first(set_reference(obj, refcmds))
    else
        argument_error("unexpected value type for keyword `refcmds`")
    end
end

"""
    findlast(obj::RemoteMirror, mrk::Integer) -> ser

yields the serial number of the last data-frame produced by remote object `obj` whose mark
is equal to `mrk`. If no match is found, 0 is returned; otherwise, a positive number is
returned. Unless the caller has locked the object, the data-frame with the returned serial
number may no longer be available after the call to this function.

"""
Base.findlast(obj::RemoteMirror, mrk::Integer) = tao_find_mark(obj, mrk)

"""
    fetch!(dat, obj, n = obj.serial) -> dat

overwrites destination `dat` with the `n`-th data-frame stored by TAO remote object `obj`.
The number `n` is the serial number of the data-frame. By default, the last available
data-frame is fetched.

"""
function fetch!(dat::MirrorDataFrame, obj::RemoteMirror, n::Integer = obj.serial)
    refcmds = dat.refcmds
    perturb = dat.perturb
    reqcmds = dat.reqcmds
    devcmds = dat.devcmds
    nacts = obj.nacts
    length(refcmds) == nacts || assertion_error("invalid length for `refcmds` field")
    length(perturb) == nacts || assertion_error("invalid length for `perturb` field")
    length(reqcmds) == nacts || assertion_error("invalid length for `reqcmds` field")
    length(devcmds) == nacts || assertion_error("invalid length for `devcmds` field")
    check(tao_fetch_data(obj, n, refcmds, perturb, reqcmds, devcmds, nacts, dat))
    return dat
end

# Provides a pointer to the data-frame header that is embedded in a MirrorDataFrame.
Base.unsafe_convert(::Type{Ptr{DataFrameInfo}}, dat::MirrorDataFrame) =
    Ptr{DataFrameInfo}(pointer_from_objref(dat))

fetch(obj::RemoteMirror, n::Integer = obj.serial) =
    fetch!(DataFrame(obj), obj, n)

"""
    TaoRT.MirrorDataFrame(nacts) -> dat

yields an object `dat` suitable to store a data frame from a remote deformable
mirror with `nacts` actuators.

    TaoRT.MirrorDataFrame(obj) -> dat

yields an object `dat` suitable to store a data frame from remote deformable
mirror `obj`.

Returned object `dat` has the following members:

    dat.serial   # serial number
    dat.mark     # user-defined mark
    dat.time     # time-stamp
    dat.refcmds  # reference commands
    dat.perturb  # perturbation commands
    dat.reqcmds  # requested commands
    dat.devcmds  # commands sent to the device

The actuators' requested commands, `dat.reqcmds`, are always relative to the reference
offsets `dat.refcmds` plus some given perturbations `dat.perturb`. The commands applied to
the deformable mirror device are given by:

     devcmds = ψ(dat.reqcmds + dat.refcmds + dat.perturb)

where the mapping `ψ(...)` models the modifications (clamping, etc.) due to the deformable
mirror limitations.

"""
MirrorDataFrame(obj::RemoteMirror) = MirrorDataFrame(obj.nacts)

MirrorDataFrame(nacts::Integer) =
    MirrorDataFrame(DataFrameInfo(0, 0, HighResolutionTime(0, 0)),
                    Vector{Cdouble}(undef, nacts),
                    Vector{Cdouble}(undef, nacts),
                    Vector{Cdouble}(undef, nacts),
                    Vector{Cdouble}(undef, nacts))

DataFrame(obj::RemoteMirror) = MirrorDataFrame(obj)

propertynames(::MirrorDataFrame) = (
    :devcmds,
    :mark,
    :perturb,
    :refcmds,
    :reqcmds,
    :serial,
    :time)

# NOTE: `dat.serial`, `dat.mark`, and `dat.time` done in `remote_objects.jl`
#       for generic remote objects.
getproperty(dat::MirrorDataFrame, ::Val{:devcmds}) = getfield(dat, :devcmds)
getproperty(dat::MirrorDataFrame, ::Val{:perturb}) = getfield(dat, :perturb)
getproperty(dat::MirrorDataFrame, ::Val{:refcmds}) = getfield(dat, :refcmds)
getproperty(dat::MirrorDataFrame, ::Val{:reqcmds}) = getfield(dat, :reqcmds)
