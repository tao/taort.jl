#
# rwlocked_objects.jl -
#
# Low level interface to basic r/w locked shared objects for the Julia
# interface to the real-time C library of TAO, a Toolkit for Adaptive Optics.
#
#-------------------------------------------------------------------------------
#
# This file is part of TAO software (https://git-cral.univ-lyon1.fr/tao)
# licensed under the MIT license.
#
# Copyright (C) 2018-2023, Éric Thiébaut.
#

import ..TaoRT: RWLockedObject

propertynames(obj::RWLockedObject) = (
    :flags,
    :lock,
    :perms,
    :shmid,
    :size,
    :type)

"""
    TaoRT.RWLockedObject(arg) -> obj

attaches to the TAO read/write locked object identified by `arg` which can be
the shared memory identifier of the object or another read/write locked object.

The returned object implements the `obj.key` syntax for the following
properties:

| Name    | Description                                         |
|:--------|:----------------------------------------------------|
| `flags` | (c) Bitwise flags of the shared object              |
| `lock`  | (p) Type of lock owned by the caller                |
| `perms` | (c) Permissions access bits of the shared object    |
| `shmid` | (c) Shared memory identifier of the object          |
| `size`  | (c) Number of bytes allocated for the shared object |
| `type`  | (c) Type identifier of the shared object            |

Notes:
- (p) private member, no needs to lock the object;
- (c) constant member, no needs to lock the object.

!!! warning
    Properties should all be considered as read-only by the end-user and never
    directly modified or unexpected behavior may occur.

"""
RWLockedObject(id::Integer) = RWLockedObject(ShmId(id))
RWLockedObject(shmid::ShmId) = build(RWLockedObject, shmid)

function build(T::Type{<:RWLockedObject}, shmid::ShmId)
    ptr = tao_rwlocked_object_attach(shmid)
    return build(T, ptr)
end

"""
    TaoRT.RWLockedObject(
        type, size, flags = TaoRT.DEFAULT_PERMS) -> obj

yields a new TAO read-write locked object.

!!! warning
    Creating a r/w locked object is mainly for testing as there are no means
    yet to manage the object as a true server in Julia.

"""
RWLockedObject(type::Integer, size::Integer, flags::Integer = DEFAULT_PERMS) =
    build(RWLockedObject, type, size, flags)

function build(T::Type{<:RWLockedObject}, type::Integer, size::Integer,
               flags::Integer)
    ptr = tao_rwlocked_object_create(type, size, flags)
    return build(T, ptr)
end

function Base.show(io::IO, obj::RWLockedObject)
    lock(io)
    try
        print(io, typeof(obj))
        print(io, "\n ├─ shmid: "); show(io, obj.shmid)
        print(io, "\n ├─ type:  "); show(io, obj.type)
        print(io, "\n └─ lock:  "); show(io, obj.lock)
    finally
        unlock(io)
    end
end
