module Lib

import Tao

let file = normpath(joinpath(@__DIR__, "..", "deps", "deps.jl"))
    isfile(file) || error(
        "File \"$file\" does not exists. ",
        "Run `using Pkg; Pkg.build(\"TaoRT\");` to build it.")
end
include(joinpath("..", "deps", "deps.jl"))

const libtao = libtao_library

TAO_ENCODING(col,pxl) = TAO_ENCODING(col,pxl,pxl)

TAO_ENCODING(col,pxl,pkt) = ((tao_encoding(col) << 16) |
                             (tao_encoding(pkt) <<  8) | tao_encoding(pxl))

TAO_ENCODING(col,pxl,pkt,flg) = ((tao_encoding(flg) << 24) |
                                 TAO_ENCODING(col,pxl,pkt))

TAO_ENCODING_BITS_PER_PIXEL(enc) = (tao_encoding(enc) & TAO_ENCODING_MASK)

TAO_ENCODING_BITS_PER_PACKET(enc) = ((tao_encoding(enc) >> 8) &
                                     TAO_ENCODING_MASK)

TAO_ENCODING_COLORANT(enc) = ((tao_encoding(enc) >> 16) & TAO_ENCODING_MASK)

TAO_ENCODING_FLAGS(enc) = ((tao_encoding(enc) >> 24) & TAO_ENCODING_MASK)

TAO_ENCODING_RAW(pxl) = TAO_ENCODING(TAO_COLORANT_RAW, pxl)

TAO_ENCODING_RAW_PKT(pxl, pkt) = TAO_ENCODING(TAO_COLORANT_RAW, pxl, pkt)

TAO_ENCODING_MONO(pxl) = TAO_ENCODING(TAO_COLORANT_MONO, pxl)

TAO_ENCODING_MONO_PKT(pxl, pkt) = TAO_ENCODING(TAO_COLORANT_MONO, pxl, pkt)

TAO_ENCODING_RGB(pxl) = TAO_ENCODING(TAO_COLORANT_RGB, pxl)

TAO_ENCODING_RGB_PKT(pxl, pkt) = TAO_ENCODING(TAO_COLORANT_RGB, pxl, pkt)

TAO_ENCODING_BGR(pxl) = TAO_ENCODING(TAO_COLORANT_BGR, pxl)

TAO_ENCODING_BGR_PKT(pxl, pkt) = TAO_ENCODING(TAO_COLORANT_BGR, pxl, pkt)

TAO_ENCODING_ARGB(pxl) = TAO_ENCODING(TAO_COLORANT_ARGB, pxl)

TAO_ENCODING_ARGB_PKT(pxl, pkt) = TAO_ENCODING(TAO_COLORANT_ARGB, pxl, pkt)

TAO_ENCODING_RGBA(pxl) = TAO_ENCODING(TAO_COLORANT_RGBA, pxl)

TAO_ENCODING_RGBA_PKT(pxl, pkt) = TAO_ENCODING(TAO_COLORANT_RGBA, pxl, pkt)

TAO_ENCODING_ABGR(pxl) = TAO_ENCODING(TAO_COLORANT_ABGR, pxl)

TAO_ENCODING_ABGR_PKT(pxl, pkt) = TAO_ENCODING(TAO_COLORANT_ABGR, pxl, pkt)

TAO_ENCODING_BGRA(pxl) = TAO_ENCODING(TAO_COLORANT_BGRA, pxl)

TAO_ENCODING_BGRA_PKT(pxl, pkt) = TAO_ENCODING(TAO_COLORANT_BGRA, pxl, pkt)

TAO_ENCODING_BAYER_RGGB(pxl) = TAO_ENCODING(TAO_COLORANT_BAYER_RGGB, pxl)

TAO_ENCODING_BAYER_GRBG(pxl) = TAO_ENCODING(TAO_COLORANT_BAYER_GRBG, pxl)

TAO_ENCODING_BAYER_GBRG(pxl) = TAO_ENCODING(TAO_COLORANT_BAYER_GBRG, pxl)

TAO_ENCODING_BAYER_BGGR(pxl) = TAO_ENCODING(TAO_COLORANT_BAYER_BGGR, pxl)

const TAO_NATIVE_ENDIAN_BOM = ENDIAN_BOM

const __pid_t = Cint

const pid_t = __pid_t

struct tao_image_roi
    xoff::Clong
    yoff::Clong
    width::Clong
    height::Clong
end

function tao_count_elements(ndims, dims)
    @ccall libtao.tao_count_elements(ndims::Cint, dims::Ptr{Clong})::Clong
end

abstract type tao_array end

@enum tao_eltype::UInt32 begin
    TAO_INT8 = 1
    TAO_UINT8 = 2
    TAO_INT16 = 3
    TAO_UINT16 = 4
    TAO_INT32 = 5
    TAO_UINT32 = 6
    TAO_INT64 = 7
    TAO_UINT64 = 8
    TAO_FLOAT = 9
    TAO_DOUBLE = 10
end

function tao_create_array(eltype, ndims, dims)
    @ccall libtao.tao_create_array(eltype::tao_eltype, ndims::Cint, dims::Ptr{Clong})::Ptr{tao_array}
end

function tao_create_1d_array(eltype, dim)
    @ccall libtao.tao_create_1d_array(eltype::tao_eltype, dim::Clong)::Ptr{tao_array}
end

function tao_create_2d_array(eltype, dim1, dim2)
    @ccall libtao.tao_create_2d_array(eltype::tao_eltype, dim1::Clong, dim2::Clong)::Ptr{tao_array}
end

function tao_create_3d_array(eltype, dim1, dim2, dim3)
    @ccall libtao.tao_create_3d_array(eltype::tao_eltype, dim1::Clong, dim2::Clong, dim3::Clong)::Ptr{tao_array}
end

function tao_wrap_array(eltype, ndims, dims, data, free, ctx)
    @ccall libtao.tao_wrap_array(eltype::tao_eltype, ndims::Cint, dims::Ptr{Clong}, data::Ptr{Cvoid}, free::Ptr{Cvoid}, ctx::Ptr{Cvoid})::Ptr{tao_array}
end

function tao_wrap_1d_array(eltype, dim, data, free, ctx)
    @ccall libtao.tao_wrap_1d_array(eltype::tao_eltype, dim::Clong, data::Ptr{Cvoid}, free::Ptr{Cvoid}, ctx::Ptr{Cvoid})::Ptr{tao_array}
end

function tao_wrap_2d_array(eltype, dim1, dim2, data, free, ctx)
    @ccall libtao.tao_wrap_2d_array(eltype::tao_eltype, dim1::Clong, dim2::Clong, data::Ptr{Cvoid}, free::Ptr{Cvoid}, ctx::Ptr{Cvoid})::Ptr{tao_array}
end

function tao_wrap_3d_array(eltype, dim1, dim2, dim3, data, free, ctx)
    @ccall libtao.tao_wrap_3d_array(eltype::tao_eltype, dim1::Clong, dim2::Clong, dim3::Clong, data::Ptr{Cvoid}, free::Ptr{Cvoid}, ctx::Ptr{Cvoid})::Ptr{tao_array}
end

function tao_reference_array(arr)
    @ccall libtao.tao_reference_array(arr::Ptr{tao_array})::Ptr{tao_array}
end

function tao_unreference_array(arr)
    @ccall libtao.tao_unreference_array(arr::Ptr{tao_array})::Cvoid
end

function tao_get_array_eltype(arr)
    @ccall libtao.tao_get_array_eltype(arr::Ptr{tao_array})::tao_eltype
end

function tao_get_array_length(arr)
    @ccall libtao.tao_get_array_length(arr::Ptr{tao_array})::Clong
end

function tao_get_array_ndims(arr)
    @ccall libtao.tao_get_array_ndims(arr::Ptr{tao_array})::Cint
end

function tao_get_array_dim(arr, d)
    @ccall libtao.tao_get_array_dim(arr::Ptr{tao_array}, d::Cint)::Clong
end

function tao_get_array_data(arr)
    @ccall libtao.tao_get_array_data(arr::Ptr{tao_array})::Ptr{Cvoid}
end

@enum tao_status::Int32 begin
    TAO_ERROR = -1
    TAO_OK = 0
    TAO_TIMEOUT = 1
end

function tao_copy_to_array(dst, dstoffs, srcdata, srctype, srcdims, srcoffs, lens, ndims)
    @ccall libtao.tao_copy_to_array(dst::Ptr{tao_array}, dstoffs::Ptr{Clong}, srcdata::Ptr{Cvoid}, srctype::tao_eltype, srcdims::Ptr{Clong}, srcoffs::Ptr{Clong}, lens::Ptr{Clong}, ndims::Cint)::tao_status
end

function tao_copy_from_array(dstdata, dsttype, dstdims, dstoffs, src, srcoffs, lens, ndims)
    @ccall libtao.tao_copy_from_array(dstdata::Ptr{Cvoid}, dsttype::tao_eltype, dstdims::Ptr{Clong}, dstoffs::Ptr{Clong}, src::Ptr{tao_array}, srcoffs::Ptr{Clong}, lens::Ptr{Clong}, ndims::Cint)::tao_status
end

function tao_copy_array_to_array(dst, dstoffs, src, srcoffs, lens, ndims)
    @ccall libtao.tao_copy_array_to_array(dst::Ptr{tao_array}, dstoffs::Ptr{Clong}, src::Ptr{tao_array}, srcoffs::Ptr{Clong}, lens::Ptr{Clong}, ndims::Cint)::tao_status
end

struct __JL_Ctag_321
    data::NTuple{8, UInt8}
end

function Base.getproperty(x::Ptr{__JL_Ctag_321}, f::Symbol)
    f === :b && return Ptr{Bool}(x + 0)
    f === :i && return Ptr{Int64}(x + 0)
    f === :f && return Ptr{Cdouble}(x + 0)
    f === :s && return Ptr{NTuple{0, Cchar}}(x + 0)
    return getfield(x, f)
end

function Base.getproperty(x::__JL_Ctag_321, f::Symbol)
    r = Ref{__JL_Ctag_321}(x)
    ptr = Base.unsafe_convert(Ptr{__JL_Ctag_321}, r)
    fptr = getproperty(ptr, f)
    GC.@preserve r unsafe_load(fptr)
end

function Base.setproperty!(x::Ptr{__JL_Ctag_321}, f::Symbol, v)
    unsafe_store!(getproperty(x, f), v)
end

struct tao_attr
    data::NTuple{16, UInt8}
end

function Base.getproperty(x::Ptr{tao_attr}, f::Symbol)
    f === :type && return Ptr{UInt8}(x + 0)
    f === :flags && return Ptr{UInt8}(x + 1)
    f === :key_offset && return Ptr{UInt16}(x + 2)
    f === :key_size && return Ptr{UInt16}(x + 4)
    f === :val && return Ptr{__JL_Ctag_321}(x + 8)
    return getfield(x, f)
end

function Base.getproperty(x::tao_attr, f::Symbol)
    r = Ref{tao_attr}(x)
    ptr = Base.unsafe_convert(Ptr{tao_attr}, r)
    fptr = getproperty(ptr, f)
    GC.@preserve r unsafe_load(fptr)
end

function Base.setproperty!(x::Ptr{tao_attr}, f::Symbol, v)
    unsafe_store!(getproperty(x, f), v)
end

@enum tao_attr_type::UInt32 begin
    TAO_ATTR_NONE = 0
    TAO_ATTR_BOOLEAN = 1
    TAO_ATTR_INTEGER = 2
    TAO_ATTR_FLOAT = 3
    TAO_ATTR_STRING = 4
end

function tao_attr_get_type(attr)
    @ccall libtao.tao_attr_get_type(attr::Ptr{tao_attr})::tao_attr_type
end

function tao_attr_get_flags(attr)
    @ccall libtao.tao_attr_get_flags(attr::Ptr{tao_attr})::Cuint
end

function tao_attr_get_key(attr)
    @ccall libtao.tao_attr_get_key(attr::Ptr{tao_attr})::Cstring
end

function tao_attr_get_max_key_size(attr)
    @ccall libtao.tao_attr_get_max_key_size(attr::Ptr{tao_attr})::Csize_t
end

function tao_attr_get_max_val_size(attr)
    @ccall libtao.tao_attr_get_max_val_size(attr::Ptr{tao_attr})::Csize_t
end

function tao_attr_is_readable(attr)
    @ccall libtao.tao_attr_is_readable(attr::Ptr{tao_attr})::Cint
end

function tao_attr_is_writable(attr)
    @ccall libtao.tao_attr_is_writable(attr::Ptr{tao_attr})::Cint
end

function tao_attr_is_variable(attr)
    @ccall libtao.tao_attr_is_variable(attr::Ptr{tao_attr})::Cint
end

function tao_attr_get_boolean_value(attr, ptr)
    @ccall libtao.tao_attr_get_boolean_value(attr::Ptr{tao_attr}, ptr::Ptr{Cint})::tao_status
end

function tao_attr_get_integer_value(attr, ptr)
    @ccall libtao.tao_attr_get_integer_value(attr::Ptr{tao_attr}, ptr::Ptr{Int64})::tao_status
end

function tao_attr_get_float_value(attr, ptr)
    @ccall libtao.tao_attr_get_float_value(attr::Ptr{tao_attr}, ptr::Ptr{Cdouble})::tao_status
end

function tao_attr_get_string_value(attr, ptr)
    @ccall libtao.tao_attr_get_string_value(attr::Ptr{tao_attr}, ptr::Ptr{Cstring})::tao_status
end

function tao_attr_set_boolean_value(attr, val, overwrite)
    @ccall libtao.tao_attr_set_boolean_value(attr::Ptr{tao_attr}, val::Cint, overwrite::Cint)::tao_status
end

function tao_attr_set_integer_value(attr, val, overwrite)
    @ccall libtao.tao_attr_set_integer_value(attr::Ptr{tao_attr}, val::Int64, overwrite::Cint)::tao_status
end

function tao_attr_set_float_value(attr, val, overwrite)
    @ccall libtao.tao_attr_set_float_value(attr::Ptr{tao_attr}, val::Cdouble, overwrite::Cint)::tao_status
end

function tao_attr_set_string_value(attr, val, overwrite)
    @ccall libtao.tao_attr_set_string_value(attr::Ptr{tao_attr}, val::Cstring, overwrite::Cint)::tao_status
end

function tao_attr_check(attr1, attr2, flags)
    @ccall libtao.tao_attr_check(attr1::Ptr{tao_attr}, attr2::Ptr{tao_attr}, flags::Cuint)::Cuint
end

struct tao_attr_table
    size::Csize_t
    length::Clong
end

function tao_attr_table_create_with_size(size)
    @ccall libtao.tao_attr_table_create_with_size(size::Csize_t)::Ptr{tao_attr_table}
end

function tao_attr_table_set_size(table, size)
    @ccall libtao.tao_attr_table_set_size(table::Ptr{tao_attr_table}, size::Csize_t)::tao_status
end

struct tao_attr_descr
    key::Cstring
    type::UInt8
    flags::UInt8
    size::Csize_t
end

function tao_attr_table_create(defs, num)
    @ccall libtao.tao_attr_table_create(defs::Ptr{tao_attr_descr}, num::Clong)::Ptr{tao_attr_table}
end

function tao_attr_table_destroy(table)
    @ccall libtao.tao_attr_table_destroy(table::Ptr{tao_attr_table})::Cvoid
end

function tao_attr_table_get_size(table)
    @ccall libtao.tao_attr_table_get_size(table::Ptr{tao_attr_table})::Csize_t
end

function tao_attr_table_get_length(table)
    @ccall libtao.tao_attr_table_get_length(table::Ptr{tao_attr_table})::Clong
end

function tao_attr_table_instantiate(table, defs, num)
    @ccall libtao.tao_attr_table_instantiate(table::Ptr{tao_attr_table}, defs::Ptr{tao_attr_descr}, num::Clong)::tao_status
end

function tao_attr_table_check(table)
    @ccall libtao.tao_attr_table_check(table::Ptr{tao_attr_table})::tao_status
end

function tao_attr_table_get_entry(table, index)
    @ccall libtao.tao_attr_table_get_entry(table::Ptr{tao_attr_table}, index::Clong)::Ptr{tao_attr}
end

function tao_attr_table_unsafe_get_entry(table, index)
    @ccall libtao.tao_attr_table_unsafe_get_entry(table::Ptr{tao_attr_table}, index::Clong)::Ptr{tao_attr}
end

function tao_attr_table_try_find_entry(table, key)
    @ccall libtao.tao_attr_table_try_find_entry(table::Ptr{tao_attr_table}, key::Cstring)::Clong
end

function tao_attr_table_find_entry(table, key)
    @ccall libtao.tao_attr_table_find_entry(table::Ptr{tao_attr_table}, key::Cstring)::Ptr{tao_attr}
end

function tao_attr_table_get_boolean_value(table, key, ptr)
    @ccall libtao.tao_attr_table_get_boolean_value(table::Ptr{tao_attr_table}, key::Cstring, ptr::Ptr{Cint})::tao_status
end

function tao_attr_table_get_integer_value(table, key, ptr)
    @ccall libtao.tao_attr_table_get_integer_value(table::Ptr{tao_attr_table}, key::Cstring, ptr::Ptr{Int64})::tao_status
end

function tao_attr_table_get_float_value(table, key, ptr)
    @ccall libtao.tao_attr_table_get_float_value(table::Ptr{tao_attr_table}, key::Cstring, ptr::Ptr{Cdouble})::tao_status
end

function tao_attr_table_get_string_value(table, key, ptr)
    @ccall libtao.tao_attr_table_get_string_value(table::Ptr{tao_attr_table}, key::Cstring, ptr::Ptr{Cstring})::tao_status
end

function tao_attr_table_set_boolean_value(table, key, val, overwrite)
    @ccall libtao.tao_attr_table_set_boolean_value(table::Ptr{tao_attr_table}, key::Cstring, val::Cint, overwrite::Cint)::tao_status
end

function tao_attr_table_set_integer_value(table, key, val, overwrite)
    @ccall libtao.tao_attr_table_set_integer_value(table::Ptr{tao_attr_table}, key::Cstring, val::Int64, overwrite::Cint)::tao_status
end

function tao_attr_table_set_float_value(table, key, val, overwrite)
    @ccall libtao.tao_attr_table_set_float_value(table::Ptr{tao_attr_table}, key::Cstring, val::Cdouble, overwrite::Cint)::tao_status
end

function tao_attr_table_set_string_value(table, key, val, overwrite)
    @ccall libtao.tao_attr_table_set_string_value(table::Ptr{tao_attr_table}, key::Cstring, val::Cstring, overwrite::Cint)::tao_status
end

function tao_version_get(major, minor, patch)
    @ccall libtao.tao_version_get(major::Ptr{Cint}, minor::Ptr{Cint}, patch::Ptr{Cint})::Cvoid
end

const tao_serial = Int64

@enum tao_process_sharing::UInt32 begin
    TAO_PROCESS_PRIVATE = 0
    TAO_PROCESS_SHARED = 1
end

struct tao_buffer
    data::Cstring
    size::Csize_t
    offset::Csize_t
    pending::Csize_t
    flags::Cuint
end

function tao_buffer_initialize(buf)
    @ccall libtao.tao_buffer_initialize(buf::Ptr{tao_buffer})::Cvoid
end

function tao_buffer_create(size)
    @ccall libtao.tao_buffer_create(size::Csize_t)::Ptr{tao_buffer}
end

function tao_buffer_destroy(buf)
    @ccall libtao.tao_buffer_destroy(buf::Ptr{tao_buffer})::Cvoid
end

function tao_buffer_resize(buf, cnt)
    @ccall libtao.tao_buffer_resize(buf::Ptr{tao_buffer}, cnt::Csize_t)::tao_status
end

function tao_buffer_flush(buf)
    @ccall libtao.tao_buffer_flush(buf::Ptr{tao_buffer})::Cvoid
end

function tao_buffer_clear(buf)
    @ccall libtao.tao_buffer_clear(buf::Ptr{tao_buffer})::Cvoid
end

function tao_buffer_get_contents_size(buf)
    @ccall libtao.tao_buffer_get_contents_size(buf::Ptr{tao_buffer})::Csize_t
end

function tao_buffer_get_contents(buf, sizptr)
    @ccall libtao.tao_buffer_get_contents(buf::Ptr{tao_buffer}, sizptr::Ptr{Csize_t})::Ptr{Cvoid}
end

function tao_buffer_get_unused_size(buf)
    @ccall libtao.tao_buffer_get_unused_size(buf::Ptr{tao_buffer})::Csize_t
end

function tao_buffer_get_total_unused_size(buf)
    @ccall libtao.tao_buffer_get_total_unused_size(buf::Ptr{tao_buffer})::Csize_t
end

function tao_buffer_get_unused_part(buf, data)
    @ccall libtao.tao_buffer_get_unused_part(buf::Ptr{tao_buffer}, data::Ptr{Ptr{Cvoid}})::Csize_t
end

function tao_buffer_adjust_contents_size(buf, adj)
    @ccall libtao.tao_buffer_adjust_contents_size(buf::Ptr{tao_buffer}, adj::Cssize_t)::tao_status
end

function tao_buffer_read_from_fd(buf, fd, cnt)
    @ccall libtao.tao_buffer_read_from_fd(buf::Ptr{tao_buffer}, fd::Cint, cnt::Csize_t)::Cssize_t
end

function tao_buffer_write_to_fd(buf, fd)
    @ccall libtao.tao_buffer_write_to_fd(buf::Ptr{tao_buffer}, fd::Cint)::Cssize_t
end

function tao_buffer_append_bytes(buf, ptr, siz)
    @ccall libtao.tao_buffer_append_bytes(buf::Ptr{tao_buffer}, ptr::Ptr{Cvoid}, siz::Clong)::tao_status
end

function tao_buffer_append_string(buf, str, len)
    @ccall libtao.tao_buffer_append_string(buf::Ptr{tao_buffer}, str::Cstring, len::Clong)::tao_status
end

function tao_buffer_append_char(buf, c)
    @ccall libtao.tao_buffer_append_char(buf::Ptr{tao_buffer}, c::Cint)::tao_status
end

function tao_file_open(path, mode)
    @ccall libtao.tao_file_open(path::Cstring, mode::Cstring)::Ptr{Libc.FILE}
end

function tao_file_close(file)
    @ccall libtao.tao_file_close(file::Ptr{Libc.FILE})::tao_status
end

function tao_config_open(name, mode)
    @ccall libtao.tao_config_open(name::Cstring, mode::Cstring)::Ptr{Libc.FILE}
end

function tao_config_path(path, size, name)
    @ccall libtao.tao_config_path(path::Cstring, size::Clong, name::Cstring)::tao_status
end

function tao_config_read_long(name, ptr)
    @ccall libtao.tao_config_read_long(name::Cstring, ptr::Ptr{Clong})::tao_status
end

function tao_config_write_long(name, value)
    @ccall libtao.tao_config_write_long(name::Cstring, value::Clong)::tao_status
end

const tao_shmid = Int32

function tao_config_read_shmid(name)
    @ccall libtao.tao_config_read_shmid(name::Cstring)::tao_shmid
end

const tao_encoding = UInt32

struct tao_byte_order_mark
    data::NTuple{4, UInt8}
end

function Base.getproperty(x::Ptr{tao_byte_order_mark}, f::Symbol)
    f === :bytes && return Ptr{NTuple{4, UInt8}}(x + 0)
    f === :value && return Ptr{UInt32}(x + 0)
    return getfield(x, f)
end

function Base.getproperty(x::tao_byte_order_mark, f::Symbol)
    r = Ref{tao_byte_order_mark}(x)
    ptr = Base.unsafe_convert(Ptr{tao_byte_order_mark}, r)
    fptr = getproperty(ptr, f)
    GC.@preserve r unsafe_load(fptr)
end

function Base.setproperty!(x::Ptr{tao_byte_order_mark}, f::Symbol, v)
    unsafe_store!(getproperty(x, f), v)
end

function tao_size_of_eltype(eltype)
    @ccall libtao.tao_size_of_eltype(eltype::tao_eltype)::Csize_t
end

function tao_name_of_eltype(eltype)
    @ccall libtao.tao_name_of_eltype(eltype::tao_eltype)::Cstring
end

function tao_description_of_eltype(eltype)
    @ccall libtao.tao_description_of_eltype(eltype::tao_eltype)::Cstring
end

function tao_encoding_of_eltype(eltype)
    @ccall libtao.tao_encoding_of_eltype(eltype::tao_eltype)::tao_encoding
end

function tao_eltype_of_encoding(enc)
    @ccall libtao.tao_eltype_of_encoding(enc::tao_encoding)::tao_eltype
end

function tao_format_encoding(str, enc)
    @ccall libtao.tao_format_encoding(str::Cstring, enc::tao_encoding)::tao_status
end

function tao_parse_encoding(str)
    @ccall libtao.tao_parse_encoding(str::Cstring)::tao_encoding
end

function tao_copy(dstdata, dsttype, dstdims, dstoffs, srcdata, srctype, srcdims, srcoffs, lens, ndims)
    @ccall libtao.tao_copy(dstdata::Ptr{Cvoid}, dsttype::tao_eltype, dstdims::Ptr{Clong}, dstoffs::Ptr{Clong}, srcdata::Ptr{Cvoid}, srctype::tao_eltype, srcdims::Ptr{Clong}, srcoffs::Ptr{Clong}, lens::Ptr{Clong}, ndims::Cint)::tao_status
end

function tao_copy_checked_args(dstdata, dsttype, dstdims, dstoffs, srcdata, srctype, srcdims, srcoffs, lens, ndims)
    @ccall libtao.tao_copy_checked_args(dstdata::Ptr{Cvoid}, dsttype::tao_eltype, dstdims::Ptr{Clong}, dstoffs::Ptr{Clong}, srcdata::Ptr{Cvoid}, srctype::tao_eltype, srcdims::Ptr{Clong}, srcoffs::Ptr{Clong}, lens::Ptr{Clong}, ndims::Cint)::Cvoid
end

# typedef void tao_error_getter ( int code , const char * * reason , const char * * info )
const tao_error_getter = Cvoid

@enum tao_error_code::Int32 begin
    TAO_SUCCESS = 0
    TAO_ACQUISITION_RUNNING = -1
    TAO_ALREADY_EXIST = -2
    TAO_ALREADY_IN_USE = -3
    TAO_ASSERTION_FAILED = -4
    TAO_BAD_ADDRESS = -5
    TAO_BAD_ALGORITHM = -6
    TAO_BAD_ARGUMENT = -7
    TAO_BAD_ATTACHMENTS = -8
    TAO_BAD_BIAS = -9
    TAO_BAD_BOUNDING_BOX = -10
    TAO_BAD_BUFFERS = -11
    TAO_BAD_CAMERA = -12
    TAO_BAD_CHANNELS = -13
    TAO_BAD_CHARACTER = -14
    TAO_BAD_COMMAND = -15
    TAO_BAD_CONNECTION = -16
    TAO_BAD_DEPTH = -17
    TAO_BAD_DEVICE = -18
    TAO_BAD_ENCODING = -19
    TAO_BAD_ESCAPE = -20
    TAO_BAD_EXPOSURETIME = -21
    TAO_BAD_FANSPEED = -22
    TAO_BAD_FILENAME = -23
    TAO_BAD_FORGETTING_FACTOR = -24
    TAO_BAD_FRAMERATE = -25
    TAO_BAD_GAIN = -26
    TAO_BAD_KEY_SIZE = -27
    TAO_BAD_MAGIC = -28
    TAO_BAD_MAX_EXCURSION = -29
    TAO_BAD_NAME = -30
    TAO_BAD_NUMBER = -31
    TAO_BAD_OWNER = -32
    TAO_BAD_PIXELTYPE = -33
    TAO_BAD_PREPROCESSING = -34
    TAO_BAD_RANGE = -35
    TAO_BAD_RANK = -36
    TAO_BAD_REFERENCE = -37
    TAO_BAD_RESTORING_FORCE = -38
    TAO_BAD_ROI = -39
    TAO_BAD_SERIAL = -40
    TAO_BAD_SIZE = -41
    TAO_BAD_SMOOTHNESS_LEVEL = -42
    TAO_BAD_SPEED = -43
    TAO_BAD_STAGE = -44
    TAO_BAD_STATE = -45
    TAO_BAD_STATUS = -46
    TAO_BAD_STRING = -47
    TAO_BAD_TABLE_SIZE = -48
    TAO_BAD_TEMPERATURE = -49
    TAO_BAD_TYPE = -50
    TAO_BAD_VALUE = -51
    TAO_BAD_VALUE_SIZE = -52
    TAO_BROKEN_CYCLE = -53
    TAO_CANT_TRACK_ERROR = -54
    TAO_CORRUPTED = -55
    TAO_DESTROYED = -56
    TAO_EXHAUSTED = -57
    TAO_EXHAUSTED_TIME = -58
    TAO_FORBIDDEN_CHANGE = -59
    TAO_INEXACT_CONVERSION = -60
    TAO_MISSING_SEPARATOR = -61
    TAO_MUST_RESET = -62
    TAO_NOT_ACQUIRING = -63
    TAO_NOT_FOUND = -64
    TAO_NOT_LOCKED = -65
    TAO_NOT_READY = -66
    TAO_NOT_RUNNING = -67
    TAO_NOT_YET_IMPLEMENTED = -68
    TAO_NO_DATA = -69
    TAO_NO_FITS_SUPPORT = -70
    TAO_OUT_OF_RANGE = -71
    TAO_OVERWRITTEN = -72
    TAO_SYSTEM_ERROR = -73
    TAO_UNCLOSED_STRING = -74
    TAO_UNREADABLE = -75
    TAO_UNRECOVERABLE = -76
    TAO_UNSUPPORTED = -77
    TAO_UNWRITABLE = -78
end

struct tao_error
    func::Cstring
    code::Cint
    proc::Ptr{tao_error_getter}
end

function tao_store_other_error(func, code, proc)
    @ccall libtao.tao_store_other_error(func::Cstring, code::Cint, proc::Ptr{tao_error_getter})::Cvoid
end

function tao_store_error(func, code)
    @ccall libtao.tao_store_error(func::Cstring, code::Cint)::Cvoid
end

function tao_store_system_error(func)
    @ccall libtao.tao_store_system_error(func::Cstring)::Cvoid
end

# typedef tao_status tao_error_handler ( const tao_error * err )
const tao_error_handler = Cvoid

function tao_set_error_handler(func)
    @ccall libtao.tao_set_error_handler(func::Ptr{tao_error_handler})::Ptr{tao_error_handler}
end

function tao_report_error()
    @ccall libtao.tao_report_error()::Cvoid
end

function tao_panic()
    @ccall libtao.tao_panic()::Cvoid
end

function tao_report_error_to_stderr(err, pfx, sfx)
    @ccall libtao.tao_report_error_to_stderr(err::Ptr{tao_error}, pfx::Cstring, sfx::Cstring)::tao_status
end

function tao_report_error_to_buffer(buf, err, pfx, sfx)
    @ccall libtao.tao_report_error_to_buffer(buf::Ptr{tao_buffer}, err::Ptr{tao_error}, pfx::Cstring, sfx::Cstring)::tao_status
end

# typedef tao_status tao_error_reporter ( void * ctx , const char * reason , const char * func , const char * info , int code )
const tao_error_reporter = Cvoid

function tao_report_error_with_reporter(reporter, ctx, err)
    @ccall libtao.tao_report_error_with_reporter(reporter::Ptr{tao_error_reporter}, ctx::Ptr{Cvoid}, err::Ptr{tao_error})::tao_status
end

function tao_get_last_error()
    @ccall libtao.tao_get_last_error()::Ptr{tao_error}
end

function tao_clear_error(err)
    @ccall libtao.tao_clear_error(err::Ptr{tao_error})::Cvoid
end

function tao_any_errors(err)
    @ccall libtao.tao_any_errors(err::Ptr{tao_error})::Cint
end

function tao_retrieve_error_details(code, reason, info, proc, buffer)
    @ccall libtao.tao_retrieve_error_details(code::Cint, reason::Ptr{Cstring}, info::Ptr{Cstring}, proc::Ptr{tao_error_getter}, buffer::Cstring)::Cvoid
end

function tao_get_error_reason(code)
    @ccall libtao.tao_get_error_reason(code::Cint)::Cstring
end

function tao_get_error_name(code)
    @ccall libtao.tao_get_error_name(code::Cint)::Cstring
end

function tao_indexed_layout_build(inds, msk, dim1, dim2, orient)
    @ccall libtao.tao_indexed_layout_build(inds::Ptr{Clong}, msk::Ptr{UInt8}, dim1::Clong, dim2::Clong, orient::Cuint)::Clong
end

function tao_indexed_layout_check(inds, dim1, dim2)
    @ccall libtao.tao_indexed_layout_check(inds::Ptr{Clong}, dim1::Clong, dim2::Clong)::Clong
end

function tao_layout_mask_create_from_text(shape, nrows, dims)
    @ccall libtao.tao_layout_mask_create_from_text(shape::Ptr{Cstring}, nrows::Clong, dims::Ptr{Clong})::Ptr{UInt8}
end

function tao_layout_mask_create(dim1, dim2, nacts)
    @ccall libtao.tao_layout_mask_create(dim1::Clong, dim2::Clong, nacts::Clong)::Ptr{UInt8}
end

function tao_layout_mask_instantiate(mask, dim1, dim2, nacts, work)
    @ccall libtao.tao_layout_mask_instantiate(mask::Ptr{UInt8}, dim1::Clong, dim2::Clong, nacts::Clong, work::Ptr{Clong})::Ptr{UInt8}
end

function tao_calloc(nelem, elsize)
    @ccall libtao.tao_calloc(nelem::Csize_t, elsize::Csize_t)::Ptr{Cvoid}
end

const tao_event = Cuint

function tao_malloc(size)
    @ccall libtao.tao_malloc(size::Csize_t)::Ptr{Cvoid}
end

function tao_realloc(ptr, size)
    @ccall libtao.tao_realloc(ptr::Ptr{Cvoid}, size::Csize_t)::Ptr{Cvoid}
end

function tao_free(ptr)
    @ccall libtao.tao_free(ptr::Ptr{Cvoid})::Cvoid
end

function tao_strlen(str)
    @ccall libtao.tao_strlen(str::Cstring)::Csize_t
end

function tao_basename(path)
    @ccall libtao.tao_basename(path::Cstring)::Cstring
end

function tao_check_string(str, siz)
    @ccall libtao.tao_check_string(str::Cstring, siz::Clong)::Clong
end

@enum tao_message_level::UInt32 begin
    TAO_MESG_DEBUG = 0
    TAO_MESG_INFO = 1
    TAO_MESG_WARN = 2
    TAO_MESG_ERROR = 3
    TAO_MESG_ASSERT = 4
    TAO_MESG_FATAL = 5
    TAO_MESG_QUIET = 6
end

function tao_message_threshold_get()
    @ccall libtao.tao_message_threshold_get()::tao_message_level
end

function tao_message_threshold_set(level)
    @ccall libtao.tao_message_threshold_set(level::tao_message_level)::Cvoid
end

function tao_split_command(list, cmd, len)
    @ccall libtao.tao_split_command(list::Ptr{Ptr{Cstring}}, cmd::Cstring, len::Clong)::Cint
end

function tao_pack_words(dest, argv, argc)
    @ccall libtao.tao_pack_words(dest::Ptr{tao_buffer}, argv::Ptr{Cstring}, argc::Cint)::tao_status
end

function tao_parse_int(str, ptr, base)
    @ccall libtao.tao_parse_int(str::Cstring, ptr::Ptr{Cint}, base::Cint)::tao_status
end

function tao_parse_long(str, ptr, base)
    @ccall libtao.tao_parse_long(str::Cstring, ptr::Ptr{Clong}, base::Cint)::tao_status
end

function tao_parse_double(str, ptr)
    @ccall libtao.tao_parse_double(str::Cstring, ptr::Ptr{Cdouble})::tao_status
end

function tao_transcode_utf8_string_to_unicode(src, dst, len)
    @ccall libtao.tao_transcode_utf8_string_to_unicode(src::Cstring, dst::Ptr{Cwchar_t}, len::Clong)::tao_status
end

function tao_transcode_unicode_string_to_ascii(src, dst, len)
    @ccall libtao.tao_transcode_unicode_string_to_ascii(src::Ptr{Cwchar_t}, dst::Cstring, len::Clong)::tao_status
end

function tao_transcode_unicode_string_to_utf8(src, dst, len)
    @ccall libtao.tao_transcode_unicode_string_to_utf8(src::Ptr{Cwchar_t}, dst::Cstring, len::Clong)::tao_status
end

@enum tao_preprocessing::UInt32 begin
    TAO_PREPROCESSING_NONE = 0
    TAO_PREPROCESSING_AFFINE = 1
    TAO_PREPROCESSING_FULL = 2
end

struct tao_camera_roi
    xbin::Clong
    ybin::Clong
    xoff::Clong
    yoff::Clong
    width::Clong
    height::Clong
end

function tao_camera_roi_copy(dest, src)
    @ccall libtao.tao_camera_roi_copy(dest::Ptr{tao_camera_roi}, src::Ptr{tao_camera_roi})::Ptr{tao_camera_roi}
end

function tao_camera_roi_define(dest, xbin, ybin, xoff, yoff, width, height)
    @ccall libtao.tao_camera_roi_define(dest::Ptr{tao_camera_roi}, xbin::Clong, ybin::Clong, xoff::Clong, yoff::Clong, width::Clong, height::Clong)::Ptr{tao_camera_roi}
end

function tao_camera_roi_check(roi, sensorwidth, sensorheight)
    @ccall libtao.tao_camera_roi_check(roi::Ptr{tao_camera_roi}, sensorwidth::Clong, sensorheight::Clong)::tao_status
end

function tao_camera_roi_compare(a, b)
    @ccall libtao.tao_camera_roi_compare(a::Ptr{tao_camera_roi}, b::Ptr{tao_camera_roi})::Cint
end

const tao_time_member = Int64

struct tao_time <: Tao.AbstractTimeValue
    sec::tao_time_member
    nsec::tao_time_member
end

struct tao_camera_config
    sensorwidth::Clong
    sensorheight::Clong
    origin::tao_time
    frames::tao_serial
    droppedframes::tao_serial
    overruns::tao_serial
    lostframes::tao_serial
    overflows::tao_serial
    lostsyncs::tao_serial
    timeouts::tao_serial
    roi::tao_camera_roi
    framerate::Cdouble
    exposuretime::Cdouble
    buffers::Clong
    rawencoding::tao_encoding
    pixeltype::tao_eltype
    preprocessing::tao_preprocessing
    attr_table::tao_attr_table
    attr_table_data::NTuple{8192, UInt8}
end

function tao_camera_config_get_sensorwidth(cfg)
    @ccall libtao.tao_camera_config_get_sensorwidth(cfg::Ptr{tao_camera_config})::Clong
end

function tao_camera_config_get_sensorheight(cfg)
    @ccall libtao.tao_camera_config_get_sensorheight(cfg::Ptr{tao_camera_config})::Clong
end

function tao_camera_config_get_origin(cfg)
    @ccall libtao.tao_camera_config_get_origin(cfg::Ptr{tao_camera_config})::tao_time
end

function tao_camera_config_get_frames(cfg)
    @ccall libtao.tao_camera_config_get_frames(cfg::Ptr{tao_camera_config})::tao_serial
end

function tao_camera_config_get_droppedframes(cfg)
    @ccall libtao.tao_camera_config_get_droppedframes(cfg::Ptr{tao_camera_config})::tao_serial
end

function tao_camera_config_get_overruns(cfg)
    @ccall libtao.tao_camera_config_get_overruns(cfg::Ptr{tao_camera_config})::tao_serial
end

function tao_camera_config_get_lostframes(cfg)
    @ccall libtao.tao_camera_config_get_lostframes(cfg::Ptr{tao_camera_config})::tao_serial
end

function tao_camera_config_get_overflows(cfg)
    @ccall libtao.tao_camera_config_get_overflows(cfg::Ptr{tao_camera_config})::tao_serial
end

function tao_camera_config_get_lostsyncs(cfg)
    @ccall libtao.tao_camera_config_get_lostsyncs(cfg::Ptr{tao_camera_config})::tao_serial
end

function tao_camera_config_get_timeouts(cfg)
    @ccall libtao.tao_camera_config_get_timeouts(cfg::Ptr{tao_camera_config})::tao_serial
end

function tao_camera_config_get_xbin(cfg)
    @ccall libtao.tao_camera_config_get_xbin(cfg::Ptr{tao_camera_config})::Clong
end

function tao_camera_config_get_ybin(cfg)
    @ccall libtao.tao_camera_config_get_ybin(cfg::Ptr{tao_camera_config})::Clong
end

function tao_camera_config_get_xoff(cfg)
    @ccall libtao.tao_camera_config_get_xoff(cfg::Ptr{tao_camera_config})::Clong
end

function tao_camera_config_get_yoff(cfg)
    @ccall libtao.tao_camera_config_get_yoff(cfg::Ptr{tao_camera_config})::Clong
end

function tao_camera_config_get_width(cfg)
    @ccall libtao.tao_camera_config_get_width(cfg::Ptr{tao_camera_config})::Clong
end

function tao_camera_config_get_height(cfg)
    @ccall libtao.tao_camera_config_get_height(cfg::Ptr{tao_camera_config})::Clong
end

function tao_camera_config_get_framerate(cfg)
    @ccall libtao.tao_camera_config_get_framerate(cfg::Ptr{tao_camera_config})::Cdouble
end

function tao_camera_config_get_exposuretime(cfg)
    @ccall libtao.tao_camera_config_get_exposuretime(cfg::Ptr{tao_camera_config})::Cdouble
end

function tao_camera_config_get_pixeltype(cfg)
    @ccall libtao.tao_camera_config_get_pixeltype(cfg::Ptr{tao_camera_config})::tao_eltype
end

function tao_camera_config_get_rawencoding(cfg)
    @ccall libtao.tao_camera_config_get_rawencoding(cfg::Ptr{tao_camera_config})::tao_encoding
end

function tao_camera_config_get_preprocessing(cfg)
    @ccall libtao.tao_camera_config_get_preprocessing(cfg::Ptr{tao_camera_config})::tao_preprocessing
end

function tao_camera_config_set_origin(cfg, val)
    @ccall libtao.tao_camera_config_set_origin(cfg::Ptr{tao_camera_config}, val::Ptr{tao_time})::Cvoid
end

function tao_camera_config_set_frames(cfg, val)
    @ccall libtao.tao_camera_config_set_frames(cfg::Ptr{tao_camera_config}, val::tao_serial)::Cvoid
end

function tao_camera_config_set_droppedframes(cfg, val)
    @ccall libtao.tao_camera_config_set_droppedframes(cfg::Ptr{tao_camera_config}, val::tao_serial)::Cvoid
end

function tao_camera_config_set_overruns(cfg, val)
    @ccall libtao.tao_camera_config_set_overruns(cfg::Ptr{tao_camera_config}, val::tao_serial)::Cvoid
end

function tao_camera_config_set_lostframes(cfg, val)
    @ccall libtao.tao_camera_config_set_lostframes(cfg::Ptr{tao_camera_config}, val::tao_serial)::Cvoid
end

function tao_camera_config_set_overflows(cfg, val)
    @ccall libtao.tao_camera_config_set_overflows(cfg::Ptr{tao_camera_config}, val::tao_serial)::Cvoid
end

function tao_camera_config_set_lostsyncs(cfg, val)
    @ccall libtao.tao_camera_config_set_lostsyncs(cfg::Ptr{tao_camera_config}, val::tao_serial)::Cvoid
end

function tao_camera_config_set_timeouts(cfg, val)
    @ccall libtao.tao_camera_config_set_timeouts(cfg::Ptr{tao_camera_config}, val::tao_serial)::Cvoid
end

function tao_camera_config_set_xbin(cfg, val)
    @ccall libtao.tao_camera_config_set_xbin(cfg::Ptr{tao_camera_config}, val::Clong)::Cvoid
end

function tao_camera_config_set_ybin(cfg, val)
    @ccall libtao.tao_camera_config_set_ybin(cfg::Ptr{tao_camera_config}, val::Clong)::Cvoid
end

function tao_camera_config_set_xoff(cfg, val)
    @ccall libtao.tao_camera_config_set_xoff(cfg::Ptr{tao_camera_config}, val::Clong)::Cvoid
end

function tao_camera_config_set_yoff(cfg, val)
    @ccall libtao.tao_camera_config_set_yoff(cfg::Ptr{tao_camera_config}, val::Clong)::Cvoid
end

function tao_camera_config_set_width(cfg, val)
    @ccall libtao.tao_camera_config_set_width(cfg::Ptr{tao_camera_config}, val::Clong)::Cvoid
end

function tao_camera_config_set_height(cfg, val)
    @ccall libtao.tao_camera_config_set_height(cfg::Ptr{tao_camera_config}, val::Clong)::Cvoid
end

function tao_camera_config_set_framerate(cfg, val)
    @ccall libtao.tao_camera_config_set_framerate(cfg::Ptr{tao_camera_config}, val::Cdouble)::Cvoid
end

function tao_camera_config_set_exposuretime(cfg, val)
    @ccall libtao.tao_camera_config_set_exposuretime(cfg::Ptr{tao_camera_config}, val::Cdouble)::Cvoid
end

function tao_camera_config_set_pixeltype(cfg, val)
    @ccall libtao.tao_camera_config_set_pixeltype(cfg::Ptr{tao_camera_config}, val::tao_eltype)::Cvoid
end

function tao_camera_config_set_rawencoding(cfg, val)
    @ccall libtao.tao_camera_config_set_rawencoding(cfg::Ptr{tao_camera_config}, val::tao_encoding)::Cvoid
end

function tao_camera_config_set_preprocessing(cfg, val)
    @ccall libtao.tao_camera_config_set_preprocessing(cfg::Ptr{tao_camera_config}, val::tao_preprocessing)::Cvoid
end

function tao_camera_config_initialize(cfg)
    @ccall libtao.tao_camera_config_initialize(cfg::Ptr{tao_camera_config})::Cvoid
end

function tao_camera_config_copy(dst, src)
    @ccall libtao.tao_camera_config_copy(dst::Ptr{tao_camera_config}, src::Ptr{tao_camera_config})::Cvoid
end

function tao_camera_config_get_attr_number(cfg)
    @ccall libtao.tao_camera_config_get_attr_number(cfg::Ptr{tao_camera_config})::Clong
end

function tao_camera_config_get_attr(cfg, index)
    @ccall libtao.tao_camera_config_get_attr(cfg::Ptr{tao_camera_config}, index::Clong)::Ptr{tao_attr}
end

function tao_camera_config_unsafe_get_attr(cfg, index)
    @ccall libtao.tao_camera_config_unsafe_get_attr(cfg::Ptr{tao_camera_config}, index::Clong)::Ptr{tao_attr}
end

function tao_camera_config_try_find_attr(cfg, key)
    @ccall libtao.tao_camera_config_try_find_attr(cfg::Ptr{tao_camera_config}, key::Cstring)::Clong
end

function tao_camera_config_find_attr(cfg, key)
    @ccall libtao.tao_camera_config_find_attr(cfg::Ptr{tao_camera_config}, key::Cstring)::Ptr{tao_attr}
end

function tao_fast_pixel_type(proc, enc)
    @ccall libtao.tao_fast_pixel_type(proc::tao_preprocessing, enc::tao_encoding)::tao_eltype
end

struct tao_acquisition_buffer
    data::Ptr{Cvoid}
    size::Csize_t
    offset::Clong
    width::Clong
    height::Clong
    stride::Clong
    encoding::tao_encoding
    serial::tao_serial
    frame_start::tao_time
    frame_end::tao_time
    buffer_ready::tao_time
end

abstract type tao_camera end

abstract type tao_camera_ops end

function tao_camera_create(ops, ctx, size)
    @ccall libtao.tao_camera_create(ops::Ptr{tao_camera_ops}, ctx::Ptr{Cvoid}, size::Csize_t)::Ptr{tao_camera}
end

function tao_camera_destroy(cam)
    @ccall libtao.tao_camera_destroy(cam::Ptr{tao_camera})::tao_status
end

function tao_camera_lock(cam)
    @ccall libtao.tao_camera_lock(cam::Ptr{tao_camera})::tao_status
end

function tao_camera_try_lock(cam)
    @ccall libtao.tao_camera_try_lock(cam::Ptr{tao_camera})::tao_status
end

function tao_camera_unlock(cam)
    @ccall libtao.tao_camera_unlock(cam::Ptr{tao_camera})::tao_status
end

function tao_camera_signal(cam)
    @ccall libtao.tao_camera_signal(cam::Ptr{tao_camera})::tao_status
end

function tao_camera_broadcast(cam)
    @ccall libtao.tao_camera_broadcast(cam::Ptr{tao_camera})::tao_status
end

function tao_camera_wait(cam)
    @ccall libtao.tao_camera_wait(cam::Ptr{tao_camera})::tao_status
end

function tao_camera_abstimed_wait(cam, abstime)
    @ccall libtao.tao_camera_abstimed_wait(cam::Ptr{tao_camera}, abstime::Ptr{tao_time})::tao_status
end

function tao_camera_timed_wait(cam, secs)
    @ccall libtao.tao_camera_timed_wait(cam::Ptr{tao_camera}, secs::Cdouble)::tao_status
end

function tao_camera_get_attr_number(cam)
    @ccall libtao.tao_camera_get_attr_number(cam::Ptr{tao_camera})::Clong
end

function tao_camera_get_attr(cam, index)
    @ccall libtao.tao_camera_get_attr(cam::Ptr{tao_camera}, index::Clong)::Ptr{tao_attr}
end

function tao_camera_unsafe_get_attr(cam, index)
    @ccall libtao.tao_camera_unsafe_get_attr(cam::Ptr{tao_camera}, index::Clong)::Ptr{tao_attr}
end

function tao_camera_try_find_attr(cam, key)
    @ccall libtao.tao_camera_try_find_attr(cam::Ptr{tao_camera}, key::Cstring)::Clong
end

function tao_camera_find_attr(cam, key)
    @ccall libtao.tao_camera_find_attr(cam::Ptr{tao_camera}, key::Cstring)::Ptr{tao_attr}
end

function tao_camera_get_information(src, dst)
    @ccall libtao.tao_camera_get_information(src::Ptr{tao_camera}, dst::Ptr{tao_camera_config})::tao_status
end

function tao_camera_configuration_check_preprocessing(cfg)
    @ccall libtao.tao_camera_configuration_check_preprocessing(cfg::Ptr{tao_camera_config})::tao_status
end

function tao_camera_check_configuration(cam, cfg)
    @ccall libtao.tao_camera_check_configuration(cam::Ptr{tao_camera}, cfg::Ptr{tao_camera_config})::tao_status
end

function tao_camera_update_configuration(cam)
    @ccall libtao.tao_camera_update_configuration(cam::Ptr{tao_camera})::tao_status
end

function tao_camera_get_configuration(src, dst)
    @ccall libtao.tao_camera_get_configuration(src::Ptr{tao_camera}, dst::Ptr{tao_camera_config})::tao_status
end

function tao_camera_set_configuration(cam, cfg)
    @ccall libtao.tao_camera_set_configuration(cam::Ptr{tao_camera}, cfg::Ptr{tao_camera_config})::tao_status
end

function tao_camera_wait_acquisition_buffer(cam, buf, secs, drop)
    @ccall libtao.tao_camera_wait_acquisition_buffer(cam::Ptr{tao_camera}, buf::Ptr{tao_acquisition_buffer}, secs::Cdouble, drop::Cint)::tao_status
end

function tao_camera_start_acquisition(cam)
    @ccall libtao.tao_camera_start_acquisition(cam::Ptr{tao_camera})::tao_status
end

function tao_camera_stop_acquisition(cam)
    @ccall libtao.tao_camera_stop_acquisition(cam::Ptr{tao_camera})::tao_status
end

function tao_camera_reset(cam)
    @ccall libtao.tao_camera_reset(cam::Ptr{tao_camera})::tao_status
end

@enum tao_state::UInt32 begin
    TAO_STATE_INITIALIZING = 0
    TAO_STATE_WAITING = 1
    TAO_STATE_CONFIGURING = 2
    TAO_STATE_STARTING = 3
    TAO_STATE_WORKING = 4
    TAO_STATE_STOPPING = 5
    TAO_STATE_ABORTING = 6
    TAO_STATE_ERROR = 7
    TAO_STATE_RESETTING = 8
    TAO_STATE_QUITTING = 9
    TAO_STATE_UNREACHABLE = 10
end

function tao_camera_get_state(cam)
    @ccall libtao.tao_camera_get_state(cam::Ptr{tao_camera})::tao_state
end

function tao_camera_set_origin_of_time(cam, orig)
    @ccall libtao.tao_camera_set_origin_of_time(cam::Ptr{tao_camera}, orig::Ptr{tao_time})::tao_status
end

function tao_camera_get_origin_of_time(orig, cam)
    @ccall libtao.tao_camera_get_origin_of_time(orig::Ptr{tao_time}, cam::Ptr{tao_camera})::Ptr{tao_time}
end

function tao_camera_get_elapsed_seconds(cam, t)
    @ccall libtao.tao_camera_get_elapsed_seconds(cam::Ptr{tao_camera}, t::Ptr{tao_time})::Cdouble
end

function tao_camera_get_elapsed_milliseconds(cam, t)
    @ccall libtao.tao_camera_get_elapsed_milliseconds(cam::Ptr{tao_camera}, t::Ptr{tao_time})::Cdouble
end

function tao_camera_get_elapsed_microseconds(cam, t)
    @ccall libtao.tao_camera_get_elapsed_microseconds(cam::Ptr{tao_camera}, t::Ptr{tao_time})::Cdouble
end

function tao_camera_get_elapsed_nanoseconds(cam, t)
    @ccall libtao.tao_camera_get_elapsed_nanoseconds(cam::Ptr{tao_camera}, t::Ptr{tao_time})::Cdouble
end

abstract type tao_remote_camera end

function tao_remote_camera_create(owner, nbufs, flags)
    @ccall libtao.tao_remote_camera_create(owner::Cstring, nbufs::Clong, flags::Cuint)::Ptr{tao_remote_camera}
end

function tao_remote_camera_attach(shmid)
    @ccall libtao.tao_remote_camera_attach(shmid::tao_shmid)::Ptr{tao_remote_camera}
end

function tao_remote_camera_detach(cam)
    @ccall libtao.tao_remote_camera_detach(cam::Ptr{tao_remote_camera})::tao_status
end

function tao_remote_camera_get_size(cam)
    @ccall libtao.tao_remote_camera_get_size(cam::Ptr{tao_remote_camera})::Csize_t
end

@enum tao_object_type::UInt32 begin
    TAO_NULL_OBJECT = 0
    TAO_SHARED_OBJECT = 2144192000
    TAO_RWLOCKED_OBJECT = 2144192032
    TAO_REMOTE_OBJECT = 2144192064
    TAO_SHARED_ARRAY = 2144192033
    TAO_REMOTE_CAMERA = 2144192066
    TAO_REMOTE_MIRROR = 2144192067
    TAO_REMOTE_SENSOR = 2144192068
end

function tao_remote_camera_get_type(cam)
    @ccall libtao.tao_remote_camera_get_type(cam::Ptr{tao_remote_camera})::tao_object_type
end

function tao_remote_camera_get_flags(obj)
    @ccall libtao.tao_remote_camera_get_flags(obj::Ptr{tao_remote_camera})::UInt32
end

function tao_remote_camera_get_perms(obj)
    @ccall libtao.tao_remote_camera_get_perms(obj::Ptr{tao_remote_camera})::UInt32
end

function tao_remote_camera_get_shmid(cam)
    @ccall libtao.tao_remote_camera_get_shmid(cam::Ptr{tao_remote_camera})::tao_shmid
end

function tao_remote_camera_publish_shmid(obj)
    @ccall libtao.tao_remote_camera_publish_shmid(obj::Ptr{tao_remote_camera})::tao_status
end

function tao_remote_camera_unpublish_shmid(obj)
    @ccall libtao.tao_remote_camera_unpublish_shmid(obj::Ptr{tao_remote_camera})::tao_status
end

function tao_remote_camera_lock(cam)
    @ccall libtao.tao_remote_camera_lock(cam::Ptr{tao_remote_camera})::tao_status
end

function tao_remote_camera_unlock(cam)
    @ccall libtao.tao_remote_camera_unlock(cam::Ptr{tao_remote_camera})::tao_status
end

function tao_remote_camera_try_lock(cam)
    @ccall libtao.tao_remote_camera_try_lock(cam::Ptr{tao_remote_camera})::tao_status
end

function tao_remote_camera_abstimed_lock(cam, lim)
    @ccall libtao.tao_remote_camera_abstimed_lock(cam::Ptr{tao_remote_camera}, lim::Ptr{tao_time})::tao_status
end

function tao_remote_camera_timed_lock(cam, secs)
    @ccall libtao.tao_remote_camera_timed_lock(cam::Ptr{tao_remote_camera}, secs::Cdouble)::tao_status
end

# typedef tao_status tao_remote_camera_callback ( tao_remote_camera * obj , void * data )
const tao_remote_camera_callback = Cvoid

function tao_remote_camera_lock_do(obj, task, task_data, secs)
    @ccall libtao.tao_remote_camera_lock_do(obj::Ptr{tao_remote_camera}, task::Ptr{tao_remote_camera_callback}, task_data::Ptr{Cvoid}, secs::Cdouble)::tao_status
end

function tao_remote_camera_unlock_do(obj, task, task_data, secs)
    @ccall libtao.tao_remote_camera_unlock_do(obj::Ptr{tao_remote_camera}, task::Ptr{tao_remote_camera_callback}, task_data::Ptr{Cvoid}, secs::Cdouble)::tao_status
end

function tao_remote_camera_signal_condition(cam)
    @ccall libtao.tao_remote_camera_signal_condition(cam::Ptr{tao_remote_camera})::tao_status
end

function tao_remote_camera_broadcast_condition(cam)
    @ccall libtao.tao_remote_camera_broadcast_condition(cam::Ptr{tao_remote_camera})::tao_status
end

function tao_remote_camera_wait_condition(cam)
    @ccall libtao.tao_remote_camera_wait_condition(cam::Ptr{tao_remote_camera})::tao_status
end

function tao_remote_camera_abstimed_wait_condition(cam, lim)
    @ccall libtao.tao_remote_camera_abstimed_wait_condition(cam::Ptr{tao_remote_camera}, lim::Ptr{tao_time})::tao_status
end

function tao_remote_camera_timed_wait_condition(cam, secs)
    @ccall libtao.tao_remote_camera_timed_wait_condition(cam::Ptr{tao_remote_camera}, secs::Cdouble)::tao_status
end

function tao_remote_camera_wait(obj, pred, pred_data, secs)
    @ccall libtao.tao_remote_camera_wait(obj::Ptr{tao_remote_camera}, pred::Ptr{tao_remote_camera_callback}, pred_data::Ptr{Cvoid}, secs::Cdouble)::tao_status
end

function tao_remote_camera_lock_wait_do(obj, pred, pred_data, task, task_data, secs)
    @ccall libtao.tao_remote_camera_lock_wait_do(obj::Ptr{tao_remote_camera}, pred::Ptr{tao_remote_camera_callback}, pred_data::Ptr{Cvoid}, task::Ptr{tao_remote_camera_callback}, task_data::Ptr{Cvoid}, secs::Cdouble)::tao_status
end

function tao_remote_camera_get_owner(cam)
    @ccall libtao.tao_remote_camera_get_owner(cam::Ptr{tao_remote_camera})::Cstring
end

function tao_remote_camera_get_pid(cam)
    @ccall libtao.tao_remote_camera_get_pid(cam::Ptr{tao_remote_camera})::pid_t
end

function tao_remote_camera_get_nbufs(cam)
    @ccall libtao.tao_remote_camera_get_nbufs(cam::Ptr{tao_remote_camera})::Clong
end

function tao_remote_camera_get_serial(cam)
    @ccall libtao.tao_remote_camera_get_serial(cam::Ptr{tao_remote_camera})::tao_serial
end

function tao_remote_camera_get_ncmds(cam)
    @ccall libtao.tao_remote_camera_get_ncmds(cam::Ptr{tao_remote_camera})::tao_serial
end

function tao_remote_camera_get_state(cam)
    @ccall libtao.tao_remote_camera_get_state(cam::Ptr{tao_remote_camera})::tao_state
end

function tao_remote_camera_set_state(obj, state)
    @ccall libtao.tao_remote_camera_set_state(obj::Ptr{tao_remote_camera}, state::tao_state)::tao_status
end

@enum tao_command::UInt32 begin
    TAO_COMMAND_NONE = 0
    TAO_COMMAND_RESET = 1
    TAO_COMMAND_SEND = 2
    TAO_COMMAND_CONFIG = 3
    TAO_COMMAND_START = 4
    TAO_COMMAND_STOP = 5
    TAO_COMMAND_ABORT = 6
    TAO_COMMAND_KILL = 7
end

function tao_remote_camera_get_command(obj)
    @ccall libtao.tao_remote_camera_get_command(obj::Ptr{tao_remote_camera})::tao_command
end

function tao_remote_camera_command_done(obj)
    @ccall libtao.tao_remote_camera_command_done(obj::Ptr{tao_remote_camera})::tao_status
end

function tao_remote_camera_is_alive(cam)
    @ccall libtao.tao_remote_camera_is_alive(cam::Ptr{tao_remote_camera})::Cint
end

function tao_remote_camera_get_sensorwidth(cam)
    @ccall libtao.tao_remote_camera_get_sensorwidth(cam::Ptr{tao_remote_camera})::Clong
end

function tao_remote_camera_get_sensorheight(cam)
    @ccall libtao.tao_remote_camera_get_sensorheight(cam::Ptr{tao_remote_camera})::Clong
end

function tao_remote_camera_get_origin(cam)
    @ccall libtao.tao_remote_camera_get_origin(cam::Ptr{tao_remote_camera})::tao_time
end

function tao_remote_camera_get_frames(cam)
    @ccall libtao.tao_remote_camera_get_frames(cam::Ptr{tao_remote_camera})::tao_serial
end

function tao_remote_camera_get_droppedframes(cam)
    @ccall libtao.tao_remote_camera_get_droppedframes(cam::Ptr{tao_remote_camera})::tao_serial
end

function tao_remote_camera_get_overruns(cam)
    @ccall libtao.tao_remote_camera_get_overruns(cam::Ptr{tao_remote_camera})::tao_serial
end

function tao_remote_camera_get_lostframes(cam)
    @ccall libtao.tao_remote_camera_get_lostframes(cam::Ptr{tao_remote_camera})::tao_serial
end

function tao_remote_camera_get_overflows(cam)
    @ccall libtao.tao_remote_camera_get_overflows(cam::Ptr{tao_remote_camera})::tao_serial
end

function tao_remote_camera_get_lostsyncs(cam)
    @ccall libtao.tao_remote_camera_get_lostsyncs(cam::Ptr{tao_remote_camera})::tao_serial
end

function tao_remote_camera_get_timeouts(cam)
    @ccall libtao.tao_remote_camera_get_timeouts(cam::Ptr{tao_remote_camera})::tao_serial
end

function tao_remote_camera_get_xbin(cam)
    @ccall libtao.tao_remote_camera_get_xbin(cam::Ptr{tao_remote_camera})::Clong
end

function tao_remote_camera_get_ybin(cam)
    @ccall libtao.tao_remote_camera_get_ybin(cam::Ptr{tao_remote_camera})::Clong
end

function tao_remote_camera_get_xoff(cam)
    @ccall libtao.tao_remote_camera_get_xoff(cam::Ptr{tao_remote_camera})::Clong
end

function tao_remote_camera_get_yoff(cam)
    @ccall libtao.tao_remote_camera_get_yoff(cam::Ptr{tao_remote_camera})::Clong
end

function tao_remote_camera_get_width(cam)
    @ccall libtao.tao_remote_camera_get_width(cam::Ptr{tao_remote_camera})::Clong
end

function tao_remote_camera_get_height(cam)
    @ccall libtao.tao_remote_camera_get_height(cam::Ptr{tao_remote_camera})::Clong
end

function tao_remote_camera_get_framerate(cam)
    @ccall libtao.tao_remote_camera_get_framerate(cam::Ptr{tao_remote_camera})::Cdouble
end

function tao_remote_camera_get_exposuretime(cam)
    @ccall libtao.tao_remote_camera_get_exposuretime(cam::Ptr{tao_remote_camera})::Cdouble
end

function tao_remote_camera_get_pixeltype(cam)
    @ccall libtao.tao_remote_camera_get_pixeltype(cam::Ptr{tao_remote_camera})::tao_eltype
end

function tao_remote_camera_get_rawencoding(cam)
    @ccall libtao.tao_remote_camera_get_rawencoding(cam::Ptr{tao_remote_camera})::tao_encoding
end

function tao_remote_camera_get_preprocessing(cam)
    @ccall libtao.tao_remote_camera_get_preprocessing(cam::Ptr{tao_remote_camera})::tao_preprocessing
end

function tao_remote_camera_get_preprocessing_shmid(cam, idx)
    @ccall libtao.tao_remote_camera_get_preprocessing_shmid(cam::Ptr{tao_remote_camera}, idx::Cint)::tao_shmid
end

function tao_remote_camera_get_attr_number(cam)
    @ccall libtao.tao_remote_camera_get_attr_number(cam::Ptr{tao_remote_camera})::Clong
end

function tao_remote_camera_get_attr(cam, index)
    @ccall libtao.tao_remote_camera_get_attr(cam::Ptr{tao_remote_camera}, index::Clong)::Ptr{tao_attr}
end

function tao_remote_camera_unsafe_get_attr(cam, index)
    @ccall libtao.tao_remote_camera_unsafe_get_attr(cam::Ptr{tao_remote_camera}, index::Clong)::Ptr{tao_attr}
end

function tao_remote_camera_try_find_attr(cam, key)
    @ccall libtao.tao_remote_camera_try_find_attr(cam::Ptr{tao_remote_camera}, key::Cstring)::Clong
end

function tao_remote_camera_find_attr(cam, key)
    @ccall libtao.tao_remote_camera_find_attr(cam::Ptr{tao_remote_camera}, key::Cstring)::Ptr{tao_attr}
end

function tao_remote_camera_get_configuration(src, dst)
    @ccall libtao.tao_remote_camera_get_configuration(src::Ptr{tao_remote_camera}, dst::Ptr{tao_camera_config})::tao_status
end

function tao_remote_camera_configure(cam, cfg, secs)
    @ccall libtao.tao_remote_camera_configure(cam::Ptr{tao_remote_camera}, cfg::Ptr{tao_camera_config}, secs::Cdouble)::tao_serial
end

function tao_remote_camera_start(cam, secs)
    @ccall libtao.tao_remote_camera_start(cam::Ptr{tao_remote_camera}, secs::Cdouble)::tao_serial
end

function tao_remote_camera_stop(cam, secs)
    @ccall libtao.tao_remote_camera_stop(cam::Ptr{tao_remote_camera}, secs::Cdouble)::tao_serial
end

function tao_remote_camera_abort(cam, secs)
    @ccall libtao.tao_remote_camera_abort(cam::Ptr{tao_remote_camera}, secs::Cdouble)::tao_serial
end

function tao_remote_camera_reset(cam, secs)
    @ccall libtao.tao_remote_camera_reset(cam::Ptr{tao_remote_camera}, secs::Cdouble)::tao_serial
end

function tao_remote_camera_kill(cam, secs)
    @ccall libtao.tao_remote_camera_kill(cam::Ptr{tao_remote_camera}, secs::Cdouble)::tao_serial
end

function tao_remote_camera_send_command(obj, cmd, task, data, secs)
    @ccall libtao.tao_remote_camera_send_command(obj::Ptr{tao_remote_camera}, cmd::tao_command, task::Ptr{tao_remote_camera_callback}, data::Ptr{Cvoid}, secs::Cdouble)::tao_serial
end

function tao_remote_camera_wait_command(cam, num, secs)
    @ccall libtao.tao_remote_camera_wait_command(cam::Ptr{tao_remote_camera}, num::tao_serial, secs::Cdouble)::tao_status
end

function tao_remote_camera_wait_output(cam, serial, secs)
    @ccall libtao.tao_remote_camera_wait_output(cam::Ptr{tao_remote_camera}, serial::tao_serial, secs::Cdouble)::tao_serial
end

function tao_remote_camera_get_image_shmid(cam, serial)
    @ccall libtao.tao_remote_camera_get_image_shmid(cam::Ptr{tao_remote_camera}, serial::tao_serial)::tao_shmid
end

abstract type tao_remote_mirror end

function tao_remote_mirror_create(owner, nbufs, inds, dim1, dim2, cmin, cmax, flags)
    @ccall libtao.tao_remote_mirror_create(owner::Cstring, nbufs::Clong, inds::Ptr{Clong}, dim1::Clong, dim2::Clong, cmin::Cdouble, cmax::Cdouble, flags::Cuint)::Ptr{tao_remote_mirror}
end

function tao_remote_mirror_attach(shmid)
    @ccall libtao.tao_remote_mirror_attach(shmid::tao_shmid)::Ptr{tao_remote_mirror}
end

function tao_remote_mirror_detach(obj)
    @ccall libtao.tao_remote_mirror_detach(obj::Ptr{tao_remote_mirror})::tao_status
end

function tao_remote_mirror_get_size(obj)
    @ccall libtao.tao_remote_mirror_get_size(obj::Ptr{tao_remote_mirror})::Csize_t
end

function tao_remote_mirror_get_type(obj)
    @ccall libtao.tao_remote_mirror_get_type(obj::Ptr{tao_remote_mirror})::tao_object_type
end

function tao_remote_mirror_get_flags(obj)
    @ccall libtao.tao_remote_mirror_get_flags(obj::Ptr{tao_remote_mirror})::UInt32
end

function tao_remote_mirror_get_perms(obj)
    @ccall libtao.tao_remote_mirror_get_perms(obj::Ptr{tao_remote_mirror})::UInt32
end

function tao_remote_mirror_get_shmid(obj)
    @ccall libtao.tao_remote_mirror_get_shmid(obj::Ptr{tao_remote_mirror})::tao_shmid
end

function tao_remote_mirror_publish_shmid(obj)
    @ccall libtao.tao_remote_mirror_publish_shmid(obj::Ptr{tao_remote_mirror})::tao_status
end

function tao_remote_mirror_unpublish_shmid(obj)
    @ccall libtao.tao_remote_mirror_unpublish_shmid(obj::Ptr{tao_remote_mirror})::tao_status
end

function tao_remote_mirror_lock(obj)
    @ccall libtao.tao_remote_mirror_lock(obj::Ptr{tao_remote_mirror})::tao_status
end

function tao_remote_mirror_unlock(obj)
    @ccall libtao.tao_remote_mirror_unlock(obj::Ptr{tao_remote_mirror})::tao_status
end

function tao_remote_mirror_try_lock(obj)
    @ccall libtao.tao_remote_mirror_try_lock(obj::Ptr{tao_remote_mirror})::tao_status
end

function tao_remote_mirror_abstimed_lock(obj, lim)
    @ccall libtao.tao_remote_mirror_abstimed_lock(obj::Ptr{tao_remote_mirror}, lim::Ptr{tao_time})::tao_status
end

function tao_remote_mirror_timed_lock(obj, secs)
    @ccall libtao.tao_remote_mirror_timed_lock(obj::Ptr{tao_remote_mirror}, secs::Cdouble)::tao_status
end

# typedef tao_status tao_remote_mirror_callback ( tao_remote_mirror * obj , void * data )
const tao_remote_mirror_callback = Cvoid

function tao_remote_mirror_lock_do(obj, task, task_data, secs)
    @ccall libtao.tao_remote_mirror_lock_do(obj::Ptr{tao_remote_mirror}, task::Ptr{tao_remote_mirror_callback}, task_data::Ptr{Cvoid}, secs::Cdouble)::tao_status
end

function tao_remote_mirror_unlock_do(obj, task, task_data, secs)
    @ccall libtao.tao_remote_mirror_unlock_do(obj::Ptr{tao_remote_mirror}, task::Ptr{tao_remote_mirror_callback}, task_data::Ptr{Cvoid}, secs::Cdouble)::tao_status
end

function tao_remote_mirror_signal_condition(obj)
    @ccall libtao.tao_remote_mirror_signal_condition(obj::Ptr{tao_remote_mirror})::tao_status
end

function tao_remote_mirror_broadcast_condition(obj)
    @ccall libtao.tao_remote_mirror_broadcast_condition(obj::Ptr{tao_remote_mirror})::tao_status
end

function tao_remote_mirror_wait_condition(obj)
    @ccall libtao.tao_remote_mirror_wait_condition(obj::Ptr{tao_remote_mirror})::tao_status
end

function tao_remote_mirror_abstimed_wait_condition(obj, lim)
    @ccall libtao.tao_remote_mirror_abstimed_wait_condition(obj::Ptr{tao_remote_mirror}, lim::Ptr{tao_time})::tao_status
end

function tao_remote_mirror_timed_wait_condition(obj, secs)
    @ccall libtao.tao_remote_mirror_timed_wait_condition(obj::Ptr{tao_remote_mirror}, secs::Cdouble)::tao_status
end

function tao_remote_mirror_wait(obj, pred, pred_data, secs)
    @ccall libtao.tao_remote_mirror_wait(obj::Ptr{tao_remote_mirror}, pred::Ptr{tao_remote_mirror_callback}, pred_data::Ptr{Cvoid}, secs::Cdouble)::tao_status
end

function tao_remote_mirror_lock_wait_do(obj, pred, pred_data, task, task_data, secs)
    @ccall libtao.tao_remote_mirror_lock_wait_do(obj::Ptr{tao_remote_mirror}, pred::Ptr{tao_remote_mirror_callback}, pred_data::Ptr{Cvoid}, task::Ptr{tao_remote_mirror_callback}, task_data::Ptr{Cvoid}, secs::Cdouble)::tao_status
end

function tao_remote_mirror_get_owner(obj)
    @ccall libtao.tao_remote_mirror_get_owner(obj::Ptr{tao_remote_mirror})::Cstring
end

function tao_remote_mirror_get_pid(obj)
    @ccall libtao.tao_remote_mirror_get_pid(obj::Ptr{tao_remote_mirror})::pid_t
end

function tao_remote_mirror_get_nbufs(obj)
    @ccall libtao.tao_remote_mirror_get_nbufs(obj::Ptr{tao_remote_mirror})::Clong
end

function tao_remote_mirror_get_serial(obj)
    @ccall libtao.tao_remote_mirror_get_serial(obj::Ptr{tao_remote_mirror})::tao_serial
end

function tao_remote_mirror_get_ncmds(obj)
    @ccall libtao.tao_remote_mirror_get_ncmds(obj::Ptr{tao_remote_mirror})::tao_serial
end

function tao_remote_mirror_get_state(obj)
    @ccall libtao.tao_remote_mirror_get_state(obj::Ptr{tao_remote_mirror})::tao_state
end

function tao_remote_mirror_set_state(obj, state)
    @ccall libtao.tao_remote_mirror_set_state(obj::Ptr{tao_remote_mirror}, state::tao_state)::tao_status
end

function tao_remote_mirror_get_command(obj)
    @ccall libtao.tao_remote_mirror_get_command(obj::Ptr{tao_remote_mirror})::tao_command
end

function tao_remote_mirror_command_done(obj)
    @ccall libtao.tao_remote_mirror_command_done(obj::Ptr{tao_remote_mirror})::tao_status
end

function tao_remote_mirror_is_alive(obj)
    @ccall libtao.tao_remote_mirror_is_alive(obj::Ptr{tao_remote_mirror})::Cint
end

function tao_remote_mirror_get_mark(obj)
    @ccall libtao.tao_remote_mirror_get_mark(obj::Ptr{tao_remote_mirror})::tao_serial
end

function tao_remote_mirror_get_nacts(obj)
    @ccall libtao.tao_remote_mirror_get_nacts(obj::Ptr{tao_remote_mirror})::Clong
end

function tao_remote_mirror_get_dims(obj)
    @ccall libtao.tao_remote_mirror_get_dims(obj::Ptr{tao_remote_mirror})::Ptr{Clong}
end

function tao_remote_mirror_get_layout(obj, dims)
    @ccall libtao.tao_remote_mirror_get_layout(obj::Ptr{tao_remote_mirror}, dims::Ptr{Clong})::Ptr{Clong}
end

function tao_remote_mirror_get_cmin(obj)
    @ccall libtao.tao_remote_mirror_get_cmin(obj::Ptr{tao_remote_mirror})::Cdouble
end

function tao_remote_mirror_get_cmax(obj)
    @ccall libtao.tao_remote_mirror_get_cmax(obj::Ptr{tao_remote_mirror})::Cdouble
end

function tao_remote_mirror_get_reference(obj)
    @ccall libtao.tao_remote_mirror_get_reference(obj::Ptr{tao_remote_mirror})::Ptr{Cdouble}
end

function tao_remote_mirror_kill(obj, secs)
    @ccall libtao.tao_remote_mirror_kill(obj::Ptr{tao_remote_mirror}, secs::Cdouble)::tao_serial
end

function tao_remote_mirror_set_reference(obj, vals, nvals, secs, datnum)
    @ccall libtao.tao_remote_mirror_set_reference(obj::Ptr{tao_remote_mirror}, vals::Ptr{Cdouble}, nvals::Clong, secs::Cdouble, datnum::Ptr{tao_serial})::tao_serial
end

function tao_remote_mirror_set_perturbation(obj, vals, nvals, secs, datnum)
    @ccall libtao.tao_remote_mirror_set_perturbation(obj::Ptr{tao_remote_mirror}, vals::Ptr{Cdouble}, nvals::Clong, secs::Cdouble, datnum::Ptr{tao_serial})::tao_serial
end

function tao_remote_mirror_reset(obj, mark, secs, datnum)
    @ccall libtao.tao_remote_mirror_reset(obj::Ptr{tao_remote_mirror}, mark::tao_serial, secs::Cdouble, datnum::Ptr{tao_serial})::tao_serial
end

function tao_remote_mirror_send_commands(obj, vals, nvals, mark, secs, datnum)
    @ccall libtao.tao_remote_mirror_send_commands(obj::Ptr{tao_remote_mirror}, vals::Ptr{Cdouble}, nvals::Clong, mark::tao_serial, secs::Cdouble, datnum::Ptr{tao_serial})::tao_serial
end

function tao_remote_mirror_send_command(obj, cmd, task, task_data, secs)
    @ccall libtao.tao_remote_mirror_send_command(obj::Ptr{tao_remote_mirror}, cmd::tao_command, task::Ptr{tao_remote_mirror_callback}, task_data::Ptr{Cvoid}, secs::Cdouble)::tao_serial
end

function tao_remote_mirror_wait_command(obj, cmdnum, secs)
    @ccall libtao.tao_remote_mirror_wait_command(obj::Ptr{tao_remote_mirror}, cmdnum::tao_serial, secs::Cdouble)::tao_status
end

function tao_remote_mirror_wait_output(obj, datnum, secs)
    @ccall libtao.tao_remote_mirror_wait_output(obj::Ptr{tao_remote_mirror}, datnum::tao_serial, secs::Cdouble)::tao_serial
end

function tao_remote_mirror_find_mark(obj, mrk)
    @ccall libtao.tao_remote_mirror_find_mark(obj::Ptr{tao_remote_mirror}, mrk::tao_serial)::tao_serial
end

struct tao_dataframe_info
    serial::tao_serial
    mark::tao_serial
    time::tao_time
end

function tao_remote_mirror_fetch_data(obj, datnum, refcmds, perturb, reqcmds, devcmds, nvals, info)
    @ccall libtao.tao_remote_mirror_fetch_data(obj::Ptr{tao_remote_mirror}, datnum::tao_serial, refcmds::Ptr{Cdouble}, perturb::Ptr{Cdouble}, reqcmds::Ptr{Cdouble}, devcmds::Ptr{Cdouble}, nvals::Clong, info::Ptr{tao_dataframe_info})::tao_status
end

struct tao_remote_mirror_operations
    on_send::Ptr{Cvoid}
    name::Cstring
    debug::Bool
end

function tao_remote_mirror_run_loop(obj, ops, ctx)
    @ccall libtao.tao_remote_mirror_run_loop(obj::Ptr{tao_remote_mirror}, ops::Ptr{tao_remote_mirror_operations}, ctx::Ptr{Cvoid})::tao_status
end

function tao_command_get_name(cmd)
    @ccall libtao.tao_command_get_name(cmd::tao_command)::Cstring
end

function tao_state_get_name(state)
    @ccall libtao.tao_state_get_name(state::tao_state)::Cstring
end

function tao_check_owner_name(owner)
    @ccall libtao.tao_check_owner_name(owner::Cstring)::tao_status
end

abstract type tao_remote_object end

function tao_remote_object_create(owner, type, nbufs, offset, stride, size, flags)
    @ccall libtao.tao_remote_object_create(owner::Cstring, type::UInt32, nbufs::Clong, offset::Clong, stride::Clong, size::Csize_t, flags::Cuint)::Ptr{tao_remote_object}
end

function tao_remote_object_attach(shmid)
    @ccall libtao.tao_remote_object_attach(shmid::tao_shmid)::Ptr{tao_remote_object}
end

function tao_remote_object_detach(obj)
    @ccall libtao.tao_remote_object_detach(obj::Ptr{tao_remote_object})::tao_status
end

function tao_remote_object_get_size(obj)
    @ccall libtao.tao_remote_object_get_size(obj::Ptr{tao_remote_object})::Csize_t
end

function tao_remote_object_get_type(obj)
    @ccall libtao.tao_remote_object_get_type(obj::Ptr{tao_remote_object})::tao_object_type
end

function tao_remote_object_get_flags(obj)
    @ccall libtao.tao_remote_object_get_flags(obj::Ptr{tao_remote_object})::UInt32
end

function tao_remote_object_get_perms(obj)
    @ccall libtao.tao_remote_object_get_perms(obj::Ptr{tao_remote_object})::UInt32
end

function tao_remote_object_get_shmid(obj)
    @ccall libtao.tao_remote_object_get_shmid(obj::Ptr{tao_remote_object})::tao_shmid
end

function tao_remote_object_publish_shmid(obj)
    @ccall libtao.tao_remote_object_publish_shmid(obj::Ptr{tao_remote_object})::tao_status
end

function tao_remote_object_unpublish_shmid(obj)
    @ccall libtao.tao_remote_object_unpublish_shmid(obj::Ptr{tao_remote_object})::tao_status
end

function tao_remote_object_lock(obj)
    @ccall libtao.tao_remote_object_lock(obj::Ptr{tao_remote_object})::tao_status
end

function tao_remote_object_unlock(obj)
    @ccall libtao.tao_remote_object_unlock(obj::Ptr{tao_remote_object})::tao_status
end

function tao_remote_object_try_lock(obj)
    @ccall libtao.tao_remote_object_try_lock(obj::Ptr{tao_remote_object})::tao_status
end

function tao_remote_object_abstimed_lock(obj, lim)
    @ccall libtao.tao_remote_object_abstimed_lock(obj::Ptr{tao_remote_object}, lim::Ptr{tao_time})::tao_status
end

function tao_remote_object_timed_lock(obj, secs)
    @ccall libtao.tao_remote_object_timed_lock(obj::Ptr{tao_remote_object}, secs::Cdouble)::tao_status
end

# typedef tao_status tao_remote_object_callback ( tao_remote_object * obj , void * data )
const tao_remote_object_callback = Cvoid

function tao_remote_object_lock_do(obj, task, task_data, secs)
    @ccall libtao.tao_remote_object_lock_do(obj::Ptr{tao_remote_object}, task::Ptr{tao_remote_object_callback}, task_data::Ptr{Cvoid}, secs::Cdouble)::tao_status
end

function tao_remote_object_unlock_do(obj, task, task_data, secs)
    @ccall libtao.tao_remote_object_unlock_do(obj::Ptr{tao_remote_object}, task::Ptr{tao_remote_object_callback}, task_data::Ptr{Cvoid}, secs::Cdouble)::tao_status
end

function tao_remote_object_signal_condition(obj)
    @ccall libtao.tao_remote_object_signal_condition(obj::Ptr{tao_remote_object})::tao_status
end

function tao_remote_object_broadcast_condition(obj)
    @ccall libtao.tao_remote_object_broadcast_condition(obj::Ptr{tao_remote_object})::tao_status
end

function tao_remote_object_wait_condition(obj)
    @ccall libtao.tao_remote_object_wait_condition(obj::Ptr{tao_remote_object})::tao_status
end

function tao_remote_object_abstimed_wait_condition(obj, lim)
    @ccall libtao.tao_remote_object_abstimed_wait_condition(obj::Ptr{tao_remote_object}, lim::Ptr{tao_time})::tao_status
end

function tao_remote_object_timed_wait_condition(obj, secs)
    @ccall libtao.tao_remote_object_timed_wait_condition(obj::Ptr{tao_remote_object}, secs::Cdouble)::tao_status
end

function tao_remote_object_wait(obj, pred, pred_data, secs)
    @ccall libtao.tao_remote_object_wait(obj::Ptr{tao_remote_object}, pred::Ptr{tao_remote_object_callback}, pred_data::Ptr{Cvoid}, secs::Cdouble)::tao_status
end

function tao_remote_object_lock_wait_do(obj, pred, pred_data, task, task_data, secs)
    @ccall libtao.tao_remote_object_lock_wait_do(obj::Ptr{tao_remote_object}, pred::Ptr{tao_remote_object_callback}, pred_data::Ptr{Cvoid}, task::Ptr{tao_remote_object_callback}, task_data::Ptr{Cvoid}, secs::Cdouble)::tao_status
end

function tao_remote_object_get_owner(obj)
    @ccall libtao.tao_remote_object_get_owner(obj::Ptr{tao_remote_object})::Cstring
end

function tao_remote_object_get_pid(obj)
    @ccall libtao.tao_remote_object_get_pid(obj::Ptr{tao_remote_object})::pid_t
end

function tao_remote_object_get_nbufs(obj)
    @ccall libtao.tao_remote_object_get_nbufs(obj::Ptr{tao_remote_object})::Clong
end

function tao_remote_object_get_serial(obj)
    @ccall libtao.tao_remote_object_get_serial(obj::Ptr{tao_remote_object})::tao_serial
end

function tao_remote_object_get_ncmds(obj)
    @ccall libtao.tao_remote_object_get_ncmds(obj::Ptr{tao_remote_object})::tao_serial
end

function tao_remote_object_get_state(obj)
    @ccall libtao.tao_remote_object_get_state(obj::Ptr{tao_remote_object})::tao_state
end

function tao_remote_object_set_state(obj, state)
    @ccall libtao.tao_remote_object_set_state(obj::Ptr{tao_remote_object}, state::tao_state)::tao_status
end

function tao_remote_object_get_command(obj)
    @ccall libtao.tao_remote_object_get_command(obj::Ptr{tao_remote_object})::tao_command
end

function tao_remote_object_command_done(obj)
    @ccall libtao.tao_remote_object_command_done(obj::Ptr{tao_remote_object})::tao_status
end

function tao_remote_object_is_alive(obj)
    @ccall libtao.tao_remote_object_is_alive(obj::Ptr{tao_remote_object})::Cint
end

function tao_remote_object_send_command(obj, cmd, task, task_data, secs)
    @ccall libtao.tao_remote_object_send_command(obj::Ptr{tao_remote_object}, cmd::tao_command, task::Ptr{tao_remote_object_callback}, task_data::Ptr{Cvoid}, secs::Cdouble)::tao_serial
end

function tao_remote_object_wait_command(obj, num, secs)
    @ccall libtao.tao_remote_object_wait_command(obj::Ptr{tao_remote_object}, num::tao_serial, secs::Cdouble)::tao_status
end

function tao_remote_object_wait_output(obj, num, secs)
    @ccall libtao.tao_remote_object_wait_output(obj::Ptr{tao_remote_object}, num::tao_serial, secs::Cdouble)::tao_serial
end

function tao_remote_object_kill(obj, secs)
    @ccall libtao.tao_remote_object_kill(obj::Ptr{tao_remote_object}, secs::Cdouble)::tao_serial
end

abstract type tao_remote_sensor end

function tao_remote_sensor_create(owner, nbufs, max_ninds, max_nsubs, flags)
    @ccall libtao.tao_remote_sensor_create(owner::Cstring, nbufs::Clong, max_ninds::Clong, max_nsubs::Clong, flags::Cuint)::Ptr{tao_remote_sensor}
end

function tao_remote_sensor_attach(shmid)
    @ccall libtao.tao_remote_sensor_attach(shmid::tao_shmid)::Ptr{tao_remote_sensor}
end

function tao_remote_sensor_detach(wfs)
    @ccall libtao.tao_remote_sensor_detach(wfs::Ptr{tao_remote_sensor})::tao_status
end

function tao_remote_sensor_get_size(wfs)
    @ccall libtao.tao_remote_sensor_get_size(wfs::Ptr{tao_remote_sensor})::Csize_t
end

function tao_remote_sensor_get_type(wfs)
    @ccall libtao.tao_remote_sensor_get_type(wfs::Ptr{tao_remote_sensor})::tao_object_type
end

function tao_remote_sensor_get_flags(obj)
    @ccall libtao.tao_remote_sensor_get_flags(obj::Ptr{tao_remote_sensor})::UInt32
end

function tao_remote_sensor_get_perms(obj)
    @ccall libtao.tao_remote_sensor_get_perms(obj::Ptr{tao_remote_sensor})::UInt32
end

function tao_remote_sensor_get_shmid(wfs)
    @ccall libtao.tao_remote_sensor_get_shmid(wfs::Ptr{tao_remote_sensor})::tao_shmid
end

function tao_remote_sensor_publish_shmid(obj)
    @ccall libtao.tao_remote_sensor_publish_shmid(obj::Ptr{tao_remote_sensor})::tao_status
end

function tao_remote_sensor_unpublish_shmid(obj)
    @ccall libtao.tao_remote_sensor_unpublish_shmid(obj::Ptr{tao_remote_sensor})::tao_status
end

function tao_remote_sensor_lock(wfs)
    @ccall libtao.tao_remote_sensor_lock(wfs::Ptr{tao_remote_sensor})::tao_status
end

function tao_remote_sensor_unlock(wfs)
    @ccall libtao.tao_remote_sensor_unlock(wfs::Ptr{tao_remote_sensor})::tao_status
end

function tao_remote_sensor_try_lock(wfs)
    @ccall libtao.tao_remote_sensor_try_lock(wfs::Ptr{tao_remote_sensor})::tao_status
end

function tao_remote_sensor_abstimed_lock(wfs, lim)
    @ccall libtao.tao_remote_sensor_abstimed_lock(wfs::Ptr{tao_remote_sensor}, lim::Ptr{tao_time})::tao_status
end

function tao_remote_sensor_timed_lock(wfs, secs)
    @ccall libtao.tao_remote_sensor_timed_lock(wfs::Ptr{tao_remote_sensor}, secs::Cdouble)::tao_status
end

# typedef tao_status tao_remote_sensor_callback ( tao_remote_sensor * obj , void * data )
const tao_remote_sensor_callback = Cvoid

function tao_remote_sensor_lock_do(obj, task, task_data, secs)
    @ccall libtao.tao_remote_sensor_lock_do(obj::Ptr{tao_remote_sensor}, task::Ptr{tao_remote_sensor_callback}, task_data::Ptr{Cvoid}, secs::Cdouble)::tao_status
end

function tao_remote_sensor_unlock_do(obj, task, task_data, secs)
    @ccall libtao.tao_remote_sensor_unlock_do(obj::Ptr{tao_remote_sensor}, task::Ptr{tao_remote_sensor_callback}, task_data::Ptr{Cvoid}, secs::Cdouble)::tao_status
end

function tao_remote_sensor_signal_condition(wfs)
    @ccall libtao.tao_remote_sensor_signal_condition(wfs::Ptr{tao_remote_sensor})::tao_status
end

function tao_remote_sensor_broadcast_condition(wfs)
    @ccall libtao.tao_remote_sensor_broadcast_condition(wfs::Ptr{tao_remote_sensor})::tao_status
end

function tao_remote_sensor_wait_condition(wfs)
    @ccall libtao.tao_remote_sensor_wait_condition(wfs::Ptr{tao_remote_sensor})::tao_status
end

function tao_remote_sensor_abstimed_wait_condition(wfs, lim)
    @ccall libtao.tao_remote_sensor_abstimed_wait_condition(wfs::Ptr{tao_remote_sensor}, lim::Ptr{tao_time})::tao_status
end

function tao_remote_sensor_timed_wait_condition(wfs, secs)
    @ccall libtao.tao_remote_sensor_timed_wait_condition(wfs::Ptr{tao_remote_sensor}, secs::Cdouble)::tao_status
end

function tao_remote_sensor_wait(obj, pred, pred_data, secs)
    @ccall libtao.tao_remote_sensor_wait(obj::Ptr{tao_remote_sensor}, pred::Ptr{tao_remote_sensor_callback}, pred_data::Ptr{Cvoid}, secs::Cdouble)::tao_status
end

function tao_remote_sensor_lock_wait_do(obj, pred, pred_data, task, task_data, secs)
    @ccall libtao.tao_remote_sensor_lock_wait_do(obj::Ptr{tao_remote_sensor}, pred::Ptr{tao_remote_sensor_callback}, pred_data::Ptr{Cvoid}, task::Ptr{tao_remote_sensor_callback}, task_data::Ptr{Cvoid}, secs::Cdouble)::tao_status
end

function tao_remote_sensor_get_owner(wfs)
    @ccall libtao.tao_remote_sensor_get_owner(wfs::Ptr{tao_remote_sensor})::Cstring
end

function tao_remote_sensor_get_pid(obj)
    @ccall libtao.tao_remote_sensor_get_pid(obj::Ptr{tao_remote_sensor})::pid_t
end

function tao_remote_sensor_get_nbufs(wfs)
    @ccall libtao.tao_remote_sensor_get_nbufs(wfs::Ptr{tao_remote_sensor})::Clong
end

function tao_remote_sensor_get_serial(wfs)
    @ccall libtao.tao_remote_sensor_get_serial(wfs::Ptr{tao_remote_sensor})::tao_serial
end

function tao_remote_sensor_get_ncmds(wfs)
    @ccall libtao.tao_remote_sensor_get_ncmds(wfs::Ptr{tao_remote_sensor})::tao_serial
end

function tao_remote_sensor_get_state(wfs)
    @ccall libtao.tao_remote_sensor_get_state(wfs::Ptr{tao_remote_sensor})::tao_state
end

function tao_remote_sensor_set_state(obj, state)
    @ccall libtao.tao_remote_sensor_set_state(obj::Ptr{tao_remote_sensor}, state::tao_state)::tao_status
end

function tao_remote_sensor_get_command(obj)
    @ccall libtao.tao_remote_sensor_get_command(obj::Ptr{tao_remote_sensor})::tao_command
end

function tao_remote_sensor_command_done(obj)
    @ccall libtao.tao_remote_sensor_command_done(obj::Ptr{tao_remote_sensor})::tao_status
end

function tao_remote_sensor_is_alive(wfs)
    @ccall libtao.tao_remote_sensor_is_alive(wfs::Ptr{tao_remote_sensor})::Cint
end

function tao_remote_sensor_get_ninds(wfs)
    @ccall libtao.tao_remote_sensor_get_ninds(wfs::Ptr{tao_remote_sensor})::Clong
end

function tao_remote_sensor_get_max_ninds(wfs)
    @ccall libtao.tao_remote_sensor_get_max_ninds(wfs::Ptr{tao_remote_sensor})::Clong
end

function tao_remote_sensor_get_dims(wfs)
    @ccall libtao.tao_remote_sensor_get_dims(wfs::Ptr{tao_remote_sensor})::Ptr{Clong}
end

function tao_remote_sensor_get_inds(wfs, dims)
    @ccall libtao.tao_remote_sensor_get_inds(wfs::Ptr{tao_remote_sensor}, dims::Ptr{Clong})::Ptr{Clong}
end

function tao_remote_sensor_get_nsubs(wfs)
    @ccall libtao.tao_remote_sensor_get_nsubs(wfs::Ptr{tao_remote_sensor})::Clong
end

function tao_remote_sensor_get_max_nsubs(wfs)
    @ccall libtao.tao_remote_sensor_get_max_nsubs(wfs::Ptr{tao_remote_sensor})::Clong
end

struct tao_bounding_box
    xoff::UInt16
    yoff::UInt16
    width::UInt16
    height::UInt16
end

struct tao_position
    x::Cdouble
    y::Cdouble
end

struct tao_subimage
    box::tao_bounding_box
    ref::tao_position
end

function tao_remote_sensor_get_subs(wfs, nsubs)
    @ccall libtao.tao_remote_sensor_get_subs(wfs::Ptr{tao_remote_sensor}, nsubs::Ptr{Clong})::Ptr{tao_subimage}
end

function tao_remote_sensor_get_camera_owner(wfs)
    @ccall libtao.tao_remote_sensor_get_camera_owner(wfs::Ptr{tao_remote_sensor})::Cstring
end

struct tao_measured_position
    x::Cdouble
    y::Cdouble
    wxx::Cdouble
    wxy::Cdouble
    wyy::Cdouble
end

struct tao_shackhartmann_data
    box::tao_bounding_box
    ref::tao_position
    pos::tao_measured_position
    alpha::Cdouble
    eta::Cdouble
end

function tao_remote_sensor_fetch_data(obj, datnum, data, nvals, info)
    @ccall libtao.tao_remote_sensor_fetch_data(obj::Ptr{tao_remote_sensor}, datnum::tao_serial, data::Ptr{tao_shackhartmann_data}, nvals::Clong, info::Ptr{tao_dataframe_info})::tao_status
end

struct tao_sensor_control
    forgetting_factor::Cdouble
    smoothness_level::Cdouble
    restoring_force::Cdouble
    max_excursion::Cdouble
end

function tao_remote_sensor_get_control(wfs, ctrl)
    @ccall libtao.tao_remote_sensor_get_control(wfs::Ptr{tao_remote_sensor}, ctrl::Ptr{tao_sensor_control})::tao_status
end

function tao_remote_sensor_set_control(wfs, ctrl)
    @ccall libtao.tao_remote_sensor_set_control(wfs::Ptr{tao_remote_sensor}, ctrl::Ptr{tao_sensor_control})::tao_status
end

@enum tao_algorithm::UInt32 begin
    TAO_CENTER_OF_GRAVITY = 0
    TAO_LINEARIZED_MATCHED_FILTER = 1
end

struct tao_sensor_params
    algorithm::tao_algorithm
    ctrl::tao_sensor_control
    dims::NTuple{2, Clong}
    nsubs::Clong
    camera::NTuple{64, Cchar}
end

struct tao_sensor_config
    size::Csize_t
    subs_offset::Csize_t
    params::tao_sensor_params
    inds::Ptr{Clong}
end

function tao_remote_sensor_get_config(wfs, cfg)
    @ccall libtao.tao_remote_sensor_get_config(wfs::Ptr{tao_remote_sensor}, cfg::Ptr{tao_sensor_config})::tao_status
end

function tao_remote_sensor_configure(wfs, cfg, secs)
    @ccall libtao.tao_remote_sensor_configure(wfs::Ptr{tao_remote_sensor}, cfg::Ptr{tao_sensor_config}, secs::Cdouble)::tao_serial
end

function tao_remote_sensor_accept_config(wfs)
    @ccall libtao.tao_remote_sensor_accept_config(wfs::Ptr{tao_remote_sensor})::tao_status
end

function tao_remote_sensor_start(obj, secs)
    @ccall libtao.tao_remote_sensor_start(obj::Ptr{tao_remote_sensor}, secs::Cdouble)::tao_serial
end

function tao_remote_sensor_abort(obj, secs)
    @ccall libtao.tao_remote_sensor_abort(obj::Ptr{tao_remote_sensor}, secs::Cdouble)::tao_serial
end

function tao_remote_sensor_reset(obj, secs)
    @ccall libtao.tao_remote_sensor_reset(obj::Ptr{tao_remote_sensor}, secs::Cdouble)::tao_serial
end

function tao_remote_sensor_stop(obj, secs)
    @ccall libtao.tao_remote_sensor_stop(obj::Ptr{tao_remote_sensor}, secs::Cdouble)::tao_serial
end

function tao_remote_sensor_kill(wfs, secs)
    @ccall libtao.tao_remote_sensor_kill(wfs::Ptr{tao_remote_sensor}, secs::Cdouble)::tao_serial
end

function tao_remote_sensor_send_command(obj, cmd, task, task_data, secs)
    @ccall libtao.tao_remote_sensor_send_command(obj::Ptr{tao_remote_sensor}, cmd::tao_command, task::Ptr{tao_remote_sensor_callback}, task_data::Ptr{Cvoid}, secs::Cdouble)::tao_serial
end

function tao_remote_sensor_wait_command(wfs, num, secs)
    @ccall libtao.tao_remote_sensor_wait_command(wfs::Ptr{tao_remote_sensor}, num::tao_serial, secs::Cdouble)::tao_status
end

function tao_remote_sensor_wait_output(wfs, serial, secs)
    @ccall libtao.tao_remote_sensor_wait_output(wfs::Ptr{tao_remote_sensor}, serial::tao_serial, secs::Cdouble)::tao_serial
end

abstract type tao_rwlocked_object end

function tao_rwlocked_object_create(type, size, flags)
    @ccall libtao.tao_rwlocked_object_create(type::UInt32, size::Csize_t, flags::Cuint)::Ptr{tao_rwlocked_object}
end

function tao_rwlocked_object_attach(shmid)
    @ccall libtao.tao_rwlocked_object_attach(shmid::tao_shmid)::Ptr{tao_rwlocked_object}
end

function tao_rwlocked_object_detach(obj)
    @ccall libtao.tao_rwlocked_object_detach(obj::Ptr{tao_rwlocked_object})::tao_status
end

function tao_rwlocked_object_get_size(obj)
    @ccall libtao.tao_rwlocked_object_get_size(obj::Ptr{tao_rwlocked_object})::Csize_t
end

function tao_rwlocked_object_get_type(obj)
    @ccall libtao.tao_rwlocked_object_get_type(obj::Ptr{tao_rwlocked_object})::tao_object_type
end

function tao_rwlocked_object_get_flags(obj)
    @ccall libtao.tao_rwlocked_object_get_flags(obj::Ptr{tao_rwlocked_object})::UInt32
end

function tao_rwlocked_object_get_perms(obj)
    @ccall libtao.tao_rwlocked_object_get_perms(obj::Ptr{tao_rwlocked_object})::UInt32
end

function tao_rwlocked_object_get_shmid(obj)
    @ccall libtao.tao_rwlocked_object_get_shmid(obj::Ptr{tao_rwlocked_object})::tao_shmid
end

function tao_rwlocked_object_rdlock(obj)
    @ccall libtao.tao_rwlocked_object_rdlock(obj::Ptr{tao_rwlocked_object})::tao_status
end

function tao_rwlocked_object_wrlock(obj)
    @ccall libtao.tao_rwlocked_object_wrlock(obj::Ptr{tao_rwlocked_object})::tao_status
end

function tao_rwlocked_object_unlock(obj)
    @ccall libtao.tao_rwlocked_object_unlock(obj::Ptr{tao_rwlocked_object})::tao_status
end

function tao_rwlocked_object_try_rdlock(obj)
    @ccall libtao.tao_rwlocked_object_try_rdlock(obj::Ptr{tao_rwlocked_object})::tao_status
end

function tao_rwlocked_object_try_wrlock(obj)
    @ccall libtao.tao_rwlocked_object_try_wrlock(obj::Ptr{tao_rwlocked_object})::tao_status
end

function tao_rwlocked_object_timed_rdlock(obj, secs)
    @ccall libtao.tao_rwlocked_object_timed_rdlock(obj::Ptr{tao_rwlocked_object}, secs::Cdouble)::tao_status
end

function tao_rwlocked_object_timed_wrlock(obj, secs)
    @ccall libtao.tao_rwlocked_object_timed_wrlock(obj::Ptr{tao_rwlocked_object}, secs::Cdouble)::tao_status
end

function tao_rwlocked_object_abstimed_rdlock(obj, abstime)
    @ccall libtao.tao_rwlocked_object_abstimed_rdlock(obj::Ptr{tao_rwlocked_object}, abstime::Ptr{tao_time})::tao_status
end

function tao_rwlocked_object_abstimed_wrlock(obj, abstime)
    @ccall libtao.tao_rwlocked_object_abstimed_wrlock(obj::Ptr{tao_rwlocked_object}, abstime::Ptr{tao_time})::tao_status
end

# typedef tao_status tao_rwlocked_object_callback ( tao_rwlocked_object * obj , void * data )
const tao_rwlocked_object_callback = Cvoid

function tao_rwlocked_object_rdlock_do(obj, task, task_data, secs)
    @ccall libtao.tao_rwlocked_object_rdlock_do(obj::Ptr{tao_rwlocked_object}, task::Ptr{tao_rwlocked_object_callback}, task_data::Ptr{Cvoid}, secs::Cdouble)::tao_status
end

function tao_rwlocked_object_wrlock_do(obj, task, task_data, secs)
    @ccall libtao.tao_rwlocked_object_wrlock_do(obj::Ptr{tao_rwlocked_object}, task::Ptr{tao_rwlocked_object_callback}, task_data::Ptr{Cvoid}, secs::Cdouble)::tao_status
end

function tao_rwlocked_object_unlock_do(obj, task, task_data, rw, secs)
    @ccall libtao.tao_rwlocked_object_unlock_do(obj::Ptr{tao_rwlocked_object}, task::Ptr{tao_rwlocked_object_callback}, task_data::Ptr{Cvoid}, rw::Bool, secs::Cdouble)::tao_status
end

function tao_sensor_control_check(ctrl)
    @ccall libtao.tao_sensor_control_check(ctrl::Ptr{tao_sensor_control})::tao_status
end

function tao_sensor_params_check(cfg, inds, ninds, subs, nsubs)
    @ccall libtao.tao_sensor_params_check(cfg::Ptr{tao_sensor_params}, inds::Ptr{Clong}, ninds::Clong, subs::Ptr{tao_subimage}, nsubs::Clong)::tao_status
end

function tao_sensor_params_tune(dst, src)
    @ccall libtao.tao_sensor_params_tune(dst::Ptr{tao_sensor_params}, src::Ptr{tao_sensor_params})::tao_status
end

function tao_sensor_config_initialize(cfg, size)
    @ccall libtao.tao_sensor_config_initialize(cfg::Ptr{tao_sensor_config}, size::Csize_t)::tao_status
end

function tao_sensor_config_create(size)
    @ccall libtao.tao_sensor_config_create(size::Csize_t)::Ptr{tao_sensor_config}
end

function tao_sensor_config_destroy(cfg)
    @ccall libtao.tao_sensor_config_destroy(cfg::Ptr{tao_sensor_config})::Cvoid
end

function tao_sensor_config_check(cfg)
    @ccall libtao.tao_sensor_config_check(cfg::Ptr{tao_sensor_config})::tao_status
end

function tao_sensor_config_copy(dst, src)
    @ccall libtao.tao_sensor_config_copy(dst::Ptr{tao_sensor_config}, src::Ptr{tao_sensor_config})::tao_status
end

function tao_sensor_config_instanciate(config, params, inds, ninds, subs, nsubs)
    @ccall libtao.tao_sensor_config_instanciate(config::Ptr{tao_sensor_config}, params::Ptr{tao_sensor_params}, inds::Ptr{Clong}, ninds::Clong, subs::Ptr{tao_subimage}, nsubs::Clong)::tao_status
end

function tao_sensor_config_get_base(cfg)
    @ccall libtao.tao_sensor_config_get_base(cfg::Ptr{tao_sensor_config})::Ptr{tao_sensor_params}
end

function tao_sensor_config_get_camera(cfg)
    @ccall libtao.tao_sensor_config_get_camera(cfg::Ptr{tao_sensor_config})::Cstring
end

function tao_sensor_config_get_inds(cfg, ninds)
    @ccall libtao.tao_sensor_config_get_inds(cfg::Ptr{tao_sensor_config}, ninds::Ptr{Clong})::Ptr{Clong}
end

function tao_sensor_config_get_subs(cfg, nsubs)
    @ccall libtao.tao_sensor_config_get_subs(cfg::Ptr{tao_sensor_config}, nsubs::Ptr{Clong})::Ptr{tao_subimage}
end

abstract type tao_shared_array end

function tao_shared_array_create(eltype, ndims, dims, flags)
    @ccall libtao.tao_shared_array_create(eltype::tao_eltype, ndims::Cint, dims::Ptr{Clong}, flags::Cuint)::Ptr{tao_shared_array}
end

function tao_shared_array_create_1d(eltype, dim, flags)
    @ccall libtao.tao_shared_array_create_1d(eltype::tao_eltype, dim::Clong, flags::Cuint)::Ptr{tao_shared_array}
end

function tao_shared_array_create_2d(eltype, dim1, dim2, flags)
    @ccall libtao.tao_shared_array_create_2d(eltype::tao_eltype, dim1::Clong, dim2::Clong, flags::Cuint)::Ptr{tao_shared_array}
end

function tao_shared_array_create_3d(eltype, dim1, dim2, dim3, flags)
    @ccall libtao.tao_shared_array_create_3d(eltype::tao_eltype, dim1::Clong, dim2::Clong, dim3::Clong, flags::Cuint)::Ptr{tao_shared_array}
end

function tao_shared_array_attach(shmid)
    @ccall libtao.tao_shared_array_attach(shmid::tao_shmid)::Ptr{tao_shared_array}
end

function tao_shared_array_detach(arr)
    @ccall libtao.tao_shared_array_detach(arr::Ptr{tao_shared_array})::tao_status
end

function tao_shared_array_get_size(arr)
    @ccall libtao.tao_shared_array_get_size(arr::Ptr{tao_shared_array})::Csize_t
end

function tao_shared_array_get_type(arr)
    @ccall libtao.tao_shared_array_get_type(arr::Ptr{tao_shared_array})::tao_object_type
end

function tao_shared_array_get_flags(obj)
    @ccall libtao.tao_shared_array_get_flags(obj::Ptr{tao_shared_array})::UInt32
end

function tao_shared_array_get_perms(obj)
    @ccall libtao.tao_shared_array_get_perms(obj::Ptr{tao_shared_array})::UInt32
end

function tao_shared_array_get_shmid(arr)
    @ccall libtao.tao_shared_array_get_shmid(arr::Ptr{tao_shared_array})::tao_shmid
end

function tao_shared_array_get_eltype(arr)
    @ccall libtao.tao_shared_array_get_eltype(arr::Ptr{tao_shared_array})::tao_eltype
end

function tao_shared_array_get_length(arr)
    @ccall libtao.tao_shared_array_get_length(arr::Ptr{tao_shared_array})::Clong
end

function tao_shared_array_get_ndims(arr)
    @ccall libtao.tao_shared_array_get_ndims(arr::Ptr{tao_shared_array})::Cint
end

function tao_shared_array_get_dim(arr, d)
    @ccall libtao.tao_shared_array_get_dim(arr::Ptr{tao_shared_array}, d::Cint)::Clong
end

function tao_shared_array_get_data(arr)
    @ccall libtao.tao_shared_array_get_data(arr::Ptr{tao_shared_array})::Ptr{Cvoid}
end

function tao_shared_array_fill(arr, val)
    @ccall libtao.tao_shared_array_fill(arr::Ptr{tao_shared_array}, val::Cdouble)::Ptr{tao_shared_array}
end

function tao_shared_array_get_serial(arr)
    @ccall libtao.tao_shared_array_get_serial(arr::Ptr{tao_shared_array})::tao_serial
end

function tao_shared_array_set_serial(arr, cnt)
    @ccall libtao.tao_shared_array_set_serial(arr::Ptr{tao_shared_array}, cnt::tao_serial)::Cvoid
end

function tao_shared_array_get_timestamp(arr, idx, ts)
    @ccall libtao.tao_shared_array_get_timestamp(arr::Ptr{tao_shared_array}, idx::Cint, ts::Ptr{tao_time})::Cvoid
end

function tao_shared_array_set_timestamp(arr, idx, ts)
    @ccall libtao.tao_shared_array_set_timestamp(arr::Ptr{tao_shared_array}, idx::Cint, ts::Ptr{tao_time})::Cvoid
end

function tao_shared_array_rdlock(arr)
    @ccall libtao.tao_shared_array_rdlock(arr::Ptr{tao_shared_array})::tao_status
end

function tao_shared_array_wrlock(arr)
    @ccall libtao.tao_shared_array_wrlock(arr::Ptr{tao_shared_array})::tao_status
end

function tao_shared_array_unlock(arr)
    @ccall libtao.tao_shared_array_unlock(arr::Ptr{tao_shared_array})::tao_status
end

function tao_shared_array_try_rdlock(arr)
    @ccall libtao.tao_shared_array_try_rdlock(arr::Ptr{tao_shared_array})::tao_status
end

function tao_shared_array_try_wrlock(arr)
    @ccall libtao.tao_shared_array_try_wrlock(arr::Ptr{tao_shared_array})::tao_status
end

function tao_shared_array_timed_rdlock(arr, secs)
    @ccall libtao.tao_shared_array_timed_rdlock(arr::Ptr{tao_shared_array}, secs::Cdouble)::tao_status
end

function tao_shared_array_timed_wrlock(arr, secs)
    @ccall libtao.tao_shared_array_timed_wrlock(arr::Ptr{tao_shared_array}, secs::Cdouble)::tao_status
end

function tao_shared_array_abstimed_rdlock(arr, abstime)
    @ccall libtao.tao_shared_array_abstimed_rdlock(arr::Ptr{tao_shared_array}, abstime::Ptr{tao_time})::tao_status
end

function tao_shared_array_abstimed_wrlock(arr, abstime)
    @ccall libtao.tao_shared_array_abstimed_wrlock(arr::Ptr{tao_shared_array}, abstime::Ptr{tao_time})::tao_status
end

# typedef tao_status tao_shared_array_callback ( tao_shared_array * obj , void * data )
const tao_shared_array_callback = Cvoid

function tao_shared_array_rdlock_do(obj, task, task_data, secs)
    @ccall libtao.tao_shared_array_rdlock_do(obj::Ptr{tao_shared_array}, task::Ptr{tao_shared_array_callback}, task_data::Ptr{Cvoid}, secs::Cdouble)::tao_status
end

function tao_shared_array_wrlock_do(obj, task, task_data, secs)
    @ccall libtao.tao_shared_array_wrlock_do(obj::Ptr{tao_shared_array}, task::Ptr{tao_shared_array_callback}, task_data::Ptr{Cvoid}, secs::Cdouble)::tao_status
end

function tao_shared_array_unlock_do(obj, task, task_data, rw, secs)
    @ccall libtao.tao_shared_array_unlock_do(obj::Ptr{tao_shared_array}, task::Ptr{tao_shared_array_callback}, task_data::Ptr{Cvoid}, rw::Bool, secs::Cdouble)::tao_status
end

function tao_copy_to_shared_array(dst, dstoffs, srcdata, srctype, srcdims, srcoffs, lens, ndims)
    @ccall libtao.tao_copy_to_shared_array(dst::Ptr{tao_shared_array}, dstoffs::Ptr{Clong}, srcdata::Ptr{Cvoid}, srctype::tao_eltype, srcdims::Ptr{Clong}, srcoffs::Ptr{Clong}, lens::Ptr{Clong}, ndims::Cint)::tao_status
end

function tao_copy_from_shared_array(dstdata, dsttype, dstdims, dstoffs, src, srcoffs, lens, ndims)
    @ccall libtao.tao_copy_from_shared_array(dstdata::Ptr{Cvoid}, dsttype::tao_eltype, dstdims::Ptr{Clong}, dstoffs::Ptr{Clong}, src::Ptr{tao_shared_array}, srcoffs::Ptr{Clong}, lens::Ptr{Clong}, ndims::Cint)::tao_status
end

function tao_copy_array_to_shared_array(dst, dstoffs, src, srcoffs, lens, ndims)
    @ccall libtao.tao_copy_array_to_shared_array(dst::Ptr{tao_shared_array}, dstoffs::Ptr{Clong}, src::Ptr{tao_array}, srcoffs::Ptr{Clong}, lens::Ptr{Clong}, ndims::Cint)::tao_status
end

function tao_copy_shared_array_to_array(dst, dstoffs, src, srcoffs, lens, ndims)
    @ccall libtao.tao_copy_shared_array_to_array(dst::Ptr{tao_array}, dstoffs::Ptr{Clong}, src::Ptr{tao_shared_array}, srcoffs::Ptr{Clong}, lens::Ptr{Clong}, ndims::Cint)::tao_status
end

function tao_copy_shared_array_to_shared_array(dst, dstoffs, src, srcoffs, lens, ndims)
    @ccall libtao.tao_copy_shared_array_to_shared_array(dst::Ptr{tao_shared_array}, dstoffs::Ptr{Clong}, src::Ptr{tao_shared_array}, srcoffs::Ptr{Clong}, lens::Ptr{Clong}, ndims::Cint)::tao_status
end

function tao_shared_memory_create(shmid_ptr, size, flags)
    @ccall libtao.tao_shared_memory_create(shmid_ptr::Ptr{tao_shmid}, size::Csize_t, flags::Cuint)::Ptr{Cvoid}
end

function tao_shared_memory_attach(shmid, sizeptr)
    @ccall libtao.tao_shared_memory_attach(shmid::tao_shmid, sizeptr::Ptr{Csize_t})::Ptr{Cvoid}
end

function tao_shared_memory_detach(addr)
    @ccall libtao.tao_shared_memory_detach(addr::Ptr{Cvoid})::tao_status
end

function tao_shared_memory_destroy(shmid)
    @ccall libtao.tao_shared_memory_destroy(shmid::tao_shmid)::tao_status
end

function tao_shared_memory_stat(shmid, segsz, nattch)
    @ccall libtao.tao_shared_memory_stat(shmid::tao_shmid, segsz::Ptr{Csize_t}, nattch::Ptr{Int64})::tao_status
end

abstract type tao_shared_object end

function tao_shared_object_create(type, size, flags)
    @ccall libtao.tao_shared_object_create(type::UInt32, size::Csize_t, flags::Cuint)::Ptr{tao_shared_object}
end

function tao_shared_object_attach(shmid)
    @ccall libtao.tao_shared_object_attach(shmid::tao_shmid)::Ptr{tao_shared_object}
end

function tao_shared_object_detach(obj)
    @ccall libtao.tao_shared_object_detach(obj::Ptr{tao_shared_object})::tao_status
end

function tao_shared_object_get_size(obj)
    @ccall libtao.tao_shared_object_get_size(obj::Ptr{tao_shared_object})::Csize_t
end

function tao_shared_object_get_type(obj)
    @ccall libtao.tao_shared_object_get_type(obj::Ptr{tao_shared_object})::tao_object_type
end

function tao_shared_object_get_flags(obj)
    @ccall libtao.tao_shared_object_get_flags(obj::Ptr{tao_shared_object})::UInt32
end

function tao_shared_object_get_perms(obj)
    @ccall libtao.tao_shared_object_get_perms(obj::Ptr{tao_shared_object})::UInt32
end

function tao_shared_object_get_shmid(obj)
    @ccall libtao.tao_shared_object_get_shmid(obj::Ptr{tao_shared_object})::tao_shmid
end

function tao_shared_object_lock(obj)
    @ccall libtao.tao_shared_object_lock(obj::Ptr{tao_shared_object})::tao_status
end

function tao_shared_object_unlock(obj)
    @ccall libtao.tao_shared_object_unlock(obj::Ptr{tao_shared_object})::tao_status
end

function tao_shared_object_try_lock(obj)
    @ccall libtao.tao_shared_object_try_lock(obj::Ptr{tao_shared_object})::tao_status
end

function tao_shared_object_abstimed_lock(obj, lim)
    @ccall libtao.tao_shared_object_abstimed_lock(obj::Ptr{tao_shared_object}, lim::Ptr{tao_time})::tao_status
end

function tao_shared_object_timed_lock(obj, secs)
    @ccall libtao.tao_shared_object_timed_lock(obj::Ptr{tao_shared_object}, secs::Cdouble)::tao_status
end

# typedef tao_status tao_shared_object_callback ( tao_shared_object * obj , void * data )
const tao_shared_object_callback = Cvoid

function tao_shared_object_lock_do(obj, task, task_data, secs)
    @ccall libtao.tao_shared_object_lock_do(obj::Ptr{tao_shared_object}, task::Ptr{tao_shared_object_callback}, task_data::Ptr{Cvoid}, secs::Cdouble)::tao_status
end

function tao_shared_object_unlock_do(obj, task, task_data, secs)
    @ccall libtao.tao_shared_object_unlock_do(obj::Ptr{tao_shared_object}, task::Ptr{tao_shared_object_callback}, task_data::Ptr{Cvoid}, secs::Cdouble)::tao_status
end

function tao_shared_object_signal_condition(obj)
    @ccall libtao.tao_shared_object_signal_condition(obj::Ptr{tao_shared_object})::tao_status
end

function tao_shared_object_broadcast_condition(obj)
    @ccall libtao.tao_shared_object_broadcast_condition(obj::Ptr{tao_shared_object})::tao_status
end

function tao_shared_object_wait_condition(obj)
    @ccall libtao.tao_shared_object_wait_condition(obj::Ptr{tao_shared_object})::tao_status
end

function tao_shared_object_abstimed_wait_condition(obj, lim)
    @ccall libtao.tao_shared_object_abstimed_wait_condition(obj::Ptr{tao_shared_object}, lim::Ptr{tao_time})::tao_status
end

function tao_shared_object_timed_wait_condition(obj, secs)
    @ccall libtao.tao_shared_object_timed_wait_condition(obj::Ptr{tao_shared_object}, secs::Cdouble)::tao_status
end

function tao_shared_object_wait(obj, pred, pred_data, secs)
    @ccall libtao.tao_shared_object_wait(obj::Ptr{tao_shared_object}, pred::Ptr{tao_shared_object_callback}, pred_data::Ptr{Cvoid}, secs::Cdouble)::tao_status
end

function tao_shared_object_lock_wait_do(obj, pred, pred_data, task, task_data, secs)
    @ccall libtao.tao_shared_object_lock_wait_do(obj::Ptr{tao_shared_object}, pred::Ptr{tao_shared_object_callback}, pred_data::Ptr{Cvoid}, task::Ptr{tao_shared_object_callback}, task_data::Ptr{Cvoid}, secs::Cdouble)::tao_status
end

@enum tao_timeout::Int32 begin
    TAO_TIMEOUT_ERROR = -2
    TAO_TIMEOUT_PAST = -1
    TAO_TIMEOUT_NOW = 0
    TAO_TIMEOUT_FUTURE = 1
    TAO_TIMEOUT_NEVER = 2
end

function tao_sleep(secs)
    @ccall libtao.tao_sleep(secs::Cdouble)::tao_status
end

function tao_copy_time(dst, src)
    @ccall libtao.tao_copy_time(dst::Ptr{tao_time}, src::Ptr{tao_time})::Ptr{tao_time}
end

function tao_get_monotonic_time(dest)
    @ccall libtao.tao_get_monotonic_time(dest::Ptr{tao_time})::tao_status
end

function tao_get_current_time(dest)
    @ccall libtao.tao_get_current_time(dest::Ptr{tao_time})::tao_status
end

function tao_time_normalize(ts)
    @ccall libtao.tao_time_normalize(ts::Ptr{tao_time})::Ptr{tao_time}
end

function tao_time_add(dest, a, b)
    @ccall libtao.tao_time_add(dest::Ptr{tao_time}, a::Ptr{tao_time}, b::Ptr{tao_time})::Ptr{tao_time}
end

function tao_time_subtract(dest, a, b)
    @ccall libtao.tao_time_subtract(dest::Ptr{tao_time}, a::Ptr{tao_time}, b::Ptr{tao_time})::Ptr{tao_time}
end

function tao_time_to_seconds(t)
    @ccall libtao.tao_time_to_seconds(t::Ptr{tao_time})::Cdouble
end

function tao_time_to_milliseconds(t)
    @ccall libtao.tao_time_to_milliseconds(t::Ptr{tao_time})::Cdouble
end

function tao_time_to_microseconds(t)
    @ccall libtao.tao_time_to_microseconds(t::Ptr{tao_time})::Cdouble
end

function tao_time_to_nanoseconds(t)
    @ccall libtao.tao_time_to_nanoseconds(t::Ptr{tao_time})::Cdouble
end

function tao_elapsed_seconds(t, t0)
    @ccall libtao.tao_elapsed_seconds(t::Ptr{tao_time}, t0::Ptr{tao_time})::Cdouble
end

function tao_elapsed_milliseconds(t, t0)
    @ccall libtao.tao_elapsed_milliseconds(t::Ptr{tao_time}, t0::Ptr{tao_time})::Cdouble
end

function tao_elapsed_microseconds(t, t0)
    @ccall libtao.tao_elapsed_microseconds(t::Ptr{tao_time}, t0::Ptr{tao_time})::Cdouble
end

function tao_elapsed_nanoseconds(t, t0)
    @ccall libtao.tao_elapsed_nanoseconds(t::Ptr{tao_time}, t0::Ptr{tao_time})::Cdouble
end

function tao_seconds_to_time(dest, secs)
    @ccall libtao.tao_seconds_to_time(dest::Ptr{tao_time}, secs::Cdouble)::Ptr{tao_time}
end

function tao_timespec_to_time(dst, src)
    @ccall libtao.tao_timespec_to_time(dst::Ptr{tao_time}, src::Ptr{Cvoid})::Ptr{tao_time}
end

@enum tao_time_format::UInt32 begin
    TAO_TIME_FORMAT_FRACTIONAL_SECONDS = 0
    TAO_TIME_FORMAT_DATE_WITH_SECONDS = 1
    TAO_TIME_FORMAT_DATE_WITH_MILLISECONDS = 2
    TAO_TIME_FORMAT_DATE_WITH_MICROSECONDS = 3
    TAO_TIME_FORMAT_DATE_WITH_NANOSECONDS = 4
end

function tao_time_sprintf(str, fmt, ts)
    @ccall libtao.tao_time_sprintf(str::Cstring, fmt::tao_time_format, ts::Ptr{tao_time})::Cstring
end

function tao_time_snprintf(str, size, fmt, ts)
    @ccall libtao.tao_time_snprintf(str::Cstring, size::Clong, fmt::tao_time_format, ts::Ptr{tao_time})::Clong
end

function tao_adjust_time(time, secs)
    @ccall libtao.tao_adjust_time(time::Ptr{tao_time}, secs::Cdouble)::tao_status
end

function tao_get_timeout(abstime, secs)
    @ccall libtao.tao_get_timeout(abstime::Ptr{tao_time}, secs::Cdouble)::tao_timeout
end

function tao_get_maximum_absolute_time()
    @ccall libtao.tao_get_maximum_absolute_time()::Cdouble
end

struct tao_time_stat_data
    min::Cdouble
    max::Cdouble
    sum1::Cdouble
    sum2::Cdouble
    numb::Csize_t
end

struct tao_time_stat
    min::Cdouble
    max::Cdouble
    avg::Cdouble
    std::Cdouble
    numb::Csize_t
end

function tao_initialize_time_statistics(tsd)
    @ccall libtao.tao_initialize_time_statistics(tsd::Ptr{tao_time_stat_data})::Cvoid
end

function tao_update_time_statistics(tsd, t)
    @ccall libtao.tao_update_time_statistics(tsd::Ptr{tao_time_stat_data}, t::Cdouble)::Cvoid
end

function tao_compute_time_statistics(ts, tsd)
    @ccall libtao.tao_compute_time_statistics(ts::Ptr{tao_time_stat}, tsd::Ptr{tao_time_stat_data})::Ptr{tao_time_stat}
end

const TAO_INT8_MIN = typemin(Int8)

const TAO_INT8_MAX = typemax(Int8)

const TAO_UINT8_MAX = typemax(UInt8)

const TAO_INT16_MIN = typemin(Int16)

const TAO_INT16_MAX = typemax(Int16)

const TAO_UINT16_MAX = typemax(UInt16)

const TAO_INT32_MAX = typemax(Int32)

const TAO_INT32_MIN = typemin(Int32)

const TAO_UINT32_MAX = typemax(UInt32)

const TAO_INT64_MAX = typemax(Int64)

const TAO_INT64_MIN = typemin(Int64)

const TAO_UINT64_MAX = typemax(UInt64)

const TAO_CHAR_BITS = 8

const TAO_SHORT_BITS = 16

const TAO_INT_BITS = 32

const TAO_LONG_BITS = 64

const TAO_LLONG_BITS = 64

const TAO_ALIGNMENT = 64

const TAO_MAX_NDIMS = 5

const TAO_ATTR_READABLE = 0x01 << 0

const TAO_ATTR_WRITABLE = 0x01 << 1

const TAO_ATTR_VARIABLE = 0x01 << 2

const TAO_ATTR_CHECK_NAME = Cuint(1) << 0

const TAO_ATTR_CHECK_FLAGS = Cuint(1) << 1

const TAO_ATTR_CHECK_TYPE = Cuint(1) << 2

const TAO_ATTR_CHECK_VALUE = Cuint(1) << 3

const TAO_VERSION_MAJOR = 0

const TAO_VERSION_MINOR = 11

const TAO_VERSION_PATCH = 0

const TAO_CONFIG_DIR = "/tmp/tao"

const TAO_BIG_ENDIAN_BOM = Cuint(0x01020304)

const TAO_LITTLE_ENDIAN_BOM = Cuint(0x04030201)

const TAO_IS_BIG_ENDIAN = TAO_NATIVE_ENDIAN_BOM == TAO_BIG_ENDIAN_BOM

const TAO_IS_LITTLE_ENDIAN = TAO_NATIVE_ENDIAN_BOM == TAO_LITTLE_ENDIAN_BOM

const TAO_ENCODING_FLAGS_MSB_PAD = tao_encoding(0) << 0

const TAO_ENCODING_FLAGS_LSB_PAD = tao_encoding(1) << 0

const TAO_ENCODING_FLAGS_CODED = tao_encoding(1) << 1

const TAO_ENCODING_FLAGS_PARALLEL = tao_encoding(1) << 2

const TAO_ENCODING_MASK = tao_encoding(255)

const TAO_ENCODING_UNKNOWN = tao_encoding(0)

const TAO_COLORANT_RAW = 1

const TAO_COLORANT_MONO = 2

const TAO_COLORANT_RGB = 3

const TAO_COLORANT_BGR = 4

const TAO_COLORANT_ARGB = 5

const TAO_COLORANT_RGBA = 6

const TAO_COLORANT_ABGR = 7

const TAO_COLORANT_BGRA = 8

const TAO_COLORANT_BAYER_RGGB = 9

const TAO_COLORANT_BAYER_GRBG = 10

const TAO_COLORANT_BAYER_GBRG = 11

const TAO_COLORANT_BAYER_BGGR = 12

const TAO_COLORANT_YUV444 = 13

const TAO_COLORANT_YUV422 = 14

const TAO_COLORANT_YUV411 = 15

const TAO_COLORANT_YUV420P = 16

const TAO_COLORANT_YUV420SP = 17

const TAO_COLORANT_SIGNED = 18

const TAO_COLORANT_FLOAT = 19

const TAO_COLORANT_UNSIGNED = TAO_COLORANT_MONO

const TAO_ENCODING_YUV444 = TAO_ENCODING(TAO_COLORANT_YUV444, 24)

const TAO_ENCODING_YUV422 = TAO_ENCODING(TAO_COLORANT_YUV422, 16, 32)

const TAO_ENCODING_YUV411 = TAO_ENCODING(TAO_COLORANT_YUV411, 12, 48)

const TAO_ENCODING_YUV420P = TAO_ENCODING(TAO_COLORANT_YUV420P, 12, 48)

const TAO_ENCODING_YUV420SP = TAO_ENCODING(TAO_COLORANT_YUV420SP, 12, 48)

const TAO_ENCODING_ANDOR_MONO8 = TAO_ENCODING_MONO(8)

const TAO_ENCODING_ANDOR_MONO12 = TAO_ENCODING(TAO_COLORANT_MONO, 12, 16, TAO_ENCODING_FLAGS_MSB_PAD)

const TAO_ENCODING_ANDOR_MONO12PACKED = TAO_ENCODING_MONO_PKT(12, 24)

const TAO_ENCODING_ANDOR_MONO12CODED = TAO_ENCODING(TAO_COLORANT_MONO, 12, 16, TAO_ENCODING_FLAGS_CODED | TAO_ENCODING_FLAGS_MSB_PAD)

const TAO_ENCODING_ANDOR_MONO12CODEDPACKED = TAO_ENCODING(TAO_COLORANT_MONO, 12, 24, TAO_ENCODING_FLAGS_CODED)

const TAO_ENCODING_ANDOR_MONO16 = TAO_ENCODING_MONO(16)

const TAO_ENCODING_ANDOR_MONO32 = TAO_ENCODING_MONO(32)

const TAO_ENCODING_ANDOR_RGB8PACKED = TAO_ENCODING_RGB(24)

const TAO_ENCODING_ANDOR_MONO22PARALLEL = TAO_ENCODING(TAO_COLORANT_MONO, 22, 24, TAO_ENCODING_FLAGS_PARALLEL | TAO_ENCODING_FLAGS_MSB_PAD)

const TAO_ENCODING_ANDOR_MONO22PACKEDPARALLEL = TAO_ENCODING(TAO_COLORANT_MONO, 22, 88, TAO_ENCODING_FLAGS_PARALLEL)

const TAO_ENCODING_STRING_SIZE = 32

const TAO_CAMERA_CONFIG_ATTRIBUTES_SIZE = 8 * 1024

const TAO_EVENT_COMMAND = tao_event(1) << 0

const TAO_EVENT_FRAME = tao_event(1) << 1

const TAO_EVENT_ERROR = tao_event(1) << 2

const TAO_OWNER_SIZE = 64

const TAO_SHARED_ARRAY_TIMESTAMPS = 5

const TAO_BAD_SHMID = tao_shmid(-1)

const TAO_SHARED_MAGIC = 0x7fcdc600

const TAO_SHARED_MASK = 0xffffff00

const TAO_SHARED_SUPERTYPE_MASK = 0xffffffe0

const TAO_PERSISTENT = Cuint(1) << 20

const TAO_UNKNOWN_TIME = tao_time(0, 0)

const TAO_MAX_TIME_SECONDS = typemax(tao_time_member)

const TAO_MIN_TIME_SECONDS = typemin(tao_time_member)

const TAO_NANOSECONDS_PER_SECOND = 1000000000

const TAO_MICROSECONDS_PER_SECOND = 1000000

const TAO_MILLISECONDS_PER_SECOND = 1000

const TAO_SECOND = 1.0

const TAO_NANOSECOND = 1.0e-9TAO_SECOND

const TAO_MICROSECOND = 1.0e-6TAO_SECOND

const TAO_MILLISECOND = 0.001TAO_SECOND

const TAO_MINUTE = 60TAO_SECOND

const TAO_HOUR = 60TAO_MINUTE

const TAO_DAY = 24TAO_HOUR

const TAO_YEAR = 365.25TAO_DAY

const TAO_TIMEOUT_MIN = 1.0e-6

const TAO_TIMEOUT_MAX = Float64(TAO_MAX_TIME_SECONDS)

# Check API version assumed by the wrappers of this file.
function __init__()
    major = Ref{Cint}()
    minor = Ref{Cint}()
    patch = Ref{Cint}()
    tao_version_get(major, minor, patch)
    major = major[]
    minor = minor[]
    patch = patch[]
    version = VersionNumber(major,minor,patch)
    if version != libtao_version
        error("Version $version of TAO library \"$libtao_library\" is not equal to `libtao_version = $libtao_version` in \"$(abspath(joinpath(@__DIR__,..,deps,deps.jl)))\".")
    end
    if major != TAO_VERSION_MAJOR || minor < TAO_VERSION_MINOR || (minor == TAO_VERSION_MINOR && patch < TAO_VERSION_PATCH)
        error("Wrapper code in `$(@__FILE__)` assumes version $TAO_VERSION_MAJOR.$TAO_VERSION_MINOR.$TAO_VERSION_PATCH while TAO library version is $major.$minor.$patch")
    end
    return nothing
end

# exports
const PREFIXES = ["TAO_", "tao_"]
for name in names(@__MODULE__; all=true), prefix in PREFIXES
    if startswith(string(name), prefix)
        @eval export $name
    end
end

end # module
