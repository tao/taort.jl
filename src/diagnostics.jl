#
# diagnostics.jl --
#
# Diagnostic and testing routines.
#
# This file is part of TAO software (https://git-cral.univ-lyon1.fr/tao) licensed under
# the MIT license.
#
# Copyright (c) 2021-2024, Éric Thiébaut.
#

"""
    TaoRT.measure_latency(cam; number=1_000, timeout=5.0) -> dat

yields an array of time latencies when receiving images from remote camera
`cam`.  Argument `cam` can be an instance of `RemoteCamera` or the access point
name of an XPA camera server.  Keywords `number` specifies the number of frames
to acquire.  keyword `timeout` specifies a time limit for waiting each frame.

The returned value is a vector of `number` elements which are 3-tuples
`(cnt,t1,t2,t3,t4,t5)` such that `cnt` is the counter value, `t1` to `t5` are
the "Frame start", "Frame end", "Buffer ready", "Image sent", and "Image
received" times.  All times in seconds.

See also [`TaoRT.analyze_latency`](@ref).

"""
function measure_latency(cam::RemoteCamera;
                         number::Integer = 1_000,
                         timeout::Real = 5.0)
    out = Array{Tuple{Int64,Float64,Float64,Float64,Float64,Float64},1}(undef, number)
    t0 = monotonic_time()
    for i in 1:number
        arr = fetch(cam, :next)
        t5, t1, t2, t3, t4, cnt = @rdlock arr timeout=timeout (
            monotonic_time(), arr.timestamp1, arr.timestamp2, arr.timestamp3,
            arr.timestamp4, arr.counter)
        out[i] = (cnt, float(t1 - t0), float(t2 - t0), float(t3 - t0),
                  float(t4 - t0), float(t5 - t0))
    end
    return out
end

"""
    TaoRT.analyze_latency([io::IO=stdout,] dat)

prints analysis of measurements `dat` returned by
[`TaoRT.measure_latency`](@ref).  Optional argument `io` is the output
stream.

"""
analyze_latency(dat::AbstractVector{<:Tuple{Integer,Real,Real,Real,Real,Real}}) =
    analyze_latency(stdout, dat)

function analyze_latency(io::IO,
                         dat::AbstractVector{<:Tuple{Integer,Real,Real,Real,Real,Real}})
    # Collect data, stripping invalid samples.
    cnts = Int64[]
    times = [Float64[] for i in 1:5]
    names = ("Frame start", "Frame end", "Buffer ready",
             "Image sent", "Image received")
    for i in eachindex(dat)
        cnt, t1, t2, t3, t4, t5 = dat[i]
        if cnt > 0
            push!(cnts, cnt)
            push!(times[1], t1)
            push!(times[2], t2)
            push!(times[3], t3)
            push!(times[4], t4)
            push!(times[5], t5)
        end
    end
    nsamples = length(cnts)
    nsamples ≥ 2 || error("insufficient number of samples")
    nframes = last(cnts) - first(cnts) + 1

    # Figure out what are the first valid time-stamps (the first ones showing an
    # increase in time).
    org = 0
    while true
        org += 1
        if org > 5 || last(times[org]) > first(times[org])
            break
        end
    end
    org ≤ 5 || error("time-stamps never increase")

    # Measure the mean and standard deviation of the send times between samples.
    # Note the factor 1/2 in the variance which assumes that the send times are
    # independent random variables.
    Δt = [
        (times[org][i+1] - times[org][i])/(cnts[i+1] - cnts[i])
        for i in 1:nsamples-1
    ]
    send_rate_avg = mean(Δt)
    send_rate_std = std(Δt)/sqrt(2)

    # Lost samples.
    losses = filter(x -> x > 1, diff(cnts)) .- 1
    lost_syncs = length(losses)
    if lost_syncs > 0
        lost_frames = sum(losses)
    else
        lost_frames = 0
    end

    # Report analysis.
    tree = Any[
        @sprintf("Latency measurements on %d samples at %.3f Hz:",
                 nsamples, 1/send_rate_avg),
        @sprintf("'%s' interval: %.3f +/- %.3f µs",
                 names[org], send_rate_avg*1E6, send_rate_std*1E6),
        @sprintf("Lost syncs: %d / %d samples", lost_syncs, nsamples),
        @sprintf("Lost frames: %d / %d frames", lost_frames, nframes),]
    if lost_syncs > 0
        x, y = histogram(losses)
        push!(tree, [@sprintf("%d frame(s) lost (%d times)", x[i], y[i])
                     for i in 1:length(y) if y[i] > 0])
    end
    prv = org
    push!(tree, "Delays:", String[])
    for nxt in prv+1:5
        if last(times[nxt]) > first(times[nxt])
            delay = times[nxt] - times[prv]
            push!(tree[end], @sprintf("'%s' - '%s': %.3f +/- %.3f µs",
                                      names[nxt], names[prv],
                                      mean(delay)*1E6, std(delay)*1E6/sqrt(2)))
            prv = nxt
        end
    end
    print_tree(io, tree)
    nothing
end

function print_tree(io::IO, args...; prefix::AbstractString = "")
    for arg in args
        print_tree_worker(io, String(prefix), arg, 0)
    end
    nothing
end
const TREE_PREFIXES = [" ├─ ", " │  ", " └─ ", "    "];

function print_tree_worker(io::IO, pfx::String, arg, stage::Integer)
    isindexable(::AbstractVector) = true
    isindexable(::Tuple) = true
    isindexable(::Any) = false
    if isindexable(arg)
        n = length(arg)
        if n ≥ 1
            if stage == 1
                pfx *= TREE_PREFIXES[2]
            elseif stage == 2
                pfx *= TREE_PREFIXES[4]
            end
            ilast = 0
            for (i, x) in enumerate(arg)
                if !isindexable(x)
                    ilast = i
                end
            end
            for (i, x) in enumerate(arg)
                print_tree_worker(io, pfx, x, (i < ilast ? 1 : 2))
            end
        end
    else
        if stage == 1
            pfx *= TREE_PREFIXES[1]
        elseif stage == 2
            pfx *= TREE_PREFIXES[3]
        end
        println(io, pfx, arg)
    end
end

function histogram(data::AbstractArray{T};
                   cmin::Integer = minimum(data),
                   cmax::Integer = maximum(data)) where {T<:Integer}
    if cmax < cmin
        counts = Int[]
    else
        counts = zeros(Int, Int(cmax) - Int(cmin) + 1)
        offset = 1 - cmin
        for val in data
            if cmin ≤ val ≤ cmax
                counts[val + offset] += 1
            end
        end
    end
    return cmin:cmax, counts
end
