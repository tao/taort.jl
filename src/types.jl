#
# types.jl -
#
# Type definitions for the Julia interface to the C libraries of TAO, a Toolkit for
# Adaptive Optics.
#
# This file is part of TAO software (https://git-cral.univ-lyon1.fr/tao) licensed under
# the MIT license.
#
# Copyright (c) 2018-2024, Éric Thiébaut.
#

"""
    Serial

is the type of serial numbers in TAO real-time library.

"""
const Serial = Lib.tao_serial

"""
    Command

is the type of possible commands in TAO real-time library.

"""
const Command = Lib.tao_command

"""
    State

is the type of possible server states in TAO real-time library.

"""
const State = Lib.tao_state

"""
    Preprocessing

is the type of possible pre-processings in TAO real-time library.

"""
const Preprocessing = Lib.tao_preprocessing

"""
    HighResolutionTime(s, ns)

yields a high resolution time (as used in TAO timed functions) with `s` seconds and `ns`
nanoseconds.

"""
const HighResolutionTime = Lib.tao_time

struct PixelEncoding
    bits::Lib.tao_encoding
end

"""
    TaoError <: Exception

is the type of errors other than time-out or overwriting that can be returned
by TAO C Library functions.

"""
const TaoError = Lib.tao_error

"""
    TaoRT.OverwriteError()

yields an exception indicating that the requested data has been overwritten.

"""
struct OverwriteError <: Exception; end

"""
    TaoRT.NullPointer()

yields an exception indicating a null pointer handle. This is generally the sign of an
attempt to use a detached shared object or of an uncatched error.

"""
struct NullPointer <: Exception; end

# Shared memory identifier.
struct ShmId
    val::Lib.tao_shmid
end

"""
    BAD_SHMID

is the constant used to indicate an invalid shared memory identifier.

"""
const BAD_SHMID = ShmId(Lib.TAO_BAD_SHMID)

"""
    TaoRT.LockMode(val)


is used to denote the lock mode of a TAO object. The different possibilities are:

* `TaoRT.UNLOCKED`: the object is not locked by the caller;

* `TaoRT.READ_ONLY`: the object is locked by the caller for read-only access;

* `TaoRT.READ_WRITE`: the object is locked by the caller for exclusive (read-write)
  access.

"""
@enum LockMode::Cint begin
    UNLOCKED   = 0
    READ_ONLY  = 1
    READ_WRITE = 2
end

"""
    TaoRT.AbstractSharedObject

is the super-type of all TAO objects stored in shared memory except shared arrays (which
inherit from `DenseArray`) and remote cameras (which inherit from `AbstractCamera`). The
union [`TaoRT.Shared`](@ref) contains all shared TAO object types.

"""
abstract type AbstractSharedObject end

"""
    TaoRT.AbstractRemoteObject

is the super-type of most remote TAO objects. The union [`TaoRT.Remote`](@ref) contains
all remote TAO object types.

"""
abstract type AbstractRemoteObject <: AbstractSharedObject end

"""
    TaoRT.AbstractRWLockedObject

is the super-type of most TAO objects implementing read/write locking. The union
[`TaoRT.RWLockable`](@ref) contains all r/w locked TAO object types.

"""
abstract type AbstractRWLockedObject <: AbstractSharedObject end

mutable struct SharedObject <: AbstractSharedObject
    ptr::Ptr{Lib.tao_shared_object}
    lock::LockMode
end

mutable struct RemoteObject <: AbstractRemoteObject
    ptr::Ptr{Lib.tao_remote_object}
    lock::LockMode
end

mutable struct RWLockedObject <: AbstractRWLockedObject
    ptr::Ptr{Lib.tao_rwlocked_object}
    lock::LockMode
end

mutable struct SharedArray{T,N} <: DenseArray{T,N}
    ptr::Ptr{Lib.tao_shared_array}
    arr::Array{T,N}
    lock::LockMode
end

mutable struct RemoteCamera{T,N} <: AbstractCamera{T,N}
    ptr::Ptr{Lib.tao_remote_camera}
    lock::LockMode
    arrays::Vector{SharedArray{T,N}}
    shmids::Vector{ShmId}
end

mutable struct CameraConfig
    # This structure is mutable so that it can safely hold a reference to its contents
    # even though it is an immutable structure.
    data::Lib.tao_camera_config # NOTE: must be the first (and only?) field
    CameraConfig() = new()
    CameraConfig(data::Lib.tao_camera_config) = new(data)
end

mutable struct RemoteMirror <: AbstractDeformableMirror{Cdouble}
    ptr::Ptr{Lib.tao_remote_mirror}
    buf::Vector{Cdouble} # buffer of `nacts` entries for conversion
    nacts::Int
    nbufs::Int
    inds::Matrix{Int}
    lock::LockMode
end

#=
mutable struct SensorControl
    # This structure is mutable so that it can safely hold a reference to its
    # contents even though it is an immutable structure.
    data::Lib.tao_sensor_control # NOTE: must be the first (and only?) field
    SensorControl() = new()
    SensorControl(data::Lib.tao_sensor_control) = new(data)
end
=#

mutable struct RemoteSensor <: AbstractRemoteObject
    ptr::Ptr{Lib.tao_remote_sensor}
    #ctrl::Lib.tao_sensor_control # buffer of real-time parameters
    lock::LockMode
    function RemoteSensor()
        obj = new()
        setfield!(obj, :ptr, Ptr{Lib.tao_remote_sensor}(0))
        setfield!(obj, :lock, UNLOCKED)
        return obj
    end
end

"""
    TaoRT.REMOTE_OBJECT_TYPES

is a tuple of the concrete remote TAO object types.

"""
const REMOTE_OBJECT_TYPES = (RemoteObject,
                             RemoteCamera,
                             RemoteMirror,
                             RemoteSensor)

"""
    TaoRT.RWLOCKED_OBJECT_TYPES

is a tuple of the concrete TAO object types implementing read/write locking.

"""
const RWLOCKED_OBJECT_TYPES = (RWLockedObject,
                               SharedArray)

"""
    TaoRT.RWLOCKED_OBJECT_TYPES

is a tuple of all concrete shared TAO object types.

"""
const SHARED_OBJECT_TYPES = (SharedObject,
                             REMOTE_OBJECT_TYPES...,
                             RWLOCKED_OBJECT_TYPES...)

"""
    TaoRT.Shared

is the union of all concrete shared object types in TAO.

"""
const Shared = Union{SHARED_OBJECT_TYPES...}

"""
    TaoRT.Remote

is the union of all concrete remote object types in TAO.

"""
const Remote = Union{REMOTE_OBJECT_TYPES...}

"""
    TaoRT.Lockable

is the union of all concrete TAO object types which implement `lock` for exclusive
(read-write) access.

See also [`TaoRT.RWLockable`](@ref).

"""
const Lockable = Union{SharedObject, REMOTE_OBJECT_TYPES...}

"""
    TaoRT.RWLockable

is the union of all concrete TAO object types which implement `rdlock` and `wrlock` for
read-only and read-write accesses.

See also [`TaoRT.Lockable`](@ref).

"""
const RWLockable = Union{RWLOCKED_OBJECT_TYPES...}

"""
    TaoRT.DataFrame

is the super-type of output data-frames produced by remote servers (deformable mirrors,
wavefront sensors, and adaptive optics loop controllers).

---
    TaoRT.DataFrame(obj) -> dat

yields an un-initialized data-frame suitable for remote object `obj`.

"""
abstract type DataFrame end

const DataFrameInfo = Lib.tao_dataframe_info

mutable struct MirrorDataFrame <: DataFrame
    info::DataFrameInfo   # serial number, mark, timestamp, ...
    refcmds::Vector{Cdouble} # vector of reference commands
    perturb::Vector{Cdouble} # vector of perturbation commands
    reqcmds::Vector{Cdouble} # vector of requested commands
    devcmds::Vector{Cdouble} # vector of commands effectively sent to the device
end
