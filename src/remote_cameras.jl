#
# remote_cameras.jl -
#
# Low level interface to remote cameras for the Julia interface to the
# real-time C library of TAO, a Toolkit for Adaptive Optics.
#
# This file is part of TAO software (https://git-cral.univ-lyon1.fr/tao)
# licensed under the MIT license.
#
# Copyright (c) 2018-2024, Éric Thiébaut.
#

import ..TaoRT: RemoteCamera, CameraConfig

propertynames(::RemoteCamera) = (
    :alive,
    :droppedframes,
    :exposuretime,
    :flags,
    :framerate,
    :frames,
    :height,
    :lock,
    :lostframes,
    :lostsyncs,
    :nbufs,
    :ncmds,
    :origin,
    :overflows,
    :overruns,
    :owner,
    :perms,
    :pid,
    :pixeltype,
    :preprocessing,
    :preprocshmids,
    :rawencoding,
    :sensorheight,
    :sensorwidth,
    :serial,
    :shmid,
    :size,
    :state,
    :timeouts,
    :type,
    :width,
    :xbin,
    :xoff,
    :ybin,
    :yoff)

for key in (:frames, :droppedframes, :pixeltype, :framerate, :exposuretime, :lostframes,
            :lostsyncs, :origin, :overflows, :overruns, :preprocessing, :timeouts)
    @eval getproperty(cam::RemoteCamera, ::$(Val{key})) = $(Symbol("tao_get_",key))(cam)
end

for key in (:sensorwidth, :sensorheight, :width, :height, :xbin, :ybin, :xoff, :yoff)
    @eval getproperty(cam::RemoteCamera, ::$(Val{key})) = Int($(Symbol("tao_get_",key))(cam))
end

getproperty(cam::RemoteCamera, ::Val{:rawencoding}) = PixelEncoding(tao_get_rawencoding(cam))

const PreprocShmidList{A<:RemoteCamera} =
    PseudoVector{ShmId,Base.OneTo{Int},A,:preprocshmids}

getproperty(cam::RemoteCamera, ::Val{:preprocshmids}) =
    PreprocShmidList{typeof(cam)}(Base.OneTo(4), cam)

function Base.getindex(A::PreprocShmidList, i::Int)
    i ∈ keys(A) || throw(BoundsError(A, i))
    return ShmId(tao_get_preprocessing_shmid(parent(A), i - 1))
end

# Yield number of dimensions in output images for a given pre-processing.
@inline Base.ndims(x::tao_preprocessing) = (x == TAO_PREPROCESSING_FULL ? 3 : 2)

"""
    RemoteCamera{T,N}(id) -> cam

yields a client object which can be used to communicate with the camera server
whose name or shared-memory identifier is specified by `id`. Parameters `T` and
`N` are the pixel-type and number `N` of dimensions (3 with full
pre-processing, 2 otherwise) of the images produced by the camera. If omitted,
these parameters can be inferred at execution time but to the cost of having a
type-instable result.

The returned object implements the `cam.key` syntax for the following
properties:

| Name             | Description                                             |
|:-----------------|:--------------------------------------------------------|
| `alive`          | (a) Whether the server owning the object is alive       |
| `droppedframes`  | (s) Number of frames dropped by the camera              |
| `exposuretime`   | (s) Exposure time in seconds per frame                  |
| `flags`          | (c) Bitwise flags of the shared object                  |
| `framerate`      | (s) Frames per second                                   |
| `frames`         | (s) Number of frames acquired by the camera             |
| `height`         | (s) Height of the ROI in macro pixels                   |
| `lock`           | (p) Type of lock owned by the caller                    |
| `lostframes`     | (s) Number of frames lost by the camera                 |
| `lostsyncs`      | (s) Number of synchronizations lost by the camera       |
| `nbufs`          | (c) Number of entries in cyclic list of output buffers  |
| `ncmds`          | (a) Number of commands executed by the server           |
| `origin`         | (s) Origin of time for the camera                       |
| `overflows`      | (s) Number of overflows occuring for the camera         |
| `overruns`       | (s) Number of frames lost by the camera due to overruns |
| `owner`          | (c) Name of the server owning the object                |
| `perms`          | (c) Permissions access bits of the shared object        |
| `pid`            | (c) Process identifier of owner                         |
| `pixeltype`      | (s) Pixel type of the acquired images                   |
| `preprocessing`  | (s) Level of pixel pre-processing                       |
| `preprocshmids`  | (s) Shared memory identifiers of preprocessing arrays   |
| `rawencoding`    | (s) Encoding of the pixel data sent by the device       |
| `sensorheight`   | (s) Number of rows of physical pixels of the sensor     |
| `sensorwidth`    | (s) Number of physical pixels per row of the sensor     |
| `serial`         | (a) Counter of the last acquired frame                  |
| `shmid`          | (c) Shared memory identifier of the object              |
| `size`           | (c) Number of bytes allocated for the shared object     |
| `state`          | (a) State of the remote camera                          |
| `timeouts`       | (s) Number of timeouts occuring for the camera          |
| `type`           | (c) Type identifier of the shared object                |
| `width`          | (s) Width of the ROI in macro pixels                    |
| `xbin`           | (s) Horizontal binning factor                           |
| `xoff`           | (s) Horizontal offset of the ROI                        |
| `ybin`           | (s) Vertical binning factor                             |
| `yoff`           | (s) Vertical offset of the ROI                          |

Notes:

- (a) atomic shared member, no needs to lock the object;

- (c) constant member, no needs to lock the object;

- (s) shared member, caller shall have locked the object;

- (p) private member, no needs to lock the object.

- *ROI* is the Region Of Interest of the acquired image. ROI size `width` and
  `height` are in macro-pixel units. ROI offsets `xoff`, and `yoff` are in
  physical pixel units. Binning factors `xbin`, and `ybin` are in units of
  physical pixels per macro pixel.

To make sure that the values of non-immutable fields are consistent, the camera
should be locked by the caller. For example:

    secs = 30.0 # timeout in seconds
    (state, serial) = @lock cam timeout=secs (cam.state, cam.serial)

The following commands can be sent to a remote camera:

- `configure(cam, secs=Inf, pairs...; kwds...)` changes the configuration of
  the camera `cam` (acquisition must not be running though), waiting no longer
  than `secs` seconds. Configuration parameters may be specified as keywords;
  named attributes may also be specified as `key => val` pairs with `key` the
  name of the attribute and `val` its value.

- `start(cam, secs=Inf)` starts image acquisition.

- `stop(cam, secs=Inf)` stops image acquisition.

- `abort(cam, secs=Inf)` aborts image acquisition.

- `reset(cam, secs=Inf)` resets the camera (this also aborts acquisition).

- `kill(cam, secs=Inf)` kills the server owning the camera.

- `cam[i]` or equivalently `fetch(cam, i)` yields the `i`-th output image from
  a remote camera `cam`. To retrieve the last output image, use `cam[]` or call
  `fetch(cam)`. An output image is a TAO shared array.

"""
RemoteCamera(owner::AbstractString) = RemoteCamera(ShmId(owner))
RemoteCamera(shmid::ShmId) = build(RemoteCamera, shmid)

RemoteCamera{T}(owner::AbstractString) where {T} = RemoteCamera{T}(ShmId(owner))
RemoteCamera{T}(shmid::ShmId) where {T} = build(RemoteCamera{T}, shmid)

RemoteCamera{T,N}(owner::AbstractString) where {T,N} = RemoteCamera{T,N}(ShmId(owner))
RemoteCamera{T,N}(shmid::ShmId) where {T,N} = build(RemoteCamera{T,N}, shmid)

function build(T::Type{<:RemoteCamera}, shmid::ShmId)
    ptr = tao_remote_camera_attach(shmid)
    return build(T, ptr)
end

function build(::Type{RemoteCamera}, ptr::Ptr{tao_remote_camera})
    check(ptr)
    try
        pixeltype = tao_get_pixeltype(ptr)
        T = DataType(pixeltype)
        preprocessing = tao_get_preprocessing(ptr)
        N = ndims(preprocessing)
        return unsafe_build(RemoteCamera{T,N}, ptr)
    catch ex
        tao_detach(ptr)
        throw(ex)
    end
end

function build(::Type{RemoteCamera{T}}, ptr::Ptr{tao_remote_camera}) where {T}
    check(ptr)
    try
        pixeltype = tao_get_pixeltype(ptr)
        T === DataType(pixeltype) || argument_error(
            "invalid pixel type `", T, "`, should be `", DataType(pixeltype), "`")
        preprocessing = tao_get_preprocessing(ptr)
        N = ndims(preprocessing)
        return unsafe_build(RemoteCamera{T,N}, ptr)
    catch ex
        tao_detach(ptr)
        throw(ex)
    end
end

function build(::Type{RemoteCamera{T,N}}, ptr::Ptr{tao_remote_camera}) where {T,N}
    check(ptr)
    try
        pixeltype = tao_get_pixeltype(ptr)
        T === DataType(pixeltype) || argument_error(
            "invalid pixel type `", T, "`, should be `", DataType(pixeltype), "`")
        preprocessing = tao_get_preprocessing(ptr)
        N == ndims(preprocessing) || argument_error(
            "invalid number of dimensions ", N, "`, should be ", ndims(preprocessing))
        return unsafe_build(RemoteCamera{T,Int(N)}, ptr)
    catch ex
        tao_detach(ptr)
        throw(ex)
    end
end

function unsafe_build(::Type{RemoteCamera{T,N}},
                      ptr::Ptr{tao_remote_camera},
                      lock::LockMode = UNLOCKED) where {T,N}
    nbufs = tao_get_nbufs(ptr)
    arrays = Vector{SharedArray{T,N}}(undef, nbufs)
    shmids = fill!(Vector{ShmId}(undef, nbufs), BAD_SHMID)
    obj = RemoteCamera{T,N}(ptr, lock, arrays, shmids)
    return finalizer(finalize, obj)
end

"""
    Remotecamera{T,N}(cam) -> ncam

yields a possibly new instance of a remote camera connected to the same server
as `cam` but with correct type parameters (pixel type and number of dimensions
of the produced images). If the type parameters have changed, a new instance is
returned which steals the handle of the input camera `cam` (which is modified
as if it has been *detached*). Otherwise, the input camera instance is returned
unchanged.

This may be useful after having configuring the camera with a different pixel
type and/or level of pre-processing.

To avoid type-instability, the pixel-type `T` and number `N` of dimensions (3
with full pre-processing, 2 otherwise) of the produced images should be
specified, they must match current settings.

"""
function RemoteCamera(cam::RemoteCamera{T,N}) where {T,N}
    ptr = getfield(cam, :ptr)
    isnull(ptr) && RemoteCamera{T,N}()
    pixeltype = cam.pixeltype
    Tp = DataType(pixeltype)
    preprocessing = cam.preprocessing
    Np = ndims(preprocessing)
    Tp === T && Np == N && return cam
    setfield!(cam, :ptr, null(ptr)) # as if camera detached
    return unsafe_build(RemoteCamera{Tp,Np}, ptr, cam.lock)
end

function RemoteCamera{Tp}(cam::RemoteCamera{T,N}) where {Tp,T,N}
    ptr = setfield!(cam, :ptr)
    isnull(ptr) && RemoteCamera{Tp,N}()
    pixeltype = cam.pixeltype
    Tp === DataType(pixeltype) || argument_error(
        "invalid pixel type `", Tp, "`, should be `", DataType(pixeltype), "`")
    preprocessing = cam.preprocessing
    Np = ndims(preprocessing)
    Tp === T && Np == N && return cam
    setfield!(cam, :ptr, typeof(ptr)(0)) # as if camera detached
    return unsafe_build(RemoteCamera{Tp,Np}, ptr, cam.lock)
end

function RemoteCamera{Tp,Np}(cam::RemoteCamera{T,N}) where {Tp,Np,T,N}
    ptr = setfield!(cam, :ptr)
    isnull(ptr) && RemoteCamera{Tp,Int(Np)}()
    pixeltype = cam.pixeltype
    Tp === DataType(pixeltype) || argument_error(
        "invalid pixel type `", Tp, "`, should be `", DataType(pixeltype), "`")
    preprocessing = cam.preprocessing
    Np == ndims(preprocessing) || argument_error(
        "invalid number of dimensions ", Np, "`, should be ", ndims(preprocessing))
    Tp === T && Np == N && return cam
    setfield!(cam, :ptr, typeof(ptr)(0)) # as if camera detached
    return unsafe_build(RemoteCamera{Tp,Int(Np)}, ptr, cam.lock)
end

# Build a detached instance.
RemoteCamera{T,N}() where {T,N} = RemoteCamera{T,N}(
    Ptr{Lib.tao_remote_camera}(0), UNLOCKED, SharedArray{T,N}[], ShmId[])

function Base.show(io::IO, obj::RemoteCamera)
    lock(io)
    try
        print(io, typeof(obj), "(\"", obj.owner, "\")")
        print(io, "\n ├─ shmid:          "); show(io, obj.shmid)
        print(io, "\n ├─ nbufs:          ", obj.nbufs)
        print(io, "\n ├─ exposure time:  ", obj.exposuretime)
        print(io, "\n ├─ frame rate:     ", obj.framerate)
        print(io, "\n ├─ sensor size:    ", obj.sensorwidth, " × ", obj.sensorheight)
        print(io, "\n ├─ image size:     ", obj.width, " × ", obj.height)
        print(io, "\n ├─ binning:        ", obj.xbin, " × ", obj.ybin)
        print(io, "\n ├─ offsets:        (", obj.xoff, ", ", obj.yoff, ")")
        print(io, "\n ├─ pixel type:     "); show(io, obj.pixeltype)
        print(io, "\n ├─ raw encoding:   "); show(io, obj.rawencoding)
        print(io, "\n ├─ pre-processing: "); show(io, obj.preprocessing)
        print(io, "\n ├─ state:          "); show(io, obj.state)
        print(io, "\n ├─ lock:           "); show(io, obj.lock)
        print(io, "\n ├─ serial:         ", obj.serial)
        print(io, "\n └─ ncmds:          ", obj.ncmds)
    finally
        unlock(io)
    end
end

"""
    ShmId(cam, n, lim = Inf) -> shmid::ShmId

yields the shared memory identifier of the shared array storing the `n`-th
output image of the remote TAO camera `cam`. The number `n` specifies the
serial number of the image. The returned value may be `BAD_SHMID = $BAD_SHMID`
to indicate that there is no such image (whatever the reason).

"""
function TaoRT.ShmId(cam::RemoteCamera, n::Integer, lim::TimeLimit = forever)
    r = @autolock cam lim tao_get_image_shmid(cam, n)
    # We purposely do not check the result by calling `Priv.check(ShmId(r))`.
    return ShmId(r)
end

# Simple commands for remote vameras.
for (func, type) in ((:start, RemoteCamera),
                     (:stop,  RemoteCamera),
                     (:abort, RemoteCamera),
                     (:reset, RemoteCamera))
    @eval $func(obj::$type, secs::Real = Inf) =
        check_command_serial($(Symbol("tao_",func))(assert_unlocked(obj), secs))
end

function configure(obj::RemoteCamera, cfg::CameraConfig, secs::Real=Inf)
    check_command_serial(tao_configure(assert_unlocked(obj), cfg, secs))
end

function merge_attributes!(dict::AbstractDict{K,Any}, attrs) where {K<:AbstractString}
    for (key, val) in attrs
        key = as(K, key)
        haskey(dict, key) && throw(ArgumentError("attribute \"$key\" specified more than once"))
        dict[key] = val
    end
end

function configure(cam::RemoteCamera,
                   args::Pair{<:AbstractString,<:Any}...; kwds...)
    return configure(cam, Inf, args...; kwds...)
end

function configure(cam::RemoteCamera,
                   secs::Real,
                   args::Pair{<:AbstractString,<:Any}...;
                   buffers::Union{Nothing,Integer} = nothing,
                   exposuretime::Union{Nothing,Real} = nothing,
                   framerate::Union{Nothing,Real} = nothing,
                   height::Union{Nothing,Integer} = nothing,
                   # FIXME: origin?
                   pixeltype = nothing,
                   preprocessing = nothing,
                   rawencoding = nothing,
                   width::Union{Nothing,Integer} = nothing,
                   xbin::Union{Nothing,Integer} = nothing,
                   xoff::Union{Nothing,Integer} = nothing,
                   ybin::Union{Nothing,Integer} = nothing,
                   yoff::Union{Nothing,Integer} = nothing,
                   kwds...)
    # Get current configuration.
    assert_unlocked(cam) # avoid dead-locks
    cfg = @lock cam CameraConfig(cam)

    # Dictionary of attributes. Needed to ensure unicity.
    dict = Dict{String,Any}()
    merge_attributes!(dict, args)
    merge_attributes!(dict, kwds)

    # Set properties.
    if buffers !== nothing
        cfg.buffers = buffers
    end
    if exposuretime !== nothing
        cfg.exposuretime = exposuretime
    end
    if framerate !== nothing
        cfg.framerate = framerate
    end
    if height !== nothing
        cfg.height = height
    end
    if pixeltype !== nothing
        cfg.pixeltype = pixeltype
    end
    if preprocessing !== nothing
        cfg.preprocessing = preprocessing
    end
    if rawencoding !== nothing
        cfg.rawencoding = rawencoding
    end
    if width !== nothing
        cfg.width = width
    end
    if xbin !== nothing
        cfg.xbin = xbin
    end
    if xoff !== nothing
        cfg.xoff = xoff
    end
    if ybin !== nothing
        cfg.ybin = ybin
    end
    if yoff !== nothing
        cfg.yoff = yoff
    end

    # Set attributes.
    for (key, val) in dict
        cfg[key] = val
    end

    # Apply configuration.
    return configure(cam, cfg, secs)
end

"""
    cam[n = cam.serial] -> arr
    fetch(cam, n = cam.serial) -> arr

yield the shared array storing the `n`-th image acquired by the remote camera
`cam`. The default is to return the last available image. It is possible to
specify `n = cam.serial + 1` to fetch the next available image.

The result is a shared array. The caller has the responsibility of locking it
(at least for read-only access) to preserve its contents and of controlling its
serial number. If the requested image is the next available one, attempting to
lock the image will automatically wait for the image to be effectively acquired
and it is guaranteed that the next image will be effectively stored in that
shared array. This is the reason to allow for fetching the next available
image. Fetching in the future beyond the next available image, that is with
`n > cam.serial + 1`, is forbidden with this method. The method
[`wait_output`](@ref) shall be used to wait for an image whose serial number is
greater than `cam.serial + 1`.

Fetching in the past is possible, that is with `n < cam.serial`, however output
images are stored in a cyclic list of length `cam.nbufs` so there is always the
risk that the requested image has been overwritten by a more recent one.

A typical usage is:

    n = cam.serial + 1 # choose the next acquired image
    arr = fetch(cam, n)
    @rdlock arr timeout=secs begin
        if arr.serial == n
            # This is a valid output image with the expected serial number.
            ...
        elseif cam.serial < n
            # Image has not yet been acquired in the allowed time.
            throw(TimeoutError())
        else
            # Image has been overwritten.
            throw(OverwriteError())
        end
    end

"""
function fetch(cam::RemoteCamera{T,N}, n::Integer = cam.serial,
               secs::Real = Inf) where {T,N}
    n < 1 && argument_error(
        "fetching before the first frame is not allowed")
    n > cam.serial + 1 && argument_error(
        "fetching in the future by more than one frame is not allowed")

    # Get the shared memory identifier of the requested image.
    shmid = @lock cam timeout=secs ShmId(cam, n)
    shmid == BAD_SHMID && throw_last_error()

    # To avoid the overheads of systematically attaching/detaching shared
    # arrays, figure out whether we already have attached the shared array in
    # our list.
    arrays = getfield(cam, :arrays)  # list of shared arrays
    shmids = getfield(cam, :shmids)  # list if shared memory identifiers
    index = (Int(n)::Int - 1)%length(shmids) + 1
    if shmids[index] != shmid
        arrays[index] = SharedArray{T,N}(shmid)
        shmids[index] = shmid
    end
    return arrays[index]
end

Base.getindex(cam::RemoteCamera) = fetch(cam)
Base.getindex(cam::RemoteCamera, i::Integer) = fetch(cam, i)

"""
    CameraConfig(cam) -> cfg

yields the current configuration of remote camera `cam`. The remote camera is
automatically locked and unlocked if not yet done by the caller. The
configuration can be modified and used to configure the camera with new
settings.

The returned object implements the `cam.key` syntax for the following
properties:

| Name            | Description                                            |
|:----------------|:-------------------------------------------------------|
| `buffers`       | Number of acquisition buffers                          |
| `droppedframes` | Number of dropped frames                               |
| `exposuretime`  | Exposure time in seconds                               |
| `framerate`     | Acquisition rate in frames per second                  |
| `frames`        | Number of frames acquired so far                       |
| `height`        | Number of lines of pixels in the ROI (in macro-pixels) |
| `lostframes`    | Number of lost frames                                  |
| `lostsyncs`     | Number of synchronization losts so far                 |
| `origin`        | Origin of time                                         |
| `overflows`     | Number of overflows                                    |
| `overruns`      | Number of frames lost due to overruns                  |
| `pixeltype`     | Pixel type in pre-processed images                     |
| `preprocessing` | Level of image pre-processing                          |
| `rawencoding`   | Pixel encoding in images acquired by the sensor        |
| `sensorheight`  | Number of physical pixels per column of the detector   |
| `sensorwidth`   | Number of physical pixels per row of the detector      |
| `timeouts`      | Number of timeouts so far                              |
| `width`         | Number of pixels per line of the ROI (in macro-pixels) |
| `xbin`          | Horizontal binning (in physical pixels)                |
| `xoff`          | Horizontal offset of the ROI (in physical pixels)      |
| `ybin`          | Vertical binning (in physical pixels)                  |
| `yoff`          | Vertical offset of the ROI (in physical pixels)        |

Other properties (known as *attributes*) that depend on the type of camera are
implemented as in a dictionary with string keys. The list of available keys is
defined once by the camera server and never change during the lifetime of the
camera.  Only the values of the writable keys can change.

    cfg[key]             # yield value of key
    cfg[key] = val       # set value of key
    isreadonly(cfg, key) # yield whether key is read-only
    iswritable(cfg, key) # yield whether key is writable
    get(cfg, key, def)   # yield value of key if it exists, or def otherwise
    haskey(cfg, key)     # yield whether key exists
    keys(cfg)            # yield iterator over existing keys
    values(cfg)          # yield iterator over values of existing keys

"""
function CameraConfig(cam::RemoteCamera)
    cfg = CameraConfig()
    @autolock cam check(tao_get_configuration(cam, cfg))
    return cfg
end

function Base.show(io::IO, cfg::CameraConfig)
    # Private method to do the work.
    function _show(io::IO, str::String, x)
        print(io, str)
        show(io, x)
    end
    @lock io begin
        _show(io, "CameraConfig(buffers = ", cfg.buffers)
        _show(io, ", droppedframes = ", cfg.lostsyncs)
        _show(io, ", exposuretime = ", cfg.exposuretime)
        _show(io, ", framerate = ", cfg.framerate)
        _show(io, ", frames = ", cfg.frames)
        _show(io, ", height = ", cfg.height)
        _show(io, ", lostframes = ", cfg.lostframes)
        _show(io, ", lostsyncs = ", cfg.lostsyncs)
        _show(io, ", origin = ", cfg.origin)
        _show(io, ", overflows = ", cfg.overflows)
        _show(io, ", overruns = ", cfg.overruns)
        _show(io, ", pixeltype = ", cfg.pixeltype)
        _show(io, ", preprocessing = ", cfg.preprocessing)
        _show(io, ", rawencoding = ", cfg.rawencoding)
        _show(io, ", sensorheight = ", cfg.sensorheight)
        _show(io, ", sensorwidth = ", cfg.sensorwidth)
        _show(io, ", timeouts = ", cfg.timeouts)
        _show(io, ", width = ", cfg.width)
        _show(io, ", xbin = ", cfg.xbin)
        _show(io, ", xoff = ", cfg.xoff)
        _show(io, ", ybin = ", cfg.ybin)
        _show(io, ", yoff = ", cfg.yoff)
        for (key, val) in cfg
            print(io, ", ", key, " => ", repr(val))
        end
        print(io, ")")
    end
end

Base.convert(::Type{tao_camera_config}, cfg::CameraConfig) = getfield(cfg, 1)
Base.unsafe_convert(::Type{Ptr{tao_camera_config}}, cfg::CameraConfig) =
    unsafe_pointer(cfg)

propertynames(::CameraConfig) = (
    :buffers,
    :droppedframes,
    :exposuretime,
    :framerate,
    :frames,
    :height,
    :lostframes,
    :lostsyncs,
    :origin,
    :overflows,
    :overruns,
    :pixeltype,
    :preprocessing,
    :rawencoding,
    :sensorheight,
    :sensorwidth,
    :timeouts,
    :width,
    :xbin,
    :xoff,
    :ybin,
    :yoff,)

# Deal with camera configuration properties.
@inline getproperty(cfg::CameraConfig, key::Symbol) = getproperty(cfg, Val(key))
@inline setproperty!(cfg::CameraConfig, key::Symbol, x) = setproperty!(cfg, Val(key), x)
setproperty!(cfg::CameraConfig, ::Val{sym}, val) where {sym} =
    throw_non_existing_or_read_only_property(cfg, sym)

# Read-only properties.
getproperty(obj::CameraConfig, key::Val{:sensorwidth})   = Int(load(obj, key))
getproperty(obj::CameraConfig, key::Val{:sensorheight})  = Int(load(obj, key))
getproperty(obj::CameraConfig, key::Val{:origin})        = load(obj, key)
getproperty(obj::CameraConfig, key::Val{:frames})        = load(obj, key)
getproperty(obj::CameraConfig, key::Val{:droppedframes}) = load(obj, key)
getproperty(obj::CameraConfig, key::Val{:overruns})      = load(obj, key)
getproperty(obj::CameraConfig, key::Val{:lostframes})    = load(obj, key)
getproperty(obj::CameraConfig, key::Val{:overflows})     = load(obj, key)
getproperty(obj::CameraConfig, key::Val{:lostsyncs})     = load(obj, key)
getproperty(obj::CameraConfig, key::Val{:timeouts})      = load(obj, key)

for f in (:width, :height, :xoff, :yoff, :xbin, :ybin)
    @eval begin
        getproperty(obj::CameraConfig, key::$(Val{f})) = Int(load(obj, Val(:roi), key))
        setproperty!(obj::CameraConfig, key::$(Val{f}), val) = store!(obj, val, Val(:roi), key)
    end
end
for f in (:buffers,)
    @eval begin
        getproperty(obj::CameraConfig, key::$(Val{f})) = Int(load(obj,  key))
        setproperty!(obj::CameraConfig, key::$(Val{f}), val) = store!(obj, val, key)
    end
end
for f in (:framerate, :exposuretime, :rawencoding, :pixeltype, :preprocessing)
    @eval begin
        getproperty(obj::CameraConfig, key::$(Val{f})) = load(obj, key)
        setproperty!(obj::CameraConfig, key::$(Val{f}), val) = store!(obj, val, key)
    end
end

# Union of possible types for attribute values.
const AttrValue = Union{Bool,Int64,Cdouble,String}

const AttrStore = Union{RemoteCamera,CameraConfig}

# Private iterators to iterate over attribute keys or values of a camera
# configuration.
abstract type AbstractAttrIter end
struct AttrKeyIter{T<:AttrStore} <: AbstractAttrIter
    parent::T
end
struct AttrValueIter{T<:AttrStore} <: AbstractAttrIter
    parent::T
end

Base.parent(iter::AbstractAttrIter) = getfield(iter, :parent)
Base.keys(obj::AttrStore) = AttrKeyIter(obj)
Base.values(obj::AttrStore) = AttrValueIter(obj)
Base.IteratorSize(::Type{<:AttrStore}) = Base.SizeUnknown()
Base.IteratorSize(::Type{<:AbstractAttrIter}) = Base.HasLength()
Base.IteratorEltype(::Type{<:AttrStore}) = Base.HasEltype()
Base.IteratorEltype(::Type{<:AbstractAttrIter}) = Base.HasEltype()
Base.eltype(::Type{<:AttrStore}) = Pair{String,AttrValue}
Base.eltype(::Type{<:AttrKeyIter}) = String
Base.eltype(::Type{<:AttrValueIter}) = AttrValue
function Base.length(iter::AbstractAttrIter)
    obj = parent(iter)
    GC.@preserve obj begin
        return as(Int, get_number_of_attributes(obj))
    end
end

function Base.iterate(iter::Union{AttrStore,AbstractAttrIter}, i::Int = 0)
    obj = iter isa AttrStore ? iter : parent(iter)
    GC.@preserve obj begin
        i += 1
        attr = unsafe_get_attr(obj, i)
        isnull(attr) && return nothing
        if iter isa AttrKeyIter
            return (unsafe_get_key(attr), i)
        elseif iter isa AttrValueIter
            return (unsafe_get_value(attr), i)
        else
            return (unsafe_get_key(attr) => unsafe_get_value(attr), i)
        end
    end
end

function Base.show(io::IO, iter::AttrKeyIter)
    lock(io)
    try
        print(io, typeof(iter), ": (")
        for key in iter
            print(io, "\"", key, "\", ")
        end
        print(io, ")")
    finally
        unlock(io)
    end
end

function Base.show(io::IO, iter::AttrValueIter)
    lock(io)
    try
        print(io, typeof(iter), ": (")
        for val in iter
            print(io, repr(val), ", ")
        end
        print(io, ")")
    finally
        unlock(io)
    end
end

function Base.isreadable(obj::Union{RemoteCamera,CameraConfig},
                         key::Union{AbstractString,Integer})
    flags = get_attr_flags(obj, key)
    return ! iszero(flags & TAO_ATTR_READABLE)
end

function Base.iswritable(obj::Union{RemoteCamera,CameraConfig},
                         key::Union{AbstractString,Integer})
    if obj isa RemoteCamera
        # An attribute is never directly writable for a camera.
        return false
    else
        flags = get_attr_flags(obj, key)
        return ! iszero(flags & TAO_ATTR_WRITABLE)
    end
end

function Base.isreadonly(obj::Union{RemoteCamera,CameraConfig},
                         key::Union{AbstractString,Integer})
    if obj isa RemoteCamera
        # An attribute is never directly writable for a camera.
        return isreadable(obj, key)
    else
        flags = get_attr_flags(obj, key)
        return (flags & (TAO_ATTR_READABLE|TAO_ATTR_WRITABLE)) == TAO_ATTR_READABLE
    end
end

function get_attr_flags(obj::Union{RemoteCamera,CameraConfig},
                        key::Union{AbstractString,Integer})
    GC.@preserve obj begin
        attr = unsafe_get_attr(obj, key)
        return unsafe_get_flags(attr)
    end
end

# Implement `obj[key]` syntax. NOTE: A remote camera only accept a string key
# here (integer key is for retrieving images), the `get` and `hashkey` methods
# may however be used in that case.
function Base.getindex(obj::RemoteCamera,
                       key::AbstractString)
    return get_attr_value(obj, key)
end
function Base.getindex(obj::CameraConfig,
                       key::Union{AbstractString,Integer})
    return get_attr_value(obj, key)
end

# Implement `obj[key] = val` syntax for a camera configuration (an attribute is
# never directly writable for a camera). NOTE: It is not possible to create a
# new entry, only change the value of an existing one.
function Base.setindex!(obj::CameraConfig, val,
                        key::Union{AbstractString,Integer})
    set_attr_value!(obj, key, val)
    return obj
end
function Base.setindex!(obj::RemoteCamera, val,
                        key::AbstractString)
    @nospecialize val
    error("an attribute is never directly writable for a camera, use `configure` for that")
end

Base.get(obj::Union{RemoteCamera,CameraConfig}, key, def) = def
function Base.get(obj::Union{RemoteCamera,CameraConfig},
                  key::Union{AbstractString,Integer}, def)
    GC.@preserve obj begin
        if key isa Integer
            attr = unsafe_get_attr(obj, key)
            isnull(attr) && return def
        else
            i = unsafe_try_find_attr(obj, key)
            i < zero(i) && return def
            attr = unsafe_get_attr(obj, i)
        end
        return unsafe_get_value(attr)
    end
end

Base.haskey(obj::Union{RemoteCamera,CameraConfig}, key) = false
function Base.haskey(obj::Union{RemoteCamera,CameraConfig},
                     key::Union{AbstractString,Integer})
    GC.@preserve obj begin
        if key isa Integer
            return one(key) ≤ key ≤ get_number_of_attributes(obj)
        else
            i = unsafe_try_find_attr(obj, key)
            return i ≥ zero(i)
        end
    end
end

function get_attr_value(::Type{T},
                        obj::Union{RemoteCamera,CameraConfig},
                        key::Union{AbstractString,Integer}) where {T}
    GC.@preserve obj begin
        attr = unsafe_get_attr(obj, key)
        isnull(attr) && throw(KeyError(key))
        return unsafe_get_value(T, attr)
    end
end

function get_attr_value(obj::Union{RemoteCamera,CameraConfig},
                        key::Union{AbstractString,Integer})
    GC.@preserve obj begin
        attr = unsafe_get_attr(obj, key)
        isnull(attr) && throw(KeyError(key))
        return unsafe_get_value(attr)
    end
end

# An attribute is never directly writable for a camera, so this function is
# only provided for camera configuration object.
function set_attr_value!(obj::CameraConfig,
                         key::Union{AbstractString,Integer},
                         val; overwrite::Bool = false)
    GC.@preserve obj begin
        attr = unsafe_get_attr(obj, key)
        isnull(attr) && throw(KeyError(key))
        type = unsafe_get_type(attr)
        if type == TAO_ATTR_BOOLEAN
            check(tao_attr_set_boolean_value(attr, as(Bool, val), overwrite))
        elseif type == TAO_ATTR_INTEGER
            check(tao_attr_set_integer_value(attr, val, overwrite))
        elseif type == TAO_ATTR_FLOAT
            check(tao_attr_set_float_value(attr, val, overwrite))
        elseif type == TAO_ATTR_STRING
            check(tao_attr_set_string_value(attr, val, overwrite))
        else
            unknown_attribute_type(type)
        end
    end
    nothing
end

@noinline unknown_attribute_type(type) = error("unknown attribute type `$type`")

"""
    TaoRT.Priv.get_number_of_attributes(obj) -> n

yields the number of named attributes for `obj`, a remote camera or a camera
configuration.

This function is labeled "unsafe" because the caller is responsible of
preserving `obj` from being garbage collected.

"""
get_number_of_attributes(obj::Union{RemoteCamera,CameraConfig}) = tao_get_attr_number(obj)

"""
    TaoRT.Priv.unsafe_try_find_attr(obj, key) -> i

yields the index `i` of the named attributes `key` for `obj`, a remote camera
or a camera configuration. The returned index `i` is `-1` if attribute is not
found, `-2` if any argument is invalid, or a non-negative 0-based index if
`key` is found.

This function is labeled "unsafe" because the caller is responsible of
preserving `obj` from being garbage collected.

"""
function unsafe_try_find_attr(obj::Union{RemoteCamera,CameraConfig},
                              key::AbstractString)
    return tao_try_find_attr(obj, key)
end

"""
    TaoRT.Priv.unsafe_get_attr(obj, key) -> attr

yields the address of the named attributes at index `key` for `obj`, a remote
camera or a camera configuration. `key` may be a 0-based integer index or a
string. A null pointer is returned if the key is not found or out of range.

This function is labeled "unsafe" because the caller is responsible of
preserving `obj` from being garbage collected while using the returned
attribute address.

"""
function unsafe_get_attr(obj::Union{RemoteCamera,CameraConfig},
                         key::Union{AbstractString,Integer})
    # NOTE: We need to make a few checks to avoid throwing an error.
    if key isa Integer
        zero(key) < key ≤ get_number_of_attributes(obj) || return Ptr{tao_attr}(0)
        i = key - 1 # 1-based index -> 0-based index
    else
        i = unsafe_try_find_attr(obj, key)
        i ≥ zero(i) || return Ptr{tao_attr}(0)
    end
    attr = tao_get_attr(obj, i)
    isnull(attr) && clear_error()
    return attr
end

"""
    TaoRT.Priv.unsafe_get_flags(attr)

yields the bitwise flags of the named attribute `attr`. Argument may be a null
pointer.

This function is labeled "unsafe" because the caller is responsible of
preserving the object owning the attribute from being garbage collected.

"""
unsafe_get_flags(attr::Ptr{tao_attr}) = tao_attr_get_flags(attr)

"""
    TaoRT.Priv.unsafe_get_type(attr)

yields the type of the named attribute `attr`. Argument may be a null pointer.
This function leaves thread's last error unchanged.

This function is labeled "unsafe" because the caller is responsible of
preserving the object owning the attribute from being garbage collected.

"""
unsafe_get_type(attr::Ptr{tao_attr}) = tao_attr_get_type(attr)

"""
    TaoRT.Priv.unsafe_get_key(attr)

yield the name of the named attribute `attr`. An exception is thrown in case of
error.

This function is labeled "unsafe" because the caller is responsible of
preserving the object owning the attribute from being garbage collected.

"""
function unsafe_get_key(attr::Ptr{tao_attr})
    # NOTE: `unsafe_string` will throws an error with a null pointer (this is
    # what we want) and, in principle, key is guaranteed to be NULL-terminated.
    # If you do not trust this, see `safer_unsafe_string`.
    return unsafe_string(Ptr{UInt8}(tao_attr_get_key(attr)))
end

"""
    TaoRT.Priv.unsafe_get_max_val_size(attr) -> n

yields the maximum number of bytes (including the final null if it is a string)
to store the value of named attribute `attr`. Returns 0 if argument is a null
pointer.

This function is labeled "unsafe" because the caller is responsible of
preserving the object owning the attribute from being garbage collected.

"""
unsafe_get_max_val_size(attr::Ptr{tao_attr}) = tao_attr_get_max_val_size(attr)

"""
    TaoRT.Priv.unsafe_get_max_key_size(attr) -> n

yields the maximum number of bytes (including the final null if it is a string)
to store the value of named attribute `attr`. Returns 0 if argument is a null
pointer.

This function is labeled "unsafe" because the caller is responsible of
preserving the object owning the attribute from being garbage collected.

"""
unsafe_get_max_key_size(attr::Ptr{tao_attr}) = tao_attr_get_max_key_size(attr)

"""
    TaoRT.Priv.unsafe_get_value([type,] attr) -> val

yields the value of the named attribute `attr`. Optional argument `T` is the
type to assume. An exception is thrown in case of error.

This function is labeled "unsafe" because the caller is responsible of
preserving the object owning the attribute from being garbage collected.

"""
function unsafe_get_value(attr::Ptr{tao_attr})
    type = unsafe_get_type(attr) # guaranteed to be TAO_ATTR_NONE is attr is NULL
    return type == TAO_ATTR_BOOLEAN ? unsafe_get_value(Bool,    attr) :
           type == TAO_ATTR_INTEGER ? unsafe_get_value(Int64,   attr) :
           type == TAO_ATTR_FLOAT   ? unsafe_get_value(Cdouble, attr) :
           type == TAO_ATTR_STRING  ? unsafe_get_value(String,  attr) :
           unknown_attribute_type(type)
end

function unsafe_get_value(::Type{Bool}, attr::Ptr{tao_attr})
    val = Ref{Cint}()
    check(tao_attr_get_boolean_value(attr, val))
    return ! iszero(val[])
end

function unsafe_get_value(::Type{T}, attr::Ptr{tao_attr}) where {T<:Integer}
    val = Ref{Int64}()
    check(tao_attr_get_integer_value(attr, val))
    return as(T, val[])
end

function unsafe_get_value(::Type{T}, attr::Ptr{tao_attr}) where {T<:AbstractFloat}
    val = Ref{Cdouble}()
    check(tao_attr_get_float_value(attr, val))
    return as(T, val[])
end

function unsafe_get_value(::Type{String}, attr::Ptr{tao_attr})
    ref = Ref{Cstring}()
    check(tao_attr_get_string_value(attr, ref))
    return unsafe_string(ref[])
end

"""
    TaoRT.Priv.safer_unsafe_string(src::Ptr, siz::Int) -> str

copies a C-string (NUL-terminated) encoded as UTF-8 which has at most `siz`
bytes including the final NUL.

This function is labeled "unsafe" because it will crash if `src` is not a valid
memory address to data of the assumed size. This function is however "safer"
because it copies the `siz - 1` bytes at address `src` in a temporary buffer of
size `siz` and set the last entry of this buffer to NUL before calling
`unsafe_string` on it. In other words, this function will not crash is
arguments are valid but the pointed string is not NUL terminated. This is
probably overkill.

"""
function safer_unsafe_string(src::Ptr, siz::Integer)
    isnull(src) && throw(ArgumentError("invalid null string pointer"))
    siz > zero(siz) && throw(ArgumentError("invalid maximum number of bytes"))
    buf = Array{UInt8}(undef, siz)
    GC.@preserve buf begin
        dst = pointer(buf)
        siz > 1 && @ccall memcpy(dst::Ptr{Cvoid}, src::Ptr{Cvoid}, (len - 1)::Csize_t)::Ptr{Cvoid}
        @inbounds buf[siz] = 0
        return unsafe_string(dst)
    end
end
