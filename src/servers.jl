"""
    TaoRT.Servers

This module implements methods for TAO servers.

"""
module Servers

#export
#    command_done,
#    get_command,
#    publish_shmid,
#    set_state,
#    unpublish_shmid

using ..TaoRT
using ..TaoRT:
    Command,
    Remote,
    State
using ..Priv:
    assert_locked,
    check,
    tao_command_done,
    tao_get_command,
    tao_publish_shmid,
    tao_set_state,
    tao_unpublish_shmid

const TIMEOUT = Ref(5.0)

"""
    TaoRT.Servers.run_server(obj)

runs the main-loop of a simple server using remote object `obj` to communicate
with clients.

Method [`TaoRT.Servers.work`](@ref) shall be extended for the type of `obj` to
execute the task to be done by the server when a `start` command is received.

The caller must own the object which must has been freshly created.

On return, the object is detached.

"""
function run_server(obj::Remote)
    @assert obj.state == :initializing
    @lock obj begin
        publish_shmid(obj)
        # Run the outer server loop.
        while obj.state != :quitting
            # Wait for next command.
            set_state(obj, :waiting)
            while (command = get_command(obj)) == :none
                wait(obj, TIMEOUT[]) # FIXME: NOTE: obj ressource unlocked here
            end
            # Process next command.
            clear_command = true # in most cases we want to clear the pending command
            if command == :config
                set_state(obj, :configuring)
                accept_config(obj)
            elseif command == :start
                # Start working. This is done in several stages. Each stage
                # constructs an object whose type may be different each time
                # and continues with next stage. This is to cope with type
                # uncertainty once when entering the working state.
                set_state(obj, :starting)
                try
                    work(obj)
                catch err
                    report_error(err)
                end
                # Clear the pending command if not yet done.
                clear_command = (get_command(obj) == :start)
                # Ensure consistentcy of the state.
                obj.state != :quitting && set_state(obj, :stopping)
            elseif command == :stop
                set_state(obj, :stopping)
            elseif command == :abort
                set_state(obj, :aborting)
            elseif command == :kill
                set_state(obj, :quitting)
            else
                report_error("Invalid command: $command")
            end
            clear_command && command_done(obj)
        end
        set_state(obj, :unreachable)
        unpublish_shmid(obj)
    end
    detach(obj)
end

"""
    TaoRT.Servers.work(obj)

starts the processing loop for remote object `obj` used to communicate with
clients. The caller must be the owner of `obj` and must have locked `obj` for
exclusive (read-write) access.

This method is intended to be specialized for each concrete type of remote
object so as to create any additional ressources and perform any initialization
before eventually call:

    TaoRT.Servers.work(task::Function, obj, args...)

where `task(args...)` is repeatedly called to execute the server work as soon
as a `start` command is received and until a `stop` or a `kill` command is
received. As `task` is a function and the first argument, the do-block syntax
may be used:

    TaoRT.Servers.work(obj) do
        ... # execute server work
    end

Otherwise, if `task` is provided as a function which takes `obj` as one of its
arguments, the `obj` argument has to be repeated in `args...`.

"""
work(obj::Remote) = nothing # nothing to do or not yet implemented

# Run the inner loop of the server when working.
function work(task::Function, obj::Remote, args...)
    set_state(obj, :working)
    command_done(obj)
    while true
        # Get pending command, if any.
        if (command = get_command(obj)) != :none
            if command == :config
                report_error("Invalid \"$command\" command while running")
            elseif command == :start
                nothing
            elseif command == :stop
                set_state(obj, :stopping)
            elseif command == :abort
                set_state(obj, :aborting)
            elseif command == :kill
                set_state(obj, :quitting)
            else
                report_error("Invalid command: $command")
            end
            command_done(obj)
            if obj.state != :working
                return # to outer loop
            end
        end
        # Execute task (catching errors is done at a higher level).
        task(args...)
    end
end

report_error(ex::Exception) = show(stderr, ex)
function report_error(msg::AbstractString)
    lock(stderr)
    try
        printstyled(stderr, "ERROR "; bold = true, color = :red)
        printstyled(stderr, msg; bold = false, color = :red)
    finally
        unlock(stderr)
    end
end

"""
    TaoRT.Servers.get_command(obj) -> cmd

yields the current pending command for the remote object `obj`.

!!! warning
    The caller should be the owner of the object and must have locked the
    object. Clients may only post commands.

"""
get_command(obj::Remote) = tao_get_command(assert_locked(obj))

"""
    TaoRT.Servers.command_done(obj)

acknowledges that the pending command for remote object `obj` has been
executed. If object `obj` has a pendind command, the pending command is cleared
(set to `TAO_COMMAND_NONE`), the command counter of the object is incremented,
and others processes are notified that the object has changed.

!!! warning
    The caller should be the owner of the object and must have locked the
    object for exclusive (read-write) access. This function shall not be called
    once the object becomes unreachable.

"""
command_done(obj::Remote) = check(tao_command_done(assert_locked(obj)))

"""
    TaoRT.Servers.set_state(obj, state)

sets the state of the remote object `obj`. If the state changes, others
processes are notified. If the object is unreachable, it is an error if `state`
is not `:unreachable` or `TAO_STATE_UNREACHABLE`.

!!! warning
    The caller should be the owner of the object and must have locked the
    object for exclusive (read-write) access. Clients may only read the state,
    e.g. with `obj.state`.

"""
set_state(obj::Remote, state::State) = check(tao_set_state(assert_locked(obj), state))
set_state(obj::Remote, sym::Symbol) = set_state(obj, State(sym)::State)

"""
    TaoRT.Servers.publish_shmid(obj)

writes the shared memory identifier of the owned remote object `obj` to the
public configuration so that other processes can connect to the object. The
object shall have been locked by the caller who must be the owner of the
object.

"""
publish_shmid(obj::Remote) = check(tao_publish_shmid(assert_locked(obj)))

"""
    TaoRT.Servers.unpublish_shmid(obj)

writes `TAO_BAD_SHMID` as the shared memory identifier of the owned remote
object `obj` to the public configuration so that other processes can no longer
connect to the object. The object shall have been locked by the caller who must
be the owner of the object.

"""
unpublish_shmid(obj::Remote) = check(tao_unpublish_shmid(assert_locked(obj)))

"""
    TaoRT.Servers.accept_config(obj::TaoRT.Remote)

copies the secondary configuration of `obj` into its primary configuration. The
object shall have been locked by the caller who must be the owner of the
object.

"""
accept_config(obj::Remote) = check(tao_unpublish_shmid(assert_locked(obj)))

end # module
