#
# remote_objects.jl -
#
# Low level interafce to basic remote shared objects for the Julia interface to the
# real-time C library of TAO, a Toolkit for Adaptive Optics.
#
# This file is part of TAO software (https://git-cral.univ-lyon1.fr/tao) licensed under
# the MIT license.
#
# Copyright (c) 2018-2024, Éric Thiébaut.
#

import ..TaoRT: RemoteObject

propertynames(obj::RemoteObject) = (
    :alive,
    :flags,
    :lock,
    :nbufs,
    :ncmds,
    :owner,
    :perms,
    :pid,
    :serial,
    :shmid,
    :size,
    :state,
    :type,
)

getproperty(obj::Remote, ::Val{:owner}) =
    GC.@preserve obj unsafe_string(tao_get_owner(obj))

for key in (:nbufs, :ncmds, :pid, :serial, :state)
    @eval getproperty(obj::Remote, ::$(Val{key})) = $(Symbol("tao_get_",key))(obj)
end

getproperty(obj::Remote, ::Val{:alive})  = !iszero(tao_is_alive(obj))

"""
    TaoRT.RemoteObject(id) -> obj

attaches to the TAO remote object identified by `id` which can be the name of
the server owning the object or the shared memory identifier of the object.

The returned object implements the `obj.key` syntax with the following
properties:

| Name     | Description                                            |
|:---------|:-------------------------------------------------------|
| `alive`  | (a) Whether server owning the object is alive          |
| `flags`  | (c) Bitwise flags of the shared object                 |
| `lock`   | (p) Type of lock owned by the caller                   |
| `nbufs`  | (c) Number of entries in cyclic list of output buffers |
| `ncmds`  | (a) Number of commands processed by the server         |
| `owner`  | (c) Name of the server owning the object               |
| `pid`    | (c) Process identifier of owner                        |
| `perms`  | (c) Permissions access bits of the shared object       |
| `serial` | (a) Number of outputs produced by the server           |
| `shmid`  | (c) Shared memory identifier of the object             |
| `size`   | (c) Number of bytes allocated for the shared object    |
| `state`  | (a) Current server state                               |
| `type`   | (c) Type identifier of the shared object               |

- (a) atomic shared member, no needs to lock the object;
- (c) constant member, no needs to lock the object;
- (p) private member, no needs to lock the object.

!!! warning
    Properties should all be considered as read-only by the end-user and never
    directly modified or unexpected behavior may occur.

"""
RemoteObject(id::Union{AbstractString,Integer}) = RemoteObject(ShmId(id))
RemoteObject(shmid::ShmId) = build(RemoteObject, shmid)

function build(T::Type{<:RemoteObject}, shmid::ShmId)
    ptr = tao_remote_object_attach(shmid)
    return build(T, ptr)
end

"""
    TaoRT.RemoteObject(
        owner, type, nbufs, offset, stride, size,
        flags = TaoRT.DEFAULT_PERMS) -> obj

creates a new TAO remote object.

!!! warning
    Creating a remote object is mainly for testing as there are no means yet to
    manage the object as a true server in Julia.

"""
function RemoteObject(owner::AbstractString,
                      type::Integer,
                      nbufs::Integer,
                      offset::Integer,
                      stride::Integer,
                      size::Integer,
                      flags::Integer = DEFAULT_PERMS)
    return build(RemoteObject, owner, type, nbufs, offset, stride, size, flags)
end

function build(T::Type{<:RemoteObject},
               owner::AbstractString,
               type::Integer,
               nbufs::Integer,
               offset::Integer,
               stride::Integer,
               size::Integer,
               flags::Integer)
    sizeof(owner) < TAO_OWNER_SIZE || argument_error("owner name too long")
    ptr = tao_remote_object_create(owner, type, nbufs, offset,
                                   stride, size, flags)
    return build(T, ptr)
end

function Base.show(io::IO, obj::Remote)
    lock(io)
    try
        print(io, typeof(obj), "(\"", obj.owner,"\")")
        print(io, "\n ├─ shmid:  "); show(io, obj.shmid)
        print(io, "\n ├─ nbufs:  ", obj.nbufs)
        print(io, "\n ├─ state:  "); show(io, obj.state)
        print(io, "\n ├─ lock:   "); show(io, obj.lock)
        print(io, "\n ├─ serial: ", obj.serial)
        print(io, "\n └─ ncmds:  ", obj.ncmds)
    finally
        unlock(io)
    end
end

"""
    configure(obj, secs=Inf; key1=val1, key2=val2, ...) -> cmdnum

sends a configuration command with parameters `key1=val1`, `key2=val2`, ..., to
the server owning the remote object `obj` and waiting no longer than `secs`
seconds for being able to send the command.

The command is executed asynchronously, the returned value `cmdnum` is the
serial number of the command which can be used with [`wait_command`](@ref) to
wait for the command to complete.

The caller must not have locked the object.

Depending on the type of the remote object, the configuration parameters may
also be specifed as single configuration structure, say `cfg`, as in:

    configure(obj, cfg, secs=Inf) -> cmdnum

""" configure

"""
    start(obj, secs=Inf) -> cmdnum

sends a command to start the main loop of the server owning the remote object
`obj`, not waiting more than `secs` seconds for being able to send the command.

The command is executed asynchronously, the returned value `cmdnum` is the
serial number of the command which can be used with [`wait_command`](@ref) to
wait for the command to complete.

The caller must not have locked the object.

""" start

"""
    stop(obj, secs=Inf) -> cmdnum

sends a command to stop the main loop of the server owning the remote object
`obj`, not waiting more than `secs` seconds for being able to send the command.

The command is executed asynchronously, the returned value `cmdnum` is the
serial number of the command which can be used with [`wait_command`](@ref) to
wait for the command to complete.

The caller must not have locked the object.

""" stop

"""
    abort(obj, secs=Inf) -> cmdnum

sends a command to abort the main loop of the server owning the remote object
`obj`, not waiting more than `secs` seconds for being able to send the command.

The command is executed asynchronously, the returned value `cmdnum` is the
serial number of the command which can be used with [`wait_command`](@ref) to
wait for the command to complete.

The caller must not have locked the object.

""" abort

"""
    kill(obj, secs=Inf) -> cmdnum

sends a command to make the server owning the remote object `obj` to exit, not
waiting more than `secs` seconds for being able to send the command.

The command is executed asynchronously, the returned value `cmdnum` is the
serial number of the command which can be used with [`wait_command`](@ref) to
wait for the command to complete.

The caller must not have locked the object.

"""
kill(obj::Remote, secs::Real = Inf) =
    check_command_serial(tao_kill(assert_unlocked(obj), secs))

"""
    wait_command(obj, cmdnum, secs=Inf)
    wait_command(obj, cmdnum => datnum, secs=Inf)

wait for the `cmdnum`-th command sent to the server owning the remote object
`obj` to be processed but waiting no longer than `secs` seconds. The integer
`cmdnum` is the command number returned by a previous command.

The caller must not have locked the object.

Example:

    # Set some parameters and check that they are correct.
    cmdnum = configure(obj; exposuretime=2e-3, framerate=50)
    wait_command(obj, cmdnum)
    lock(obj) do
        (obj.exposuretime ≈ 2e-3 && obj.framerate ≈ 50) || error(
            "failed to set the configuration")
    end
    # Settings are correct, start image acquisition
    start(obj)

"""
wait_command(obj::Remote, pair::Pair{Serial,Serial}, secs::Real = Inf) =
    wait_command(obj, first(pair), secs)

wait_command(obj::Remote, cmdnum::Integer, secs::Real = Inf) =
    check(tao_wait_command(assert_unlocked(obj), cmdnum, secs))

"""
    wait_output(obj, datnum = 0, secs=Inf) -> actnum
    wait_output(obj, cmdnum => datnum, secs=Inf) -> actnum

wait for the `datnum`-th output to be made available by the server owning the
remote object `obj` but waiting no longer than `secs` seconds. If `datnum ≤ 0`,
the next output that will be produced by the server is waited for.

The returned value `actnum` is the actual serial number of the requested
output, a strictly positive integer which is equal to `datnum` if `datnum > 0`
was specified.

The caller must not have locked the object.

For a remote camera, the result `actnum` can be used with [`ShmId`](@ref) to
retrieve the identifier of the shared array storing the `actnum`-th image.

In case of failure, the following exceptions may be thrown:

- `TimeoutError` if the requested output is not available before the time limit
  (this may be due to the server being no longer reachable);

- `OverwriteError` if the requested output is too old and has been overwritten
  by a newer one (outputs are stored in a cyclic list of `obj.nbufs` entries);

- `TaoError` if another error occurred.

"""
wait_output(obj::Remote, pair::Pair{Serial,Serial}, secs::Real = Inf) =
    wait_output(obj, last(pair), secs)

function wait_output(obj::Remote, datnum::Integer = 0, secs::Real = Inf)
    actnum = tao_wait_output(assert_unlocked(obj), datnum, secs)
    if actnum > 0
        # A strictly positive result is a success.
        return actnum
    elseif actnum == 0 || actnum == -2
        # 0 indicates a timeout, -2 that the server is unreachable.
        throw_timeout()
    elseif actnum == -1
        # -1 indicates that output was overwritten.
        throw(OverwriteError())
    else
        # Anything else is an error.
       throw_last_error()
    end
end

"""
    obj[n = cam.serial] -> dat
    fetch(obj, n = obj.serial) -> dat

retrieve the `n`-th data-frame stored by TAO remote object `obj`. The number
`n` is the serial number of the data-frame. By default, the last available
data-frame is fetched. The syntaxes `obj[n]` and `obj[]` are shortcuts to
`fetch(obj,n)` and `fetch(obj)`.

For a remote camera, the returned data is a shared TAO array, for other TAO
remote objects, the returned structured data has, at least, the following
properties:

- `dat.serial` is the serial number of the data-frame;

- `dat.mark` is the user-defined mark of the data-frame;

- `dat.time` is the timestamp of the data-frame;

""" fetch

Base.getindex(obj::Remote, n::Integer = obj.serial) = fetch(obj, n)

#------------------------------------------------------------------------------
# DATA-FRAMES

# Implement `dat.key` syntax for data-frames.
getproperty(dat::DataFrame, key::Symbol) = getproperty(dat, Val(key))

# Implement `dat.serial`, `dat.mark`, and `dat.time` for data-frames as
# shortcuts for `dat.info.serial`, `dat.info.mark`, and `dat.info.time`.
for key in (:serial, :mark, :time)
    @eval getproperty(dat::DataFrame, ::$(Val{key})) =
        getfield(getfield(dat, :info), $(QuoteNode(key)))
end
