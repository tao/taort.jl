#
# generic.jl --
#
# Implement generic low-level methods for the Julia interface to the real-time
# C library of TAO, a Toolkit for Adaptive Optics.
#
#------------------------------------------------------------------------------
#
# This file is part of TAO software (https://git-cral.univ-lyon1.fr/tao)
# licensed under the MIT license.
#
# Copyright (c) 2023-2024, Éric Thiébaut.
#

const Object = Union{Shared,CameraConfig}

"""
    TaoRT.Priv.load(obj, keys...) -> val

yields the value stored into the C structure associated with the shared TAO
object `obj` along the path of members specified by `keys...`. An error is
thrown if the associated pointer is invalid (because the shared object has been
detached).

For example:

    TaoRT.Priv.load(cam, :roi, :xoff)

yields the value of the member `xoff` in the member `roi` of the C structure
associated with `cam`.

"""
@inline load(obj::Object, key::Symbol, keys::Symbol...) =
    load(obj, map(Val, (key, keys...))...)
@inline function load(obj::Object, key::Val, keys::Val...)
    ptr = unsafe_pointer(safe_pointer(obj), key, keys...)
    GC.@preserve obj unsafe_load(ptr)
end

"""
    TaoRT.Priv.store!(obj, val, keys...)

stores the value `val` into the C structure associated with the shared TAO
object `obj` along the path of members specified by `keys...`. An error is
thrown if the associated pointer is invalid (because the shared object has been
detached).

For example:

    TaoRT.Priv.store!(arr, 123, :serial)

"""
@inline function store!(obj::Object, val, key::Val, keys::Val...)
    ptr = unsafe_pointer(safe_pointer(obj), key, keys...)
    GC.@preserve obj unsafe_store!(ptr, as(eltype(ptr), val))
    nothing
end

"""
    TaoRT.Priv.safe_pointer(obj::TaoRT.Priv.Object) -> ptr

yields a pointer to the C structure associated with the shared TAO object
`obj`, throwing an error if the pointer is invalid (null).

"""
function safe_pointer(obj::Shared)
    ptr = unsafe_pointer(obj)
    isnull(ptr) && error("object has been detached")
    return ptr
end
function safe_pointer(cfg::CameraConfig)
    Ptr{tao_camera_config}(pointer_from_objref(cfg) + fieldoffset(typeof(cfg), 1))
end

"""
    TaoRT.Priv.unsafe_pointer(T, A, off)

yields a pointer for data of type `T` stored at offset `off` (in bytes) in
**mutable** object `A`. The pointer is only valid for the lifetime of `A`. To
avoid that `A` be garbage collected while using the pointer, a typical usage
is:

    ptr = unsafe_pointer(T, A, off)
    GC.@preserve A begin
        # Use the pointer in this protected block.
        ...
    end

This method is needed ot overcome the limitations of the usual mechanism based
on helper methods `Base.cconvert` and `Base.unsafe_convert`.

"""
unsafe_pointer(::Type{T}, A, off::Integer = 0) where {T} =
    Ptr{T}(pointer_from_objref(A) + off)

"""
    TaoRT.Priv.unsafe_pointer(obj::TaoRT.Priv.Object) -> ptr

yields a pointer to the C structure associated with the TAO object `obj`. This
pointer is not checked for validity.

"""
unsafe_pointer(obj::Shared) = getfield(obj, :ptr)
unsafe_pointer(cfg::CameraConfig) = safe_pointer(cfg)

"""
    TaoRT.Priv.unsafe_pointer(obj::Shared, keys...) -> ptr

yields the pointer to the C sub-structure along the path of members specified
by `keys...` relative to the C structure associated with the shared TAO object
`obj`.

"""
@inline unsafe_pointer(x::Ptr, keys::Symbol...) =
    unsafe_pointer(x, map(Val, keys)...)

@inline unsafe_pointer(x::Ptr, key::Val, keys::Val...) =
    unsafe_pointer(unsafe_pointer(x, key), keys...)

# error catcher
function unsafe_pointer(ptr::Ptr, key::Val{K}) where {K}
    @nospecialize ptr key
    throw(KeyError(K))
end

"""
    TaoRT.Priv.member_names(T::Type)

yields the symbolic names of the members of the TAO C structure `T`.

""" member_names # FIXME usued method

# Extend `unsafe_pointer` and `member_names` for TAO structures that have Julia
# equivalent.
for sym ∈ names(Lib)
    startswith(string(sym), "tao_") || continue
    let T = @eval Lib $sym
        (T isa DataType && isconcretetype(T) && isstructtype(T) && fieldcount(T) > 0) || continue
        keys = Symbol[]
        for i in 1:fieldcount(T)
            key = fieldname(T, i)
            type = fieldtype(T, i)
            offset = fieldoffset(T, i)
            push!(keys, key)
            @eval begin
                unsafe_pointer(x::Ptr{$T}, ::$(Val{key})) = Ptr{$type}(x + $offset)
            end
        end
        @eval member_names(::Type{Ptr{$T}}) = $((keys...,))
    end
end

unsafe_pointer(x::Ptr{tao_remote_camera}, ::Val{:base}) = x.base
unsafe_pointer(x::Ptr{tao_remote_camera}, ::Val{:config}) = x.config
unsafe_pointer(x::Ptr{tao_remote_camera}, ::Val{:preproc}) = x.preproc
unsafe_pointer(x::Ptr{tao_remote_camera}, ::Val{:config2}) = x.arg.config

"""
    TaoRT.Priv.pointed_type(ptr) -> T

yields the type of the element pointed by `ptr`. Argument can be a type or an
instance.

"""
pointed_type(ptr::Ptr) = pointed_type(typeof(ptr))
pointed_type(::Type{Ptr{T}}) where {T} = T

"""
    TaoRT.Priv.c_struct_name(x) -> sym

yields the symbolic name of the C structure in the TAO library corresponding to
`x`. Argument can be an instance or the type of a high-level TAO object or the
symbolic name of that type (such as `:RemoteCamera` or `:SharedArray`). The
returned name is unqualified (no module prefix).

"""
c_struct_name(x::Any) = c_struct_name(type_name(x))
let expr = :(error("unknown high-level TAO object type `", sym, "`"))
    for type in (SHARED_OBJECT_TYPES..., CameraConfig)
        ctype = type === CameraConfig ? tao_camera_config :
            pointed_type(fieldtype(type, :ptr))
        name = QuoteNode(nameof(type))
        cname = QuoteNode(nameof(ctype))
        expr = Expr(:if, :(sym === $name), cname, expr)
        @eval c_struct_name(T::Type{<:$type}) = $cname
    end
    @eval c_struct_name(sym::Symbol) = $expr
end

"""
    TaoRT.Priv.type_name(T) -> sym

yields the symbolic name of the type `T`. Argument can be a type, a symbol, or
an expression. In the former case, the result is the same as `nameof(T)`.

"""
type_name(x::Type) = nameof(x)
type_name(x::Symbol) = x
type_name(x::QuoteNode) = type_name(x.value)
type_name(x::Expr) =
    (x.head === :(.) && length(x.args) ≥ 2) ? type_name(x.args[end]) :
    error("unexpected type expression `$x`")

# `is_symbol_with_type_assertion(ex)` yields whether `ex` is an expression is
# of the form `name::type` with `name` a symbolic name.
is_symbol_with_type_assertion(ex::Any) = false
is_symbol_with_type_assertion(ex::Expr) =
    ex.head === :(::) && length(ex.args) == 2 && ex.args[1] isa Symbol

"""
    @generic func(arg1::T, ...)

encodes generic method to call the C function of the TAO real-time library
implementing `func` for objects of type `T` which is one of the high-level TAO
shared object type. `func` is such that a function named `"\$(ctype)_\$(func)"`
exists in TAO real-time library with `ctype` the name of the C structure
corresponding to the type `T`. There may be any other arguments `...`
with/without type assertion.

For example:

    @generic tao_get_dim(A::SharedArray, i::Integer)
    @generic tao_get_layout(A::RemoteMirror, dims)

yields the following code:

    tao_get_dim(A::Union{TaoRT.SharedArray,Ptr{TaoRT.Lib.tao_shared_array}}, i::Integer) =
        TaoRT.Lib.tao_shared_array_get_dim(A, i)
    tao_get_layout(A::Union{TaoRT.RemoteMirror, Ptr{TaoRT.Lib.tao_remote_mirror}}, dims) =
        TaoRT.Lib.tao_remote_mirror_get_layout(A, dims)

The generic method can be called with a high-level Julia object (such as
`TaoRT.RemoteCamera`) or with a pointer to the opaque C object (such as
`TaoRT.Lib.tao_remote_camera`). When called with a high-level Julia object, the
object is automatically preserved from being garbage collected.

"""
macro generic(ex::Expr)
    esc(generic(ex))
end

# This function is in charge of building the expression for the `@generic` macro.
function generic(ex::Expr)
    ex.head === :call || error("expecting function call")
    args = ex.args
    length(args) ≥ 2 || error("expecting at least one argument")
    func = args[1]
    func isa Symbol || error("unexpected non-symbol type for function name")
    verb = String(func)
    startswith(verb, "tao_") || error("function name (`$func`) must start with `tao_`")
    verb = SubString(verb, firstindex(verb)+4:lastindex(verb))

    # Parse first argument. Must be of the form `name::type`.
    name, type = let arg = args[2]
        is_symbol_with_type_assertion(arg) || error(
            "first argument must be a simple variable with a type assertion")
        arg.args[1], type_name(arg.args[2])
    end
    ctype = c_struct_name(type)
    cfunc = :(TaoRT.Lib.$(Symbol(ctype,"_",verb)))
    lhs = Expr(:call, func)  # left-hand side of short function definition
    rhs = Expr(:call, cfunc) # right-hand side of short function definition
    push!(lhs.args, :($name::Union{TaoRT.$type,Ptr{TaoRT.Lib.$ctype}}))
    push!(rhs.args, name)

    # Other arguments.
    for i in 3:length(args)
        let arg = args[i]
            push!(lhs.args, arg)
            if arg isa Symbol
                push!(rhs.args, arg)
            elseif is_symbol_with_type_assertion(arg)
                push!(rhs.args, arg.args[1])
            elseif i == length(args) && arg isa Expr && arg.head === :(...) &&
                length(arg.args) == 1 && arg.args[1] isa Symbol
                # Last argument has the `args...` form.
                push!(rhs.args, arg)
            else
                error("argument $(i-1) must be a simple variable, possibly with a type assertion, not an expression")
            end
        end
    end

    # Return short function definition.
    return :($lhs = $rhs)
end

for type in SHARED_OBJECT_TYPES
    @eval begin
        # Extend Base.unsafe_convert to automatically extract the handle from a
        # shared Tao object. This secures and simplifies calls to functions of
        # the TAO real-time library. See `get_handle` doc.
        Base.unsafe_convert(::$(Type{fieldtype(type, :ptr)}), obj::$type) =
            getfield(obj, :ptr)

        # Encode generic functions available for any shared object.
        @generic tao_detach(obj::$type)
        @generic tao_get_flags(obj::$type)
        @generic tao_get_perms(obj::$type)
        @generic tao_get_shmid(obj::$type)
        @generic tao_get_size(obj::$type)
        @generic tao_get_type(obj::$type)
        @generic tao_unlock(obj::$type)
    end
end

for type in (SharedObject, REMOTE_OBJECT_TYPES...)
    @eval begin
        @generic tao_abstimed_lock(obj::$type, lim)
        @generic tao_abstimed_wait_condition(obj::$type, lim)
        @generic tao_broadcast_condition(obj::$type)
        @generic tao_lock(obj::$type)
        @generic tao_signal_condition(obj::$type)
        @generic tao_timed_lock(obj::$type, lim)
        @generic tao_timed_wait_condition(obj::$type, lim)
        @generic tao_try_lock(obj::$type)
        @generic tao_wait_condition(obj::$type)
    end
end

for type in RWLOCKED_OBJECT_TYPES
    @eval begin
        @generic tao_abstimed_rdlock(obj::$type, lim)
        @generic tao_abstimed_wrlock(obj::$type, lim)
        @generic tao_rdlock(obj::$type)
        @generic tao_timed_rdlock(obj::$type, lim)
        @generic tao_timed_wrlock(obj::$type, lim)
        @generic tao_try_rdlock(obj::$type)
        @generic tao_try_wrlock(obj::$type)
        @generic tao_wrlock(obj::$type)
    end
end

for type in REMOTE_OBJECT_TYPES
    @eval begin
        @generic tao_command_done(obj::$type)
        @generic tao_get_command(obj::$type)
        @generic tao_get_nbufs(obj::$type)
        @generic tao_get_ncmds(obj::$type)
        @generic tao_get_owner(obj::$type)
        @generic tao_get_pid(obj::$type)
        @generic tao_get_serial(obj::$type)
        @generic tao_get_state(obj::$type)
        @generic tao_is_alive(obj::$type)
        @generic tao_publish_shmid(obj::$type)
        @generic tao_set_state(obj::$type, state)
        @generic tao_unpublish_shmid(obj::$type)
        @generic tao_wait_command(obj::$type, num, lim)
        @generic tao_wait_output(obj::$type, num, lim)
    end
end

let type = SharedArray
    @eval begin
        @generic tao_get_data(obj::$type)
        @generic tao_get_dim(obj::$type, i)
        @generic tao_get_eltype(obj::$type)
        @generic tao_get_length(obj::$type)
        @generic tao_get_ndims(obj::$type)
        @generic tao_get_serial(obj::$type)
        @generic tao_get_timestamp(obj::$type, i, t)
        @generic tao_set_serial(obj::$type, num)
        @generic tao_set_timestamp(obj::$type, i, t)
    end
end

let type = RemoteCamera
    @eval begin
        @generic tao_abort(obj::$type, lim)
        @generic tao_configure(obj::$type, cfg, lim)
        @generic tao_get_attr(obj::$type, i::Integer)
        @generic tao_get_attr_number(obj::$type)
        @generic tao_get_configuration(obj::$type, dst)
        @generic tao_get_droppedframes(obj::$type)
        @generic tao_get_exposuretime(obj::$type)
        @generic tao_get_framerate(obj::$type)
        @generic tao_get_frames(obj::$type)
        @generic tao_get_height(obj::$type)
        @generic tao_get_image_shmid(obj::$type, num)
        @generic tao_get_lostframes(obj::$type)
        @generic tao_get_lostsyncs(obj::$type)
        @generic tao_get_origin(obj::$type)
        @generic tao_get_overflows(obj::$type)
        @generic tao_get_overruns(obj::$type)
        @generic tao_get_pixeltype(obj::$type)
        @generic tao_get_preprocessing(obj::$type)
        @generic tao_get_preprocessing_shmid(obj::$type, i)
        @generic tao_get_rawencoding(obj::$type)
        @generic tao_get_sensorheight(obj::$type)
        @generic tao_get_sensorwidth(obj::$type)
        @generic tao_get_timeouts(obj::$type)
        @generic tao_get_width(obj::$type)
        @generic tao_get_xbin(obj::$type)
        @generic tao_get_xoff(obj::$type)
        @generic tao_get_ybin(obj::$type)
        @generic tao_get_yoff(obj::$type)
        @generic tao_kill(obj::$type, lim)
        @generic tao_reset(obj::$type, lim)
        @generic tao_start(obj::$type, lim)
        @generic tao_stop(obj::$type, lim)
        @generic tao_try_find_attr(obj::$type, key::AbstractString)
    end
end

let type = CameraConfig
    @eval begin
        @generic tao_get_attr(obj::$type, i::Integer)
        @generic tao_get_attr_number(obj::$type)
        @generic tao_try_find_attr(obj::$type, key::AbstractString)
    end
end

let type = RemoteMirror
    @eval begin
        @generic tao_fetch_data(obj::$type, datnum, refcmds, perturb, reqcmds, devcmds, nvals, info)
        @generic tao_find_mark(obj::$type, mrk)
        @generic tao_get_cmax(obj::$type)
        @generic tao_get_cmin(obj::$type)
        @generic tao_get_dims(obj::$type)
        @generic tao_get_layout(obj::$type, dims)
        @generic tao_get_mark(obj::$type)
        @generic tao_get_nacts(obj::$type)
        @generic tao_get_reference(obj::$type)
        @generic tao_kill(obj::$type, lim)
        @generic tao_reset(obj::$type, mark, lim, num)
        @generic tao_send_commands(obj::$type, vals, nvals, mark, lim, num)
        @generic tao_set_perturbation(obj::$type, vals, nvals, lim, num)
        @generic tao_set_reference(obj::$type, vals, nvals, lim, num)
     end
end

let type = RemoteSensor
    @eval begin
        # Layout.
        @generic tao_get_inds(obj::$type)
        @generic tao_get_ninds(obj::$type)
        @generic tao_get_max_ninds(obj::$type)
        @generic tao_get_dims(obj::$type)
        # Sub-images.
        @generic tao_get_subs(obj::$type)
        @generic tao_get_nsubs(obj::$type)
        @generic tao_get_max_nsubs(obj::$type)
        # Other parameters.
        @generic tao_get_camera_owner(obj::$type)
        @generic tao_fetch_data(obj::$type, datnum, data, nvals, info)
        @generic tao_get_control(obj::$type, ctrl)
        @generic tao_set_control(obj::$type, ctrl)
        # Configuration.
        @generic tao_get_config(obj::$type, cfg)
        @generic tao_configure(obj::$type, cfg, lim)
        @generic tao_accept_config(obj::$type)
        # Other commands.
        @generic tao_start(obj::$type, lim)
        @generic tao_stop(obj::$type, lim)
        @generic tao_abort(obj::$type, lim)
        @generic tao_reset(obj::$type, lim)
        @generic tao_kill(obj::$type, lim)
     end
end
