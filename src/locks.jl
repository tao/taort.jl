for (lock, class, mode) in ((:lock,   :Lockable,   :READ_WRITE),
                            (:rdlock, :RWLockable, :READ_ONLY),
                            (:wrlock, :RWLockable, :READ_WRITE))

    # Extend low-level API to dela with various ways to specify a timeout.
    @eval begin
        $(Symbol("tao_",lock))(obj::$(class), timeout::Forever) =
            $(Symbol("tao_",lock))(obj)

        $(Symbol("tao_",lock))(obj::$(class), timeout::Real) =
            $(Symbol("tao_timed_",lock))(obj, timeout)

        $(Symbol("tao_",lock))(obj::$(class), timeout::TimeValue) =
            $(Symbol("tao_abstimed_",lock))(obj, timeout)
    end

    # Implement `lock`.
    @eval function $(lock)(obj::$(class); timeout::TimeLimit = forever)
        assert_unlocked(obj) # avoid dead-locks
        status = $(Symbol("tao_",lock))(obj, timeout)
        if status == TAO_OK
            setfield!(obj, :lock, $(mode))
            return true
        elseif status == TAO_TIMEOUT
            return false
        else
            throw_last_error()
        end
    end

    # Implement do-block syntax for `lock`
    @eval function $(lock)(f::Function, obj::$(class); timeout::TimeLimit = forever)
        $(lock)(obj) || throw_timeout()
        try
            f()
        finally
            unlock(obj)
        end
    end

    # Implement `try_lock`.
    @eval function $(Symbol("try_",lock))(obj::$(class))
        assert_unlocked(obj) # avoid dead-locks
        status = $(Symbol("tao_try_",lock))(obj)
        if status == TAO_OK
            setfield!(obj, :lock, $(mode))
            return true
        elseif status == TAO_TIMEOUT
            return false
        else
            throw_last_error()
        end
    end

    # Implement do-block syntax for `try_lock`
    @eval function $(Symbol("try_",lock))(f::Function, obj::$(class))
        if $(Symbol("try_",lock))(obj)
            try
                f()
            finally
                unlock(obj)
            end
        else
            nothing
        end
    end
end

@doc """
    lock(obj::TaoRT.Lockable; timeout = Inf) -> bool

locks TAO object `obj` for exclusive access. The call blocks until the lock can be
acquired but no longer than the time limit specified by `timeout` (waiting forever by
default). If `timeout` is a number, it is interpreted as a relative time limit in seconds
from now; otherwise, `timeout` can be an instance of `HighResolutionTime` to specify an
absolute time limit. The returned Boolean value indicates whether the lock can be acquired
by the caller before the time limit expired.

Typical usage is:

    if lock(obj; timeout=...)
        # Object has been locked for exclusive access by the caller.
        try
            # Execute code while object is locked.
            ...
        finally
            # Unlock object as soon as exclusive access no longer needed.
            unlock(obj)
        end
    else
        # Time-out occured before lock can be obtained.
        throw(Tao.TimeoutError())
    end

To simplify such a construction, the `do`-block syntax or, better, the `@lock` macro
may be used:

    lock(obj; timeout=...) do
        ... # code to be executed while object is locked
    end

or:

    @lock obj timeout=... do
        ... # code to be executed while object is locked
    end

The latter has improved performance as it avoids creating a closure.

!!! warning
    Objects should remain locked as briefly as possible so the `do`-block syntax or,
    better, the `@lock` macro are the recommended ways to lock (and automatically unlock)
    objects.

""" lock(obj::Lockable)

"""
    lock(obj::TaoRT.Lockable, mode; timeout = Inf) -> bool

locks TAO object `obj` for access `mode` waiting no longer than `timeout`. Access `mode`
can be `TaoRT.READ_ONLY` for read-only access or `TaoRT.READ_WRITE` for read-write access.

For r/w lockable objects, read-only access is equivalent to calling [`rdlock`](@ref) while
read-write access is equivalent to calling [`wrlock`](@ref). For other objects, the only
possibility is read-write access which is equivalent to `lock(obj)`.

"""
lock(obj::Lockable, mode::LockMode; timeout::TimeLimit = forever) =
    mode == READ_WRITE ? lock(obj; timeout) : throw_invalid_lock_mode()
lock(obj::RWLockable, mode::LockMode; timeout::TimeLimit = forever) =
    mode == READ_ONLY ? rdlock(obj; timeout) :
    mode == READ_WRITE ? wrlock(obj; timeout) :
    throw_invalid_lock_mode()

@noinline throw_invalid_lock_mode() = throw(ArgumentError("invalid lock mode"))

@doc """
    lock(f::Function, obj::TaoRT.Lockable; timeout=Inf)

locks TAO object `obj` for exclusive access, executes the function `f()`, unlocks `obj`,
and returns the result of the function call. If the time limit `timeout` is exhausted
before the lock can be acquired, a `Tao.TimeoutError` exception is thrown and `f()` is not
called. This is equivalent to:

    lock(obj; timeout=...) || throw(Tao.TimeoutError())
    try
        f()
    finally
        unlock(obj)
    end

This version of `lock` is typically used in a `do`-block construction:

    lock(obj; timeout=...) do
        ... # code to execute while object is locked
    end

The macro version `@lock` has improved performance as it avoids creating a closure:

    @lock obj timeout=... begin
        ... # code to execute while object is locked
    end

""" lock(f::Function, obj::Lockable)

"""
    @lock obj [opts...] expr

evaluates code in `expr` while lockable object `obj` is locked by the caller. Options may
be specified in `opts...` with the syntax `option = value`. Available options are
`timeout` to specify the time limit for locking the object and `on_timeout` to specify the
expression to evaluate if the time limit is exhausted. By default, `timeout = Inf` and
`on_timeout = throw(TimeoutError())`.

If any options are specified, this macro call is equivalent to:

    if lock(obj; timeout)
        try
             expr
        finally
             unlock(obj)
        end
    else
        on_timeout
    end

without any options, this macro call is equivalent to:

    lock(obj)
    try
         expr
    finally
         unlock(obj)
    end

See also [`lock`](@ref lock(::TaoRT.Shared)), [`unlock`](@ref unlock(::TaoRT.Shared)),
[`@try_lock`](@ref), [`@rdlock`](@ref), and [`@wrlock`](@ref).

"""
macro lock(obj, args...)
    encode_lock_macro(:lock, obj, args)
end

# NOTE `Base.@lock` already implements the case without options.

function encode_lock_macro(func, obj, args::Tuple)
    length(args) ≥ 1 || error("missing `expr` in `@$(func) obj opts... expr`")
    expr = last(args)
    timeout = :(TaoRT.forever)
    on_timeout = :(Base.throw(TaoRT.TimeoutError()))
    for opt in args[1:end-1]
        if opt isa Expr && opt.head === :(=)
            if opt.args[1] === :timeout
                timeout = opt.args[2]
                continue
            end
            if opt.args[1] === :on_timeout
                on_timeout = opt.args[2]
                continue
            end
        end
        error("invalid option, should be `timeout=...` or `on_timeout=...`")
    end
    quote
        temp = $(esc(obj))
        if $(func)(temp; timeout = $(esc(timeout)))
            try
                $(esc(expr))
            finally
                unlock(temp)
            end
        else
            $(esc(on_timeout))
        end
    end
end

"""
    unlock(obj::TaoRT.Shared)

unlocks TAO shared object `obj` that has been locked by the caller by [`lock(obj)`](@ref),
[`try_lock(obj)`](@ref)), [`rdlock(obj)`](@ref), [`try_rdlock(obj)`](@ref),
[`wrlock(obj)`](@ref), or [`try_wrlock(obj)`](@ref). Call [`islocked(obj)`](@ref) to check
whether `obj` is locked.

"""
function unlock(obj::Shared)
    assert_locked(obj)
    setfield!(obj, :lock, UNLOCKED)
    check(tao_unlock(obj))
end

"""
    unlock(f::Function, obj::TaoRT.Shared; timeout = Inf)

unlocks TAO shared object `obj`, calls `f()`, relocks `obj` and returns the result of the
function call. If the time limit `timeout` is exhausted before the object can be relocked, a
`Tao.TimeoutError` exception is thrown.

This version of `unlock` is typically used in a `do`-block construction:

    lock(obj; timeout=...) do
        ... # code to execute while object is locked
    end

The macro version [`@unlock`](@ref) is more performant as it avoids creating a closure:

    @unlock obj [timeout=...] begin
        ... # code to be executed while object is unlocked
    end

"""
function unlock(f::Function, obj::Shared; timeout::TimeLimit = forever)
    # Unlock object, remembering its initial lock mode.
    mode = obj.lock
    unlock(obj)
    try
        # Execute code.
        f()
    finally
        # Relock the object according to its initial lock mode.
        lock(obj, mode; timeout) || throw_timeout()
    end
end

"""
    @unlock obj [timeout = Inf] expr

unlocks lockable object `obj`, evaluates code in `expr`, and relocks `obj` according to
its initial access mode (read-only or read-write). If the object cannot be relocked before
the time limit specified by `timeout`, a `TimeoutError` exception is thrown. The result of
the macro is that of `expr` unless an exception occurs.

This macro call is equivalent to:

    mode = obj.lock
    unlock(obj)
    try
        expr
    finally
        lock(obj, mode; timeout) || throw(TimeoutError())
    end

See also [`unlock`](@ref unlock(::TaoRT.Shared)), and [`lock`](@ref lock(::Shared, mode,
timeout)).

"""
macro unlock(obj, args...)
    length(args) ≥ 1 || error("missing `expr` in `@unlock obj opts... expr`")
    expr = last(args)
    timeout = :(TaoRT.forever)
    for opt in args[1:end-1]
        if opt isa Expr && opt.head === :(=) && opt.args[1] === :timeout
            timeout = opt.args[2]
            continue
        end
        error("invalid option, should be `timeout=...`")
    end
    quote
        # Unlock object, remembering its initial lock mode.
        temp = $(esc(obj))
        mode = temp.lock
        unlock(temp)
        try
            # Execute code while object is unlocked.
            $(esc(expr))
        finally
            # Relock the object according to its initial lock mode.
            lock(temp, mode; timeout=$(esc(timeout))) || throw_timeout()
        end
    end
end

"""
    islocked(obj::TaoRT.Shared) -> boolean

yields whether shared TAO object `obj` is locked by the caller.

See also: [`lock(::TaoRT.Shared)`](@ref), [`try_lock(::TaoRT.Shared)`](@ref),
[`rdlock(::TaoRT.Shared)`](@ref), [`try_rdlock(::TaoRT.Shared)`](@ref),
[`wrlock(::TaoRT.Shared)`](@ref), [`try_wrlock(::TaoRT.Shared)`](@ref), and
[`unlock(::TaoRT.Shared)`](@ref).

"""
islocked(obj::Shared) = (getfield(obj, :lock) !== UNLOCKED)

@doc """
    try_lock(obj::TaoRT.Lockable) -> bool

attempts to immediately lock TAO object `obj` for exclusive access. This is similar to
calling [`lock`](@ref lock(::TaoRT.Lockable)) with a time limit of zero seconds:

    lock(obj, 0) -> bool

"""  try_lock(obj::Lockable)

@doc """
    try_lock(f::Function, obj::TaoRT.Lockable)

if TAO object `obj` can be immediately locked for exclusive access, executes function
`f()`, unlocks object, and returns the result of the function call. If the lock cannot be
immediately acquired, `f()` is not called and `nothing` is returned.

    if try_lock(obj)
        try
            f()
        finally
            unlock(obj)
        end
    else
        nothing
    end

This version of `try_lock` is typically used in a `do`-block construction:

    try_lock(obj) do
        ... # code to execute while object is locked
    end

The macro version `@try_lock` has improved performance as it avoids creating a closure:

    @try_lock obj begin
        ... # code to execute while object is locked
    end

""" try_lock(f::Function, obj::Lockable)

"""
    @try_lock obj expr

if lockable object `obj` can be immediately locked by the caller, evaluates code in `expr`
and unlocks `obj`. This macro call is equivalent to:

    if try_lock(obj)
        try
             expr
        finally
             unlock(obj)
        end
    else
        nothing
    end

The same behavior can be obtained with `@lock` by:

    @lock obj timeout=0 on_timeout=nothing expr

See also [`try_lock`](@ref), [`unlock`](@ref unlock(::TaoRT.Shared)),
[`@try_rdlock`](@ref), and [`@try_wrlock`](@ref).

"""
macro try_lock(obj, expr)
    encode_try_lock_macro(:try_lock, obj, expr)
end

function encode_try_lock_macro(func, obj, expr)
    quote
        temp = $(esc(obj))
        if $(func)(temp)
            try
                $(esc(expr))
            finally
                unlock(temp)
            end
        else
            nothing
        end
    end
end

@doc """
    rdlock(obj::TaoRT.RWLockable; timeout = Inf) -> bool

locks TAO read/write-lockable object `obj` for read-only access. At any time, for a given
shared object, there can be any number of readers and no writers or no readers and at most
one writer. The call blocks until the lock can be acquired but no longer than the time
limit specified by `timeout` (if unspecified, there is no time limit).

Except for the assumed access policy, `rdlock` behaves like [`lock`](@ref
lock(::TaoRT.Lockable)).

""" rdlock(obj::RWLockable)

@doc """
    rdlock(f::Function, obj::TaoRT.RWLockable; timeout = Inf)

implements the `do`-block syntax for [`rdlock`](@ref). For improved performance, use the
[`@rdlock`](@ref) macro.

""" rdlock(f::Function, obj::RWLockable)

"""
    @rdlock obj [opts...] expr

evaluates code in `expr` while r/w lockable object `obj` is locked for read-only access by
the caller. Options may be specified in `opts...` with the syntax `option = value`.
Available options are `timeout` to specify the time limit for locking the object and
`on_timeout` to specify the expression to evaluate if the time limit is exhausted. By
default, `timeout = Inf` and `on_timeout = throw(TaoRT.TimeoutError())`.

This macro call is equivalent to:

    if rdlock(obj; timeout=...)
        try
             expr
        finally
             unlock(obj)
        end
    else
        on_timeout
    end

See also [`rdlock`](@ref), [`unlock`](@ref unlock(::TaoRT.Shared)), [`@try_rdlock`](@ref),
[`@lock`](@ref lock(::TaoRT.Shared)), and [`@wrlock`](@ref).

"""
macro rdlock(obj, args...)
    encode_lock_macro(:rdlock, obj, args)
end

@doc """
    try_rdlock(obj::TaoRT.RWLockable) -> bool

attempts to immediately lock object `obj` for read-only access. This is similar to calling
[`rdlock`](@ref) with a time limit of zero seconds:

    rdlock(obj, 0) -> bool

""" try_rdlock(obj::RWLockable)

@doc """
    try_rdlock(f::Function, obj::TaoRT.RWLockable; timeout = Inf)

implements the `do`-block syntax for [`try_rdlock`](@ref). For improved performance, use
the [`@try_rdlock`](@ref) macro.

""" try_rdlock(f::Function, obj::RWLockable)

"""
    @try_rdlock obj expr

if r/w lockable object `obj` can be immediately locked for read-only access by the caller,
evaluates code in `expr` and unlocks `obj`. This macro call is equivalent to:

    if try_rdlock(obj)
        try
             expr
        finally
             unlock(obj)
        end
    else
        nothing
    end

The same behavior can be obtained with `@rdlock` by:

    @rdlock obj timeout=0 on_timeout=nothing expr

See also [`try_rdlock`](@ref), [`unlock`](@ref unlock(::TaoRT.Shared)),
[`@try_wrlock`](@ref), and [`@try_lock`](@ref).

"""
macro try_rdlock(obj, expr)
    encode_try_lock_macro(:try_rdlock, obj, expr)
end

@doc """
    wrlock(obj::TaoRT.RWLockable; timeout = Inf) -> bool

locks TAO read/write-lockable object `obj` for read-write access. At any time, for a given
shared object, there can be any number of readers and no writers or no readers and at most
one writer. The call blocks until the lock can be acquired but no longer than the time
limit specified by `timeout` (if unspecified, there is no time limit).

`wrlock` behaves like [`lock`](@ref lock(::TaoRT.Lockable)) but for r/w-locked objects.

""" wrlock(obj::RWLockable)

@doc """
    wrlock(f::Function, obj::TaoRT.RWLockable; timeout = Inf)

implements the `do`-block syntax for [`wrlock`](@ref). For improved performance, use the
[`@wrlock`](@ref) macro.

""" wrlock(f::Function, obj::RWLockable)

"""
    @wrlock obj [opts...] expr

evaluates code in `expr` while r/w lockable object `obj` is locked for read-write access
by the caller. Options may be specified in `opts...` with the syntax `option = value`.
Available options are `timeout` to specify the time limit for locking the object and
`on_timeout` to specify the expression to evaluate if the time limit is exhausted. By
default, `timeout = Inf` and `on_timeout = throw(TaoRT.TimeoutError())`.

This macro call is equivalent to:

    if wrlock(obj; timeout=...)
        try
             expr
        finally
             unlock(obj)
        end
    else
        on_timeout
    end

See also [`wrlock`](@ref), [`unlock`](@ref unlock(::TaoRT.Shared)), [`@try_wrlock`](@ref),
[`lock`](@ref lock(::TaoRT.Shared)), and [`@rdlock`](@ref).

"""
macro wrlock(obj, args...)
    encode_lock_macro(:wrlock, obj, args)
end

@doc """
    try_wrlock(obj::TaoRT.RWLockable) -> bool

attempts to immediately lock object `obj` for read-write access. This is similar to
calling [`wrlock`](@ref) with a time limit of zero seconds:

    wrlock(obj, 0) -> bool

""" try_lock(obj::RWLockable)

@doc """
    try_wrlock(f::Function, obj::TaoRT.RWLockable; timeout = Inf)

implements the `do`-block syntax for [`try_wrlock`](@ref). For improved performance, use
the [`@try_wrlock`](@ref) macro.

""" try_wrlock(f::Function, obj::RWLockable)

"""
    @try_wrlock obj expr

if r/w lockable object `obj` can be immediately locked for read-write (exclusive) access
by the caller, evaluates code in `expr` and unlocks `obj`. This macro call is equivalent
to:

    if try_wrlock(obj)
        try
             expr
        finally
             unlock(obj)
        end
    else
        nothing
    end

The same behavior can be obtained with `@wrlock` by:

    @wrlock obj timeout=0 on_timeout=nothing expr

See also [`try_wrlock`](@ref), [`unlock`](@ref unlock(::TaoRT.Shared)),
[`@try_rdlock`](@ref), and [`@try_lock`](@ref).

"""
macro try_wrlock(obj, expr)
    encode_try_lock_macro(:try_wrlock, obj, expr)
end

"""
    autolock(obj::TaoRT.Lockable; timeout = Inf) do
        ... # object is locked
    end

manage to have TAO object `obj` locked while executing the code by the ellipsis
(`...`) and then restored to its initial lock state.

"""
function autolock(f::Function, obj::Lockable; timeout = forever)
    locked = islocked(obj)
    locked || lock(obj; timeout) || throw_timeout()
    try
        f()
    catch ex
        throw(ex)
    finally
        locked || unlock(obj)
    end
end

macro autolock(obj, expr)
    quote
        temp = $(esc(obj))
        locked = islocked(temp)
        locked || lock(temp)
        try
            $(esc(expr))
        finally
            locked || unlock(temp)
        end
    end
end

macro autolock(obj, timeout, expr)
    quote
        temp = $(esc(obj))
        locked = islocked(temp)
        locked || lock(temp; timeout = $(esc(timeout))) || throw_timeout()
        try
            $(esc(expr))
        finally
            locked || unlock(temp)
        end
    end
end

"""
    TaoRT.Priv.assert_locked(obj::TaoRT.Shared) -> obj

throws an error if shared TAO object `obj` is not locked, yields `obj` otherwise.

"""
assert_locked(obj::Shared) = islocked(obj) ? obj : throw_not_locked()

"""
    TaoRT.Priv.assert_unlocked(obj::TaoRT.Shared)

throws an error if shared TAO object `obj` is locked, yields `obj` otherwise.

"""
assert_unlocked(obj::Shared) = islocked(obj) ? throw_already_locked() : obj

@noinline throw_not_locked() = error("object is not locked by caller")
@noinline throw_already_locked() = error("object already locked by caller")
