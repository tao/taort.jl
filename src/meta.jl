# meta.jl --
#
# Meta-programming.
#
#------------------------------------------------------------------------------
#
# This file is part of TAO software (https://git-cral.univ-lyon1.fr/tao)
# licensed under the MIT license.
#
# Copyright (C) 2018-2023, Éric Thiébaut.
#

"""
    TaoRT.Priv.field_index(T, sym::Symbol) -> i::Int

yields the index of the field whose symbolic name is `sym` for a structure of
type `T`. This introspection function is rather slow and mostly designed for
meta-programming, avoid using it for run-time code.

"""
function field_index(::Type{T}, f::Symbol) where {T}
    for i in 1:fieldcount(T)
        f === fieldname(T, i) && return i
    end
    error("type `", T, "` has no field `", f, "`")
end

"""
    TaoRT.Priv.align(off::Integer, T::Type)

rounds offset `off` to the least multiple of `sizeof(T)` that is greater or
equal `off`.

"""
function align(off::Integer, ::Type{T}) where {T}
    off ≥ 0 || argument_error("offset must be nonnegative")
    n = sizeof(T)
    return div(off + (n - 1), n)*n
end

"""
    TaoRT.Priv.unsafe_get_field(A, T, off) -> val::T

yields the value of type `T` stored at offset `off` (in bytes) in **mutable**
object `A` . The method is labelled *unsafe* because it does not check the
validity of the arguments. It however takes care of avoiding that `A` be
garbage collected during the operation.

If the value is an ASCII C string stored in a fixed length array of `siz`
bytes at offset `off` in `A`, call:

    TaoRT.Priv.unsafe_get_field(A, Cstring, off, siz) -> str::String

"""
@inline unsafe_get_field(A, ::Type{T}, off::Integer) where {T} =
    GC.@preserve A unsafe_load(unsafe_pointer(T, A, off))

function unsafe_get_field(A, ::Type{Cstring}, off::Integer, size::Int)
    ptr = unsafe_pointer(UInt8, A, off)
    return GC.@preserve A unsafe_string(ptr, unsafe_string_length(ptr, size))
end

"""
    TaoRT.Priv.unsafe_set_field!(A, T, off, val)

stores the value `val` with type `T` at offset `off` (in bytes) in **mutable**
object `A` . The method is labelled *unsafe* because it does not check the
validity of the arguments. It however takes care of avoiding that `A` be
garbage collected during the operation and of converting `val` to the type `T`.

If the value is an ASCII C string stored in a fixed length array of `siz`
bytes, call:

    TaoRT.Priv.unsafe_set_field!(A, Cstring, off, siz, val)

"""
@inline unsafe_set_field!(A, ::Type{T}, off::Integer, val) where {T} =
    GC.@preserve A unsafe_store!(unsafe_pointer(T, A, off), as(T, val))

function unsafe_set_field!(A, ::Type{Cstring}, off::Integer, siz::Int,
                           str::AbstractString)
    len = length(str)
    len < siz || error("string is too long")
    ptr = unsafe_pointer(UInt8, A, off)
    GC.@preserve A begin
        j = 0
        for i in 1:len
            j = nextind(str, j)
            c = str[j]
            isascii(c) || error("only ASCII strings are supported")
            unsafe_store!(ptr, c, i)
        end
        for i in len+1:siz
            unsafe_store!(ptr, 0x00, i)
        end
    end
end

"""
    TaoRT.Priv.unsafe_string_length(ptr, siz)

yields the length of the C string stored at address `ptr` with a maximum number
of bytes `siz` (including the final null) throwing an error if the contents is
not a null terminated ASCII string.

"""
function unsafe_string_length(ptr::Ptr{UInt8}, siz::Int)
    flag = 0x00
    for i in 1:siz
        c = unsafe_load(ptr, i)
        if c == 0x00
            flag == 0x00 || argument_error("only ASCII strings are supported")
            return i - 1
        end
        flag |= (c & 0x80)
    end
    argument_error("not a null terminated string")
end

# Manage to have enumerations being equivalent to symbolic shortcuts.
import ..TaoRT: LockMode
import ..Lib: tao_command, tao_eltype, tao_preprocessing, tao_status, tao_state
for (type, rule) in (
    (:LockMode, nothing),
    (:tao_command, r"^.*TAO_COMMAND_" => ""),
    (:tao_eltype, r"^.*TAO_" => ""),
    (:tao_preprocessing, r"^.*TAO_PREPROCESSING_" => ""),
    (:tao_status, r"^.*TAO_" => ""),
    (:tao_state, r"^.*TAO_STATE_" => ""))

    # Build meta-code.
    T = eval(type)
    vals, keys = T[], Symbol[]
    for val in instances(T)
        str = string(val)
        if rule !== nothing
            match(first(rule), str) === nothing && error(
                "name of value $val of enumeration $T does not match ",
                first(rule))
            str = replace(str, rule)
        end
        key = Symbol(lowercase(str))
        push!(vals, val)
        push!(keys, key)
    end
    mesg = "unexpected value for `$T` enumeration"
    expr = :(throw(ArgumentError($mesg)))
    for i in reverse(sortperm(vals))
        key, val = keys[i], vals[i]
        @eval $type(::$(Val{key})) = $val
        expr = Expr(:if, :(x == $val), Expr(:quote, key), expr)
    end
    @eval begin
        Base.Symbol(x::$type) = $expr
        Base.convert(::Type{Symbol}, x::$type) = Symbol(x)
        Base.convert(::Type{$type}, x::$type) = x
        Base.convert(::Type{$type}, x::Symbol) = $type(x)
        Base.:(==)(key::Symbol, val::$type) = ($type(key) == val)
        Base.:(==)(val::$type, key::Symbol) = (key == val)
        Base.show(io::IO, ::MIME"text/plain", x::$type) = show(io, x)
        $type(key::Symbol) = $type(Val(key))
        $type(::Val{sym}) where sym = bad_symbolic_name($type, sym)
    end
    if type === :tao_eltype
        @eval begin
            Base.show(io::IO, x::$type) = print(io, DataType(x))
        end
    else
        @eval begin
            Base.show(io::IO, x::$type) = print(io, ':', Symbol(x))
        end
    end
end
LockMode(x::LockMode) = x

@noinline bad_symbolic_name(::Type{T}, sym::Symbol) where {T} =
    error("unsupported symbolic name `:$sym` for type `$T`")

# Table of TAO array element types and equivalent Julia types.
const ELEM_TYPES = ((Int8,    TAO_INT8),
                    (UInt8,   TAO_UINT8),
                    (Int16,   TAO_INT16),
                    (UInt16,  TAO_UINT16),
                    (Int32,   TAO_INT32),
                    (UInt32,  TAO_UINT32),
                    (Int64,   TAO_INT64),
                    (UInt64,  TAO_UINT64),
                    (Cfloat,  TAO_FLOAT),
                    (Cdouble, TAO_DOUBLE))

# Manage to have `tao_eltype(T)` yields the TAO enumeration for element type
# `T` and conversely with `DataType`.
for (T, val) in ELEM_TYPES
    @eval begin
        Lib.tao_eltype(::Type{$T}) = $val
        Base.convert(::Type{tao_eltype}, ::Type{$T}) = $val
        Base.:(==)(::Type{$T}, val::Lib.tao_eltype) = (val == $val)
    end
end
let expr = :(argument_error("unexpected value for `tao_eltype` enumeration"))
    for (T, val) in reverse(ELEM_TYPES)
        expr = Expr(:if, :(x == $val), :($T), expr)
    end
    @eval Base.DataType(x::tao_eltype) = $expr
end
Base.convert(::Type{DataType}, x::tao_eltype) = DataType(x)
Base.:(==)(x::Lib.tao_eltype, y::Type) = (y == x)
