# User visible changes in `TaoRT` package

## Version 0.12.0

- `findlast` can be used to find a data-frame given its mark.

- Macros `@lock`, `@rdlock`, `@wrlock`, `@unlock`, `@try_lock`, `@try_rdlock`, and
  `@try_wrlock` are provided with improved performance compared to `do`-block syntax
  because they avoid creating a closure.

- Timeout is specified as a keyword in locking methods.

## Version 0.11.0

- Now building package just find the TAO libraries and headers and no longer
  requires `Clang.jl` which is only needed to re-generate the wrapper code in
  [`src/wrap.jl`](src/wrap.jl). The wrapper code shall only be regenerated when
  there are changes in the API of the TAO library (see
  [`gen/README.md`](gen/README.md)).

## Version 0.10.1

- Use new API for time values in `Tao`.

## Version 0.10.0

- New method `attach` to attach to any shared TAO object.
