* Check that object is properly locked when setting/getting properties.

* Check `SharedArray` performances and only extend the minimal number of
  methods.

* Not a good idea to allow for attaching to an object more than once.
